package com.baqu;

import com.baqu.callback.DefaultCallback;
import com.baqu.entity.Actor;
import com.baqu.entity.FullActorDetails;
import com.baqu.entity.Media;
import com.baqu.entity.MediaComments;
import com.baqu.entity.MediaList;
import com.baqu.entity.User;

/**
 * Created by mbaque on 02/02/2017.
 */

public interface RepositoryInterface {

    void onActivityStart(VoidCallback listener);

    void onActivityStop();

    void getRecommendedList(MediaListCallback callback);

    void searchMedia(String query, MediaListCallback dataCallback);

    void onMediaVisited(Media media, BooleanCallback dataCallback);

    void getDiscover(int page, MediaListCallback callback);

    void normalLogin(String user, String password, VoidCallback callback);

    void register(String email, String password, VoidCallback callback);

    void getMediaDetails(Media media, MediaCallback callback);

    void getMostPopular(int page, MediaListCallback callback);

    void getUserDetails(UserCallback callback);

    void getTopRated(int page, MediaListCallback callback);

    void getFavourites(MediaListCallback dataCallback);

    void addMediaToFavourites(Media media, VoidCallback dataCallback);

    void deleteMediaFromFavourites(Media media, VoidCallback dataCallback);

    void getMediaComments(Media media, CommentsCallback dataCallback);

    void getActorDetails(Actor actor, ActorCallback dataCallback);

    void logoff();

    interface MediaListCallback extends DefaultCallback<MediaList> {}

    interface VoidCallback extends DefaultCallback<Void> {}

    interface UserCallback extends DefaultCallback<User> {}

    interface BooleanCallback extends DefaultCallback<Boolean> {}

    interface CommentsCallback extends DefaultCallback<MediaComments> {}

    interface MediaCallback extends DefaultCallback<Media> {}

    interface ActorCallback extends DefaultCallback<FullActorDetails> {}

}
