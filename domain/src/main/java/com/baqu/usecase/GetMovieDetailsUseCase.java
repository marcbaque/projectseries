package com.baqu.usecase;

import com.baqu.RepositoryInterface;
import com.baqu.callback.DefaultCallback;
import com.baqu.entity.Media;
import com.baqu.exception.ErrorBundle;
import com.baqu.executor.PostExecutionThread;
import com.baqu.executor.ThreadExecutor;
import com.baqu.interactor.BaseUseCase;
import com.baqu.interactor.Interactor;

import javax.inject.Inject;

/**
 * Created by mbaque on 07/02/2017.
 */

public class GetMovieDetailsUseCase extends BaseUseCase<Media> implements Interactor<Media, Media> {

    private final RepositoryInterface repository;
    private final ThreadExecutor executor;
    private Media media;

    private GetMovieDetailsUseCase.GetMovieDetailsCallback callback;

    RepositoryInterface.MediaCallback dataCallback = new RepositoryInterface.MediaCallback() {
        @Override
        public void onSuccess(Media returnParam) {
            notifyOnSuccess(returnParam, callback);
        }

        @Override
        public void onError(ErrorBundle errorBundle) {
            notifyOnError(errorBundle, callback);
        }
    };

    @Inject
    public GetMovieDetailsUseCase(PostExecutionThread postExecutionThread, ThreadExecutor executor, RepositoryInterface repository) {
        super(postExecutionThread);
        this.repository = repository;
        this.executor = executor;
    }

    @Override
    public <R extends DefaultCallback<Media>> void execute(Media media, R defaultCallback) {
        this.callback = ((GetMovieDetailsUseCase.GetMovieDetailsCallback) defaultCallback);
        this.media = media;
        executor.execute(this);
    }

    @Override
    public void run() {
        repository.getMediaDetails(media, dataCallback);
    }

    public interface GetMovieDetailsCallback extends DefaultCallback<Media> {}

}
