package com.baqu.usecase;

import com.baqu.RepositoryInterface;
import com.baqu.callback.DefaultCallback;
import com.baqu.entity.MediaList;
import com.baqu.exception.ErrorBundle;
import com.baqu.executor.PostExecutionThread;
import com.baqu.executor.ThreadExecutor;
import com.baqu.interactor.BaseUseCase;
import com.baqu.interactor.Interactor;

import javax.inject.Inject;

/**
 * Created by mbaque on 01/02/2017.
 */

public class MostPopularUseCase extends BaseUseCase<MediaList> implements Interactor<MediaList, Integer> {

    private final RepositoryInterface repository;
    private final ThreadExecutor executor;
    private MostPopularUseCase.GetMostPopularListCallback callback;
    RepositoryInterface.MediaListCallback dataCallback = new RepositoryInterface.MediaListCallback() {
        @Override
        public void onSuccess(MediaList returnParam) {
            notifyOnSuccess(returnParam, callback);
        }

        @Override
        public void onError(ErrorBundle errorBundle) {
            notifyOnError(errorBundle, callback);
        }
    };
    private int page;
    @Inject
    public MostPopularUseCase(PostExecutionThread postExecutionThread, ThreadExecutor executor, RepositoryInterface repository) {
        super(postExecutionThread);
        this.repository = repository;
        this.executor = executor;
    }

    @Override
    public <R extends DefaultCallback<MediaList>> void execute(Integer page, R defaultCallback) {
        this.callback = ((MostPopularUseCase.GetMostPopularListCallback) defaultCallback);
        this.page = page;
        executor.execute(this);
    }

    @Override
    public void run() {
        repository.getMostPopular(page, dataCallback);
    }

    public interface GetMostPopularListCallback extends DefaultCallback<MediaList>{}

}
