package com.baqu.usecase;

import com.baqu.RepositoryInterface;
import com.baqu.callback.DefaultCallback;
import com.baqu.entity.Media;
import com.baqu.exception.ErrorBundle;
import com.baqu.executor.PostExecutionThread;
import com.baqu.executor.ThreadExecutor;
import com.baqu.interactor.BaseUseCase;
import com.baqu.interactor.Interactor;

import javax.inject.Inject;

/**
 * Created by mbaque on 04/02/2017.
 */

public class RemoveMediaFromFavouritesUseCase extends BaseUseCase<Void> implements Interactor<Void, Media> {

    private final RepositoryInterface repository;
    private final ThreadExecutor executor;
    private RemoveMediaFromFavouritesUseCase.RemoveMediaFromFavourites callback;
    RepositoryInterface.VoidCallback dataCallback = new RepositoryInterface.VoidCallback() {
        @Override
        public void onSuccess(Void returnParam) {
            notifyOnSuccess(returnParam, callback);
        }

        @Override
        public void onError(ErrorBundle errorBundle) {
            notifyOnError(errorBundle, callback);
        }
    };
    private Media media;
    @Inject
    public RemoveMediaFromFavouritesUseCase(PostExecutionThread postExecutionThread, ThreadExecutor executor, RepositoryInterface repository) {
        super(postExecutionThread);
        this.repository = repository;
        this.executor = executor;
    }

    @Override
    public <R extends DefaultCallback<Void>> void execute(Media media, R defaultCallback) {
        this.callback = ((RemoveMediaFromFavouritesUseCase.RemoveMediaFromFavourites) defaultCallback);
        this.media = media;
        executor.execute(this);
    }

    @Override
    public void run() {
        repository.deleteMediaFromFavourites(media, dataCallback);
    }

    public interface RemoveMediaFromFavourites extends DefaultCallback<Void> {}

}
