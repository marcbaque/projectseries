package com.baqu.usecase;

import com.baqu.RepositoryInterface;
import com.baqu.callback.DefaultCallback;
import com.baqu.entity.UserParams;
import com.baqu.exception.ErrorBundle;
import com.baqu.executor.PostExecutionThread;
import com.baqu.executor.ThreadExecutor;
import com.baqu.interactor.BaseUseCase;
import com.baqu.interactor.Interactor;

import javax.inject.Inject;

/**
 * Created by mbaque on 02/02/2017.
 */

public class RegisterUseCase extends BaseUseCase<Void> implements Interactor<Void, UserParams> {

    private final RepositoryInterface repository;
    private final ThreadExecutor executor;
    private RegisterUseCase.RegisterCallback callback;
    RepositoryInterface.VoidCallback dataCallback = new RepositoryInterface.VoidCallback() {
        @Override
        public void onSuccess(Void returnParam) {
            notifyOnSuccess(returnParam, callback);
        }

        @Override
        public void onError(ErrorBundle errorBundle) {
            notifyOnError(errorBundle, callback);
        }
    };
    private UserParams params;
    @Inject
    public RegisterUseCase(PostExecutionThread postExecutionThread, ThreadExecutor executor, RepositoryInterface repository) {
        super(postExecutionThread);
        this.repository = repository;
        this.executor = executor;
    }

    @Override
    public <R extends DefaultCallback<Void>> void execute(UserParams params, R defaultCallback) {
        this.callback = ((RegisterUseCase.RegisterCallback) defaultCallback);
        this.params = params;
        executor.execute(this);
    }

    @Override
    public void run() {
        repository.register(params.getUser(), params.getPassword(), dataCallback);
    }

    public interface RegisterCallback extends DefaultCallback<Void> {}

}
