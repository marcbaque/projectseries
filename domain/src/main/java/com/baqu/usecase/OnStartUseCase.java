package com.baqu.usecase;

import com.baqu.RepositoryInterface;
import com.baqu.callback.DefaultCallback;
import com.baqu.exception.ErrorBundle;
import com.baqu.executor.PostExecutionThread;
import com.baqu.executor.ThreadExecutor;
import com.baqu.interactor.BaseUseCase;
import com.baqu.interactor.Interactor;

import javax.inject.Inject;

/**
 * Created by mbaque on 03/02/2017.
 */

public class OnStartUseCase extends BaseUseCase<Void> implements Interactor<Void, Void>{

    private final RepositoryInterface repository;
    private final ThreadExecutor executor;
    private OnStartUseCase.UserListener callback;
    RepositoryInterface.VoidCallback dataCallback = new RepositoryInterface.VoidCallback() {
        @Override
        public void onSuccess(Void returnParam) {
            notifyOnSuccess(returnParam, callback);
        }

        @Override
        public void onError(ErrorBundle errorBundle) {
            notifyOnError(errorBundle, callback);
        }
    };
    @Inject
    public OnStartUseCase(PostExecutionThread postExecutionThread, ThreadExecutor executor, RepositoryInterface repository) {
        super(postExecutionThread);
        this.repository = repository;
        this.executor = executor;
    }

    @Override
    public <R extends DefaultCallback<Void>> void execute(Void aVoid, R defaultCallback) {
        this.callback = (OnStartUseCase.UserListener) defaultCallback;
        executor.execute(this);
    }

    @Override
    public void run() {
        repository.onActivityStart(dataCallback);
    }

    public interface UserListener extends DefaultCallback<Void> {}
}
