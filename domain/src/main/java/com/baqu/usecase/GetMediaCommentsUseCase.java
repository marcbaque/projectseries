package com.baqu.usecase;

import com.baqu.RepositoryInterface;
import com.baqu.callback.DefaultCallback;
import com.baqu.entity.Media;
import com.baqu.entity.MediaComments;
import com.baqu.exception.ErrorBundle;
import com.baqu.executor.PostExecutionThread;
import com.baqu.executor.ThreadExecutor;
import com.baqu.interactor.BaseUseCase;
import com.baqu.interactor.Interactor;

import javax.inject.Inject;

/**
 * Created by mbaque on 07/02/2017.
 */

public class GetMediaCommentsUseCase extends BaseUseCase<MediaComments> implements Interactor<MediaComments, Media> {

    private final RepositoryInterface repository;
    private final ThreadExecutor executor;

    private GetMediaCommentsUseCase.GetCommentsCallback callback;

    RepositoryInterface.CommentsCallback dataCallback = new RepositoryInterface.CommentsCallback() {
        @Override
        public void onSuccess(MediaComments returnParam) {
            notifyOnSuccess(returnParam, callback);
        }

        @Override
        public void onError(ErrorBundle errorBundle) {
            notifyOnError(errorBundle, callback);
        }
    };

    private Media media;

    @Inject
    public GetMediaCommentsUseCase(PostExecutionThread postExecutionThread, ThreadExecutor executor, RepositoryInterface repository) {
        super(postExecutionThread);
        this.repository = repository;
        this.executor = executor;
    }

    @Override
    public <R extends DefaultCallback<MediaComments>> void execute(Media media, R defaultCallback) {
        this.callback = ((GetMediaCommentsUseCase.GetCommentsCallback) defaultCallback);
        this.media = media;
        executor.execute(this);
    }

    @Override
    public void run() {
        repository.getMediaComments(media, dataCallback);
    }

    public interface GetCommentsCallback extends DefaultCallback<MediaComments>{}

}
