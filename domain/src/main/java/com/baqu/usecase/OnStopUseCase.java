package com.baqu.usecase;

import com.baqu.RepositoryInterface;
import com.baqu.callback.DefaultCallback;
import com.baqu.executor.PostExecutionThread;
import com.baqu.executor.ThreadExecutor;
import com.baqu.interactor.BaseUseCase;
import com.baqu.interactor.Interactor;

import javax.inject.Inject;

/**
 * Created by mbaque on 03/02/2017.
 */

public class OnStopUseCase extends BaseUseCase<Void> implements Interactor<Void, Void> {

    private final RepositoryInterface repository;
    private final ThreadExecutor executor;

    @Inject
    public OnStopUseCase(PostExecutionThread postExecutionThread, ThreadExecutor executor, RepositoryInterface repository) {
        super(postExecutionThread);
        this.repository = repository;
        this.executor = executor;
    }

    @Override
    public <R extends DefaultCallback<Void>> void execute(Void aVoid, R defaultCallback) {
        executor.execute(this);
    }

    @Override
    public void run() {
        repository.onActivityStop();
    }

}
