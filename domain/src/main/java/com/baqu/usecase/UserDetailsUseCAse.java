package com.baqu.usecase;

import com.baqu.RepositoryInterface;
import com.baqu.callback.DefaultCallback;
import com.baqu.entity.User;
import com.baqu.exception.ErrorBundle;
import com.baqu.executor.PostExecutionThread;
import com.baqu.executor.ThreadExecutor;
import com.baqu.interactor.BaseUseCase;
import com.baqu.interactor.Interactor;

import javax.inject.Inject;

/**
 * Created by mbaque on 01/02/2017.
 */

public class UserDetailsUseCase extends BaseUseCase<User> implements Interactor<User, Void> {

    private final RepositoryInterface repository;
    private final ThreadExecutor executor;
    private UserDetailsCallback callback;
    RepositoryInterface.UserCallback dataCallback = new RepositoryInterface.UserCallback() {
        @Override
        public void onSuccess(User returnParam) {
            notifyOnSuccess(returnParam, callback);
        }

        @Override
        public void onError(ErrorBundle errorBundle) {
            notifyOnError(errorBundle, callback);
        }
    };
    @Inject
    public UserDetailsUseCase(PostExecutionThread postExecutionThread, ThreadExecutor executor, RepositoryInterface repository) {
        super(postExecutionThread);
        this.repository = repository;
        this.executor = executor;
    }

    @Override
    public <R extends DefaultCallback<User>> void execute(Void aVoid, R defaultCallback) {
        this.callback = ((UserDetailsCallback) defaultCallback);
        executor.execute(this);
    }

    @Override
    public void run() {
        repository.getUserDetails(dataCallback);
    }

    public interface UserDetailsCallback extends DefaultCallback<User>{}
}
