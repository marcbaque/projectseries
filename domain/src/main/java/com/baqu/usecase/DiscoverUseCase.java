package com.baqu.usecase;

import com.baqu.RepositoryInterface;
import com.baqu.callback.DefaultCallback;
import com.baqu.entity.MediaList;
import com.baqu.exception.ErrorBundle;
import com.baqu.executor.PostExecutionThread;
import com.baqu.executor.ThreadExecutor;
import com.baqu.interactor.BaseUseCase;
import com.baqu.interactor.Interactor;

import javax.inject.Inject;

/**
 * Created by mbaque on 01/02/2017.
 */

public class DiscoverUseCase extends BaseUseCase<MediaList> implements Interactor<MediaList, Integer> {

    private final RepositoryInterface repository;
    private final ThreadExecutor executor;
    private DiscoverUseCase.GetDiscoverListCallback callback;
    RepositoryInterface.MediaListCallback dataCallback = new RepositoryInterface.MediaListCallback() {
        @Override
        public void onSuccess(MediaList returnParam) {
            notifyOnSuccess(returnParam, callback);
        }

        @Override
        public void onError(ErrorBundle errorBundle) {
            notifyOnError(errorBundle, callback);
        }
    };
    private int page;
    @Inject
    public DiscoverUseCase(PostExecutionThread postExecutionThread, ThreadExecutor executor, RepositoryInterface repository) {
        super(postExecutionThread);
        this.repository = repository;
        this.executor = executor;
    }

    @Override
    public <R extends DefaultCallback<MediaList>> void execute(Integer page, R defaultCallback) {
        this.callback = ((DiscoverUseCase.GetDiscoverListCallback) defaultCallback);
        this.page = page;
        executor.execute(this);
    }

    @Override
    public void run() {
        repository.getDiscover(page, dataCallback);
    }

    public interface GetDiscoverListCallback extends DefaultCallback<MediaList>{}

}
