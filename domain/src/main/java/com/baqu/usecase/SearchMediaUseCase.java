package com.baqu.usecase;

import com.baqu.RepositoryInterface;
import com.baqu.callback.DefaultCallback;
import com.baqu.entity.MediaList;
import com.baqu.exception.ErrorBundle;
import com.baqu.executor.PostExecutionThread;
import com.baqu.executor.ThreadExecutor;
import com.baqu.interactor.BaseUseCase;
import com.baqu.interactor.Interactor;

import javax.inject.Inject;

/**
 * Created by mbaque on 06/02/2017.
 */

public class SearchMediaUseCase extends BaseUseCase<MediaList> implements Interactor<MediaList, String> {

    private final RepositoryInterface repository;
    private final ThreadExecutor executor;
    private SearchMediaUseCase.SearchMediaCallback callback;
    RepositoryInterface.MediaListCallback dataCallback = new RepositoryInterface.MediaListCallback() {
        @Override
        public void onSuccess(MediaList returnParam) {
            notifyOnSuccess(returnParam, callback);
        }

        @Override
        public void onError(ErrorBundle errorBundle) {
            notifyOnError(errorBundle, callback);
        }
    };
    private String query;
    @Inject
    public SearchMediaUseCase(PostExecutionThread postExecutionThread, ThreadExecutor executor, RepositoryInterface repository) {
        super(postExecutionThread);
        this.repository = repository;
        this.executor = executor;
    }

    @Override
    public <R extends DefaultCallback<MediaList>> void execute(String query, R defaultCallback) {
        this.callback = ((SearchMediaUseCase.SearchMediaCallback) defaultCallback);
        this.query = query;
        executor.execute(this);
    }

    @Override
    public void run() {
        repository.searchMedia(query, dataCallback);
    }

    public interface SearchMediaCallback extends DefaultCallback<MediaList> {}

}
