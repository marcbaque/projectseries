package com.baqu.usecase;

import com.baqu.RepositoryInterface;
import com.baqu.callback.DefaultCallback;
import com.baqu.entity.Media;
import com.baqu.exception.ErrorBundle;
import com.baqu.executor.PostExecutionThread;
import com.baqu.executor.ThreadExecutor;
import com.baqu.interactor.BaseUseCase;
import com.baqu.interactor.Interactor;

import javax.inject.Inject;

/**
 * Created by mbaque on 05/02/2017.
 */

public class OnMediaVisitedUseCase extends BaseUseCase<Boolean> implements Interactor<Boolean, Media> {

    private final RepositoryInterface repository;
    private final ThreadExecutor executor;
    private OnMediaVisitedUseCase.OnMediaVisitedCallback callback;
    RepositoryInterface.BooleanCallback dataCallback = new RepositoryInterface.BooleanCallback() {
        @Override
        public void onSuccess(Boolean returnParam) {
            notifyOnSuccess(returnParam, callback);
        }

        @Override
        public void onError(ErrorBundle errorBundle) {
            notifyOnError(errorBundle, callback);
        }
    };
    private Media media;
    @Inject
    public OnMediaVisitedUseCase(PostExecutionThread postExecutionThread, ThreadExecutor executor, RepositoryInterface repository) {
        super(postExecutionThread);
        this.repository = repository;
        this.executor = executor;
    }

    @Override
    public <R extends DefaultCallback<Boolean>> void execute(Media media, R defaultCallback) {
        this.media = media;
        this.callback = ((OnMediaVisitedUseCase.OnMediaVisitedCallback) defaultCallback);
        executor.execute(this);
    }

    @Override
    public void run() {
        repository.onMediaVisited(media, dataCallback);
    }

    public interface OnMediaVisitedCallback extends DefaultCallback<Boolean> {}

}
