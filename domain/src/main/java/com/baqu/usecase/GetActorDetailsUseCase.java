package com.baqu.usecase;

import com.baqu.RepositoryInterface;
import com.baqu.callback.DefaultCallback;
import com.baqu.entity.Actor;
import com.baqu.entity.FullActorDetails;
import com.baqu.entity.MediaList;
import com.baqu.exception.ErrorBundle;
import com.baqu.executor.PostExecutionThread;
import com.baqu.executor.ThreadExecutor;
import com.baqu.interactor.BaseUseCase;
import com.baqu.interactor.Interactor;

import javax.inject.Inject;

/**
 * Created by mbaque on 09/02/2017.
 */

public class GetActorDetailsUseCase extends BaseUseCase<FullActorDetails> implements Interactor<FullActorDetails, Actor> {

    private final RepositoryInterface repository;
    private final ThreadExecutor executor;

    private GetActorDetailsUseCase.GetActorDetailsCallback callback;

    RepositoryInterface.ActorCallback dataCallback = new RepositoryInterface.ActorCallback() {
        @Override
        public void onSuccess(FullActorDetails returnParam) {
            notifyOnSuccess(returnParam, callback);
        }

        @Override
        public void onError(ErrorBundle errorBundle) {
            notifyOnError(errorBundle, callback);
        }
    };
    private Actor actor;

    @Inject
    public GetActorDetailsUseCase(PostExecutionThread postExecutionThread, ThreadExecutor executor, RepositoryInterface repository) {
        super(postExecutionThread);
        this.repository = repository;
        this.executor = executor;
    }

    @Override
    public <R extends DefaultCallback<FullActorDetails>> void execute(Actor actor, R defaultCallback) {
        this.callback = ((GetActorDetailsUseCase.GetActorDetailsCallback) defaultCallback);
        this.actor = actor;
        executor.execute(this);
    }

    @Override
    public void run() {
        repository.getActorDetails(actor, dataCallback);
    }

    public interface GetActorDetailsCallback extends DefaultCallback<FullActorDetails> {}

}
