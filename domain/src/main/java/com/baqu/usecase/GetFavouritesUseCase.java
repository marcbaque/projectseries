package com.baqu.usecase;

import com.baqu.RepositoryInterface;
import com.baqu.callback.DefaultCallback;
import com.baqu.entity.MediaList;
import com.baqu.exception.ErrorBundle;
import com.baqu.executor.PostExecutionThread;
import com.baqu.executor.ThreadExecutor;
import com.baqu.interactor.BaseUseCase;
import com.baqu.interactor.Interactor;

import javax.inject.Inject;

/**
 * Created by mbaque on 04/02/2017.
 */

public class GetFavouritesUseCase extends BaseUseCase<MediaList> implements Interactor<MediaList, Void> {

    private final RepositoryInterface repository;
    private final ThreadExecutor executor;
    private GetFavouritesUseCase.GetFavouritesListCallback callback;
    RepositoryInterface.MediaListCallback dataCallback = new RepositoryInterface.MediaListCallback() {
        @Override
        public void onSuccess(MediaList returnParam) {
            notifyOnSuccess(returnParam, callback);
        }

        @Override
        public void onError(ErrorBundle errorBundle) {
            notifyOnError(errorBundle, callback);
        }
    };
    @Inject
    public GetFavouritesUseCase(PostExecutionThread postExecutionThread, ThreadExecutor executor, RepositoryInterface repository) {
        super(postExecutionThread);
        this.repository = repository;
        this.executor = executor;
    }

    @Override
    public <R extends DefaultCallback<MediaList>> void execute(Void aVoid, R defaultCallback) {
        this.callback = ((GetFavouritesUseCase.GetFavouritesListCallback) defaultCallback);
        executor.execute(this);
    }

    @Override
    public void run() {
        repository.getFavourites(dataCallback);
    }

    public interface GetFavouritesListCallback extends DefaultCallback<MediaList> {}

}
