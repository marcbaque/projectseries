package com.baqu.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mbaque on 01/02/2017.
 */

public class MediaList {

    private List<Media> mediaList;

    public MediaList(List<Media> mediaList) {
        this.mediaList = mediaList;
    }

    public MediaList() {
        this.mediaList = new ArrayList<>();
    }

    public List<Media> getMediaList() {
        return mediaList;
    }

    public void setMediaList(List<Media> mediaList) {
        this.mediaList = mediaList;
    }

}
