package com.baqu.entity;

/**
 * Created by mbaque on 07/02/2017.
 */

public class TvisoUser {

    String uid;

    String fullName;

    String imagePath;

    public TvisoUser() {
    }

    public TvisoUser(String uid, String fullName, String imagePath) {
        this.uid = uid;
        this.fullName = fullName;
        this.imagePath = imagePath;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
}
