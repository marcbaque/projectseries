package com.baqu.entity;

/**
 * Created by mbaque on 01/02/2017.
 */

public abstract class Media {

    public abstract String getPoster();
    public abstract String getRate();
    public abstract String getBackdrop();
    public abstract String getTitle();
    public abstract String getOverview();
    public abstract String getVoteCount();
    public abstract String getYear();
    public abstract String getPopularity();
    public abstract String getImdbId();
    public abstract String getTrailerKey();

}
