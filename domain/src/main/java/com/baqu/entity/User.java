package com.baqu.entity;

/**
 * Created by mbaque on 28/01/2017.
 */

public class User {

    String id;
    String language;
    String country;
    String email;

    public User() {
    }

    public User(String id, String language, String country, String email) {
        this.id = id;
        this.language = language;
        this.country = country;
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
