package com.baqu.entity;

/**
 * Created by mbaque on 02/02/2017.
 */

public class UserParams {

    String user;
    String password;

    public UserParams(String user, String password) {
        this.user = user;
        this.password = password;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
