package com.baqu.entity;

import java.util.List;

/**
 * Created by mbaque on 02/02/2017.
 */

public class Serie extends Media {

    String year;
    String title;
    String poster;
    String rate;
    String popularity;
    boolean adult;
    String overview;
    List<Integer> genres;
    String id;
    String originalTitle;
    String originalLanguage;
    String backdrop;
    String voteCount;
    String imdbId;
    Cast cast;
    MediaList similar;
    String trailerKey;
    List<Season> seasons;

    public Serie(String year, String title, String poster, String rate, String popularity, boolean adult, String overview, List<Integer> genres, String id, String originalTitle, String originalLanguage, String backdrop, String voteCount, String imdbId, Cast cast, MediaList similar, String trailerKey, List<Season> seasons) {
        this.year = year;
        this.title = title;
        this.poster = poster;
        this.rate = rate;
        this.popularity = popularity;
        this.adult = adult;
        this.overview = overview;
        this.genres = genres;
        this.id = id;
        this.originalTitle = originalTitle;
        this.originalLanguage = originalLanguage;
        this.backdrop = backdrop;
        this.voteCount = voteCount;
        this.imdbId = imdbId;
        this.cast = cast;
        this.similar = similar;
        this.trailerKey = trailerKey;
        this.seasons = seasons;
    }

    public List<Season> getSeasons() {
        return seasons;
    }

    public void setSeasons(List<Season> seasons) {
        this.seasons = seasons;
    }

    public String getTrailerKey() {
        return trailerKey;
    }

    public void setTrailerKey(String trailerKey) {
        this.trailerKey = trailerKey;
    }

    public Cast getCast() {
        return cast;
    }

    public void setCast(Cast cast) {
        this.cast = cast;
    }

    public MediaList getSimilar() {
        return similar;
    }

    public void setSimilar(MediaList similar) {
        this.similar = similar;
    }

    public String getImdbId() {
        return imdbId;
    }

    public void setImdbId(String imdbId) {
        this.imdbId = imdbId;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getPopularity() {
        return popularity;
    }

    public void setPopularity(String popularity) {
        this.popularity = popularity;
    }

    public boolean isAdult() {
        return adult;
    }

    public void setAdult(boolean adult) {
        this.adult = adult;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public List<Integer> getGenres() {
        return genres;
    }

    public void setGenres(List<Integer> genres) {
        this.genres = genres;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public String getOriginalLanguage() {
        return originalLanguage;
    }

    public void setOriginalLanguage(String originalLanguage) {
        this.originalLanguage = originalLanguage;
    }

    public String getBackdrop() {
        return backdrop;
    }

    public void setBackdrop(String backdrop) {
        this.backdrop = backdrop;
    }

    public String getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(String voteCount) {
        this.voteCount = voteCount;
    }

}
