package com.baqu.entity;

import java.util.List;

/**
 * Created by mbaque on 02/02/2017.
 */

public class Cast {

    List<Actor> cast;

    public Cast(List<Actor> cast) {
        this.cast = cast;
    }

    public List<Actor> getCast() {
        return cast;
    }

    public void setCast(List<Actor> cast) {
        this.cast = cast;
    }

}
