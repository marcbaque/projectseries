package com.baqu.entity;

import java.util.List;

/**
 * Created by mbaque on 07/02/2017.
 */

public class MediaComments {

    List<Comment> reviews;

    public MediaComments() {
    }

    public MediaComments(List<Comment> reviews) {
        this.reviews = reviews;
    }

    public List<Comment> getReviews() {
        return reviews;
    }

    public void setReviews(List<Comment> reviews) {
        this.reviews = reviews;
    }
}
