package com.baqu.entity;

import java.util.List;

/**
 * Created by mbaque on 02/02/2017.
 */

public class Movie extends Media {

    String year;
    boolean adult;
    String title;
    String poster;
    String rate;
    String popularity;
    String overview;
    List<Integer> genres;
    String id;
    String originalTitle;
    String originalLanguage;
    String backdrop;
    String voteCount;
    String imdbId;
    Cast credits;
    MediaList similar;
    String trailerKey;

    public Movie(String year, boolean adult, String title, String poster, String rate, String popularity, String overview, List<Integer> genres, String id, String originalTitle, String originalLanguage, String backdrop, String voteCount, String imdbId, Cast credits, MediaList similar, String trailerKey) {
        this.year = year;
        this.adult = adult;
        this.title = title;
        this.poster = poster;
        this.rate = rate;
        this.popularity = popularity;
        this.overview = overview;
        this.genres = genres;
        this.id = id;
        this.originalTitle = originalTitle;
        this.originalLanguage = originalLanguage;
        this.backdrop = backdrop;
        this.voteCount = voteCount;
        this.imdbId = imdbId;
        this.credits = credits;
        this.similar = similar;
        this.trailerKey = trailerKey;
    }

    public String getTrailerKey() {
        return trailerKey;
    }

    public void setTrailerKey(String trailerKey) {
        this.trailerKey = trailerKey;
    }

    public Cast getCredits() {
        return credits;
    }

    public void setCredits(Cast credits) {
        this.credits = credits;
    }

    public MediaList getSimilar() {
        return similar;
    }

    public void setSimilar(MediaList similar) {
        this.similar = similar;
    }

    public String getImdbId() {
        return imdbId;
    }

    public void setImdbId(String imdbId) {
        this.imdbId = imdbId;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public boolean isAdult() {
        return adult;
    }

    public void setAdult(boolean adult) {
        this.adult = adult;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getPopularity() {
        return popularity;
    }

    public void setPopularity(String popularity) {
        this.popularity = popularity;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public List<Integer> getGenres() {
        return genres;
    }

    public void setGenres(List<Integer> genres) {
        this.genres = genres;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public String getOriginalLanguage() {
        return originalLanguage;
    }

    public void setOriginalLanguage(String originalLanguage) {
        this.originalLanguage = originalLanguage;
    }

    public String getBackdrop() {
        return backdrop;
    }

    public void setBackdrop(String backdrop) {
        this.backdrop = backdrop;
    }

    public String getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(String voteCount) {
        this.voteCount = voteCount;
    }

}
