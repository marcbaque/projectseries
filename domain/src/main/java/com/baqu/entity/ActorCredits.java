package com.baqu.entity;

import java.util.List;

/**
 * Created by mbaque on 09/02/2017.
 */
public class ActorCredits {

    List<ActorFilm> cast;

    public ActorCredits(List<ActorFilm> cast) {
        this.cast = cast;
    }

    public List<ActorFilm> getCast() {
        return cast;
    }

    public void setCast(List<ActorFilm> cast) {
        this.cast = cast;
    }
}
