package com.baqu.entity;

import java.util.List;

/**
 * Created by mbaque on 09/02/2017.
 */

public class FullActorDetails {

    String id;

    String name;

    String placeOfBirth;

    String profilePath;

    String biography;

    ActorCredits combinedCredits;

    public FullActorDetails(String id, String name, String placeOfBirth, String profilePath, String biography, ActorCredits combinedCredits) {
        this.id = id;
        this.name = name;
        this.placeOfBirth = placeOfBirth;
        this.profilePath = profilePath;
        this.biography = biography;
        this.combinedCredits = combinedCredits;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public String getProfilePath() {
        return profilePath;
    }

    public void setProfilePath(String profilePath) {
        this.profilePath = profilePath;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public ActorCredits getCombinedCredits() {
        return combinedCredits;
    }

    public void setCombinedCredits(ActorCredits combinedCredits) {
        this.combinedCredits = combinedCredits;
    }
}
