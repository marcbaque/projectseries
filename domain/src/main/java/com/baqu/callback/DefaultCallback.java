package com.baqu.callback;

import com.baqu.exception.ErrorBundle;

/**
 * Created by mbaque on 01/02/2017.
 */

public interface DefaultCallback<T> {

    void onSuccess(T returnParam);

    void onError(ErrorBundle errorBundle);

}
