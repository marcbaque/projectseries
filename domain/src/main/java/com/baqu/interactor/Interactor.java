package com.baqu.interactor;

import com.baqu.callback.DefaultCallback;

/**
 * Created by mbaque on 01/02/2017.
 */

public interface Interactor<ReturnType, InitParams> extends Runnable{

    void run();

    <R extends DefaultCallback<ReturnType>> void execute(InitParams params, R defaultCallback);

}
