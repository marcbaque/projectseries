package com.baqu.executor;

/**
 * Created by mbaque on 01/02/2017.
 */

public interface PostExecutionThread {

    void post(Runnable runnable);

}
