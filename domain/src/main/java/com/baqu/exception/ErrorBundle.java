package com.baqu.exception;

/**
 * Created by mbaque on 01/02/2017.
 */

public interface ErrorBundle {
    Exception getException();

    String getErrorMessage();
}
