package projectseries.baqu.com.projectseries.view;

import android.support.v7.widget.Toolbar;

/**
 * Created by mbaque on 28/01/2017.
 */

public interface MenuActivity {

    void generateMenu(Toolbar toolbar);

    void onUserNameRetrieved(String name);

    void onError(String message);

    void goToLogin();

    void showLoading(String message);

    void hideLoading();

}
