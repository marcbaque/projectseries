package projectseries.baqu.com.projectseries.view.impl;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import projectseries.baqu.com.projectseries.R;
import projectseries.baqu.com.projectseries.adapter.MovieMainListRecyclerViewAdapter;
import projectseries.baqu.com.projectseries.dependencyinjection.App;
import projectseries.baqu.com.projectseries.dependencyinjection.activity.ActivityModule;
import projectseries.baqu.com.projectseries.dependencyinjection.view.ViewModule;
import projectseries.baqu.com.projectseries.entity.MediaListView;
import projectseries.baqu.com.projectseries.entity.MediaView;
import projectseries.baqu.com.projectseries.presenter.FavouritesActivityPresenter;
import projectseries.baqu.com.projectseries.utils.DataType;
import projectseries.baqu.com.projectseries.view.FavouritesActivity;

/**
 * Created by mbaque on 04/02/2017.
 */

public class FavouritesActivityImpl extends MenuActivityImpl implements FavouritesActivity {

    MovieMainListRecyclerViewAdapter adapter;

    @Inject
    FavouritesActivityPresenter presenter;

    @BindView(R.id.home_tablayout)
    TabLayout tabLayout;

    @BindView(R.id.home_movie_recyclerview)
    RecyclerView recyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ((App) getApplication())
                .getComponent()
                .plus(new ActivityModule(this),
                        new ViewModule(this))
                .inject(this);

        ButterKnife.bind(this);

        initView();
    }

    protected void initView(){
        super.initView();
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch(tab.getPosition()){
                    case 0:
                        adapter.changeDataType(DataType.ALL);
                        break;
                    case 1:
                        adapter.changeDataType(DataType.MOVIE);
                        break;
                    case 2:
                        adapter.changeDataType(DataType.SERIES);
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        GridLayoutManager manager = new GridLayoutManager(this, 2);

        recyclerView.setLayoutManager(manager);

        presenter.onCreateAdapter();

    }

    @Override
    public void onListRetrieved(MediaListView data) {
        adapter = new MovieMainListRecyclerViewAdapter(this, data) {
            @Override
            public void onMovieSelected(MediaView selectedItem) {
                MovieDetailActivityImpl.startThisActivity(FavouritesActivityImpl.this, selectedItem);
            }
        };

        recyclerView.setAdapter(adapter);
    }
}
