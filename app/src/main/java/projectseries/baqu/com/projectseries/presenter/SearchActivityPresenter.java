package projectseries.baqu.com.projectseries.presenter;

import com.baqu.entity.MediaList;
import com.baqu.exception.ErrorBundle;
import com.baqu.usecase.OnStartUseCase;
import com.baqu.usecase.OnStopUseCase;
import com.baqu.usecase.SearchMediaUseCase;

import javax.inject.Inject;

import projectseries.baqu.com.projectseries.mapper.MediaMapper;
import projectseries.baqu.com.projectseries.view.SearchActivity;

/**
 * Created by mbaque on 06/02/2017.
 */

public class SearchActivityPresenter {

    SearchActivity view;
    OnStartUseCase onStartUseCase;
    OnStopUseCase onStopUseCase;
    SearchMediaUseCase searchMediaUseCase;

    @Inject
    public SearchActivityPresenter(SearchActivity view,
                                   SearchMediaUseCase searchMediaUseCase,
                                   OnStartUseCase onStartUseCase,
                                   OnStopUseCase onStopUseCase) {
        this.view = view;
        this.searchMediaUseCase = searchMediaUseCase;
        this.onStartUseCase = onStartUseCase;
        this.onStopUseCase = onStopUseCase;
    }

    public void onActivityStart(){
        onStartUseCase.execute(null, new OnStartUseCase.UserListener() {
            @Override
            public void onSuccess(Void returnParam) {

            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                view.goToLogin();
            }
        });
        onStartUseCase.run();
    }

    public void onActivityStop(){
        onStopUseCase.execute(null, null);
        onStopUseCase.run();
    }

    public void onCreate(String query) {
        view.showLoading("Cargando...");
        searchMediaUseCase.execute(query, new SearchMediaUseCase.SearchMediaCallback() {
            @Override
            public void onSuccess(MediaList returnParam) {
                view.onListRetrieved(MediaMapper.toMediaListView(returnParam));
                view.hideLoading();
            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                if(errorBundle!=null) {
                    view.onError(errorBundle.getErrorMessage());
                }
                view.hideLoading();
            }
        });
        searchMediaUseCase.run();
    }
}
