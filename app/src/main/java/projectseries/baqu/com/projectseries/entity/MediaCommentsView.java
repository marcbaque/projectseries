package projectseries.baqu.com.projectseries.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by mbaque on 07/02/2017.
 */

public class MediaCommentsView implements Serializable {

    List<CommentView> reviews;

    public MediaCommentsView() {
    }

    public MediaCommentsView(List<CommentView> reviews) {
        this.reviews = reviews;
    }

    public List<CommentView> getReviews() {
        return reviews;
    }

    public void setReviews(List<CommentView> reviews) {
        this.reviews = reviews;
    }
}
