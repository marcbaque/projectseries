package projectseries.baqu.com.projectseries.entity;

/**
 * Created by mbaque on 07/02/2017.
 */

public class ReplyView {

    String id;

    TvisoUserView actor;

    String content;

    String datePublished;

    public ReplyView() {
    }

    public ReplyView(String id, TvisoUserView actor, String content, String datePublished) {
        this.id = id;
        this.actor = actor;
        this.content = content;
        this.datePublished = datePublished;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public TvisoUserView getActor() {
        return actor;
    }

    public void setActor(TvisoUserView actor) {
        this.actor = actor;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDatePublished() {
        return datePublished;
    }

    public void setDatePublished(String datePublished) {
        this.datePublished = datePublished;
    }
}
