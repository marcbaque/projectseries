package projectseries.baqu.com.projectseries.dependencyinjection.view;

import dagger.Module;
import dagger.Provides;
import projectseries.baqu.com.data.datasource.DBDataSource;
import projectseries.baqu.com.data.datasource.FirebaseDataSource;
import projectseries.baqu.com.projectseries.view.ActorMovieListActivity;
import projectseries.baqu.com.projectseries.view.DiscoverActivity;
import projectseries.baqu.com.projectseries.view.FavouritesActivity;
import projectseries.baqu.com.projectseries.view.LoginActivity;
import projectseries.baqu.com.projectseries.view.MenuActivity;
import projectseries.baqu.com.projectseries.view.MostPopularActivity;
import projectseries.baqu.com.projectseries.view.MovieDetailActivity;
import projectseries.baqu.com.projectseries.view.RecommendedActivity;
import projectseries.baqu.com.projectseries.view.SearchActivity;
import projectseries.baqu.com.projectseries.view.SplashActivity;
import projectseries.baqu.com.projectseries.view.TopRatedActivity;

/**
 * Created by inlab on 26/01/2017.
 */

@Module
public class ViewModule {

    private MenuActivity menuView;

    private LoginActivity loginView;

    private MovieDetailActivity detailView;

    private SplashActivity splashView;

    private SearchActivity searchView;

    private ActorMovieListActivity acotrMovieView;

    public ViewModule(SplashActivity view) {
        this.splashView = view;
    }

    public ViewModule(LoginActivity view) {
        this.loginView = view;
    }

    public ViewModule(MovieDetailActivity view){
        detailView = view;
    }

    public ViewModule(MenuActivity view) {
        menuView = view;
    }

    public ViewModule(SearchActivity view) {
        searchView = view;
    }

    public ViewModule(ActorMovieListActivity view) {
        acotrMovieView = view;
    }

    @Provides
    SplashActivity providesSplashView(){
        return splashView;
    }

    @Provides
    DiscoverActivity providesMainView(){
        return (DiscoverActivity) menuView;
    }

    @Provides
    LoginActivity providesLoginView(){
        return loginView;
    }

    @Provides
    MovieDetailActivity providesMovieDetailView(){
        return detailView;
    }

    @Provides
    MostPopularActivity providesMostPopularView(){
        return (MostPopularActivity) menuView;
    }

    @Provides
    TopRatedActivity providesTopRatedView(){
        return (TopRatedActivity) menuView;
    }

    @Provides
    FavouritesActivity providesFavouritesView(){
        return (FavouritesActivity) menuView;
    }

    @Provides
    ActorMovieListActivity providesActorMovieListView(){
        return acotrMovieView;
    }

    @Provides
    RecommendedActivity providesRecommendedView(){
        return (RecommendedActivity) menuView;
    }

    @Provides
    SearchActivity providesSearchView(){
        return searchView;
    }

    @Provides
    MenuActivity providesMenuActivity(){
        return menuView;
    }

    @Provides
    DBDataSource provideDatabase() {
        return new FirebaseDataSource();
    }
}
