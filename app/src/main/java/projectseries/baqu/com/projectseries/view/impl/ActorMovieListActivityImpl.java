package projectseries.baqu.com.projectseries.view.impl;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import projectseries.baqu.com.projectseries.R;
import projectseries.baqu.com.projectseries.adapter.ActorFilmsRecyclerViewAdapter;
import projectseries.baqu.com.projectseries.dependencyinjection.App;
import projectseries.baqu.com.projectseries.dependencyinjection.activity.ActivityModule;
import projectseries.baqu.com.projectseries.dependencyinjection.view.ViewModule;
import projectseries.baqu.com.projectseries.entity.ActorFilmView;
import projectseries.baqu.com.projectseries.entity.ActorView;
import projectseries.baqu.com.projectseries.entity.FullActorDetailsView;
import projectseries.baqu.com.projectseries.entity.MediaView;
import projectseries.baqu.com.projectseries.entity.MovieView;
import projectseries.baqu.com.projectseries.entity.SerieView;
import projectseries.baqu.com.projectseries.presenter.ActorMovieListActivityPresenter;
import projectseries.baqu.com.projectseries.presenter.MovieDetailActivityPresenter;
import projectseries.baqu.com.projectseries.view.ActorMovieListActivity;

/**
 * Created by mbaque on 09/02/2017.
 */

public class ActorMovieListActivityImpl extends AppCompatActivity implements ActorMovieListActivity {

    @BindView(R.id.actor_image)
    ImageView actorImage;

    @BindView(R.id.actor_name)
    TextView actorName;

    @BindView(R.id.actor_place_birth)
    TextView placeBirth;

    @BindView(R.id.actor_biography)
    TextView biography;

    @BindView(R.id.actor_movies_recyclerview)
    RecyclerView filmsRecyclerView;

    @Inject
    ActorMovieListActivityPresenter presenter;
    private ActorView selectedActor;
    private int vibrantColor;
    private ProgressDialog progressDialog;

    public static void startThisActivity(MovieDetailActivityImpl context, ActorView actor, @ColorRes Integer color){
        Intent detailsIntent = new Intent(context, ActorMovieListActivityImpl.class);
        detailsIntent.putExtra("selectedActor", actor);
        detailsIntent.putExtra("vibrantColor", color);
        context.startActivity(detailsIntent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_actor_details);

        ((App) getApplication())
                .getComponent()
                .plus(new ActivityModule(this),
                        new ViewModule(this))
                .inject(this);

        ButterKnife.bind(this);

        this.selectedActor = (ActorView) getIntent().getSerializableExtra("selectedActor");
        this.vibrantColor = getIntent().getIntExtra("vibrantColor", R.color.colorPrimary);
        setTitle(selectedActor.getName());

        presenter.getActorDetails(selectedActor);

        FrameLayout frameToolbar = (FrameLayout) findViewById(R.id.toolbar_layout);
        LayoutInflater.from(this).inflate(R.layout.toolbar_core, frameToolbar);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(vibrantColor);
            toolbar.setBackgroundColor(vibrantColor);
        }
        setSupportActionBar(toolbar);
    }

    @Override
    public void onActorDetailsRetrieved(FullActorDetailsView actor){
        Picasso.with(this).load("http://image.tmdb.org/t/p/w154" + actor.getProfilePath()).into(actorImage);
        actorName.setText(actor.getName());
        placeBirth.setText(actor.getPlaceOfBirth());
        if(actor.getBiography()==null || actor.getBiography().equals("")){
            biography.setText("No hay una biografía disponible");
        }
        else {
            biography.setText(actor.getBiography());
        }

        LinearLayoutManager manager = new LinearLayoutManager(this);
        filmsRecyclerView.setLayoutManager(manager);
        filmsRecyclerView.setAdapter(new ActorFilmsRecyclerViewAdapter(this, actor.getCast()) {
            @Override
            public void onMovieSelected(ActorFilmView actorFilmView) {
                if(actorFilmView.getMediaType().equals("Tv")) {
                    MovieDetailActivityImpl.startThisActivity(ActorMovieListActivityImpl.this, new SerieView(actorFilmView.getId()));
                }
                else{
                    MovieDetailActivityImpl.startThisActivity(ActorMovieListActivityImpl.this, new MovieView(actorFilmView.getId()));
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onActivityStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.onActivityStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void goToLogin(){
        LoginActivityImpl.startThisActivity(this);
        finish();
    }

    @Override
    public void onError(String message) {
        hideLoading();
    }

    @Override
    public void showLoading(String message) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(message);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
    }

    @Override
    public void hideLoading() {
        if(progressDialog != null) {
            progressDialog.dismiss();
        }
    }
}
