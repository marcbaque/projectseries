package projectseries.baqu.com.projectseries.dependencyinjection;

import android.app.Application;
import android.support.annotation.VisibleForTesting;

import projectseries.baqu.com.projectseries.dependencyinjection.application.ApplicationComponent;
import projectseries.baqu.com.projectseries.dependencyinjection.application.ApplicationModule;
import projectseries.baqu.com.projectseries.dependencyinjection.application.DaggerApplicationComponent;

/**
 * Created by inlab on 26/01/2017.
 */

public class App extends Application {

    ApplicationComponent component = null;

    public ApplicationComponent getComponent() {
        return component;
    }

    @VisibleForTesting
    public void setComponent(ApplicationComponent component) {
        this.component = component;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        component = DaggerApplicationComponent
                .builder()
                .applicationModule(new ApplicationModule(this))
                .build();

        component.inject(this);
    }
}