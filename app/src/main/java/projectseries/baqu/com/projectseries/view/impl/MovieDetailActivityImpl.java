package projectseries.baqu.com.projectseries.view.impl;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import projectseries.baqu.com.projectseries.R;
import projectseries.baqu.com.projectseries.dependencyinjection.App;
import projectseries.baqu.com.projectseries.dependencyinjection.activity.ActivityModule;
import projectseries.baqu.com.projectseries.dependencyinjection.view.ViewModule;
import projectseries.baqu.com.projectseries.entity.MediaCommentsView;
import projectseries.baqu.com.projectseries.entity.MediaView;
import projectseries.baqu.com.projectseries.entity.SerieView;
import projectseries.baqu.com.projectseries.presenter.MovieDetailActivityPresenter;
import projectseries.baqu.com.projectseries.utils.FlingBehavior;
import projectseries.baqu.com.projectseries.view.MovieDetailActivity;
import projectseries.baqu.com.projectseries.view.fragment.MovieDetailCommentsFragment;
import projectseries.baqu.com.projectseries.view.fragment.MovieDetailInfoFragment;
import projectseries.baqu.com.projectseries.view.fragment.MovieDetailSeasonsFragment;

/**
 * Created by mbaque on 29/01/2017.
 */

public class MovieDetailActivityImpl extends AppCompatActivity implements MovieDetailActivity {

    @Inject
    MovieDetailActivityPresenter presenter;

    MediaView infoFragmentData;

    MediaCommentsView commentsFragmentData;

    @BindView(R.id.detail_background_image)
    ImageView backgroundImageView;

    @BindView(R.id.detail_collapsing_toolbar_layout)
    CollapsingToolbarLayout collapsingToolbarLayout;

    @BindView(R.id.details_fab_button)
    FloatingActionButton floatingActionButton;

    @BindView(R.id.trailer_fab)
    FloatingActionButton trailerFab;

    @BindView(R.id.movie_detail_tablayout)
    TabLayout tabLayout;

    ProgressDialog progressDialog;

    boolean isDataLoaded = false;

    private MediaView selectedItem;
    private Dialog dialog;
    private int vibrantColor;
    private MovieDetailInfoFragment infoFragment;
    private MovieDetailCommentsFragment commentsFragment;
    private MovieDetailSeasonsFragment seasonsFragment;


    public static void startThisActivity(Context context, MediaView media){
        Intent detailsIntent = new Intent(context, MovieDetailActivityImpl.class);
        detailsIntent.putExtra("selectedItem", media);
        context.startActivity(detailsIntent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_movie_detail);

        initDagger();

        ButterKnife.bind(this);

        this.selectedItem = (MediaView) getIntent().getSerializableExtra("selectedItem");

        setTitle(selectedItem.getTitle());

        presenter.onCreate(selectedItem);
        initView();

        initFragments();
    }

    private void initView() {
        FrameLayout frameToolbar = (FrameLayout) findViewById(R.id.toolbar_layout);
        LayoutInflater.from(this).inflate(R.layout.toolbar_core, frameToolbar);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        backgroundImageView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                if(backgroundImageView.getDrawable()!=null){
                    backgroundImageView.getViewTreeObserver().removeOnPreDrawListener(this);
                    Palette.from(((BitmapDrawable) backgroundImageView.getDrawable()).getBitmap()).generate(new Palette.PaletteAsyncListener() {
                        @Override
                        public void onGenerated(Palette palette) {
                            onColorGenerated(palette);
                        }
                    });
                    return true;
                }
                return false;
            }
        });

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                presenter.onFabClick(floatingActionButton.isSelected(), selectedItem);
            }
        });

        Picasso.with(this).load("http://image.tmdb.org/t/p/w500" + selectedItem.getBackdrop()).into(backgroundImageView);

        if(selectedItem instanceof SerieView){
            TabLayout.Tab seasonsTab = tabLayout.newTab().setText("Temporadas");
            tabLayout.addTab(seasonsTab, 2);
        }

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                switch(tab.getPosition()){
                    case 0:
                        transaction.replace(R.id.content_movie_detail, infoFragment, "info");
                        break;
                    case 1:
                        transaction.replace(R.id.content_movie_detail, commentsFragment, "comments");
                        break;
                    case 2:
                        transaction.replace(R.id.content_movie_detail, seasonsFragment, "seasons");
                }

                transaction.addToBackStack(null);
                transaction.commit();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        trailerFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTrailer();
            }
        });

//        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
//        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) appBarLayout.getLayoutParams();
//        params.setBehavior(new FlingBehavior(R.id.details_nested_scroll_view));
    }

    private void initFragments(){

        infoFragment = new MovieDetailInfoFragment();
        commentsFragment = new MovieDetailCommentsFragment();
        seasonsFragment = new MovieDetailSeasonsFragment();

        getMediaDetails();

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_movie_detail, infoFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void initDagger(){
        ((App) getApplication())
                .getComponent()
                .plus(new ActivityModule(this),
                        new ViewModule(this))
                .inject(this);
    }

    @Override
    public void showProgress(String message){
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(message);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
    }

    @Override
    public void hideProgress(){
        if(progressDialog!=null) {
            progressDialog.dismiss();
        }
    }

    private void onColorGenerated(Palette palette) {
        int defaultColor = ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary);
        vibrantColor = palette.getDarkVibrantColor(defaultColor);
        collapsingToolbarLayout.setStatusBarScrimColor(vibrantColor);
        collapsingToolbarLayout.setContentScrimColor(vibrantColor);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(vibrantColor);
        }
    }

    public MediaView getSelectedItem(){
        return this.selectedItem;
    }

    public void getMediaDetails(){
        presenter.getMediaDetails(selectedItem);

    }

    public void getComments(){
        presenter.getComments(infoFragmentData);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onActivityStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.onActivityStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onError(String error) {
        hideProgress();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Error");
        builder.setMessage(error);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                System.exit(0);
            }
        });
        builder.create().show();
    }

    @Override
    public void onMediaDetailsRetrieved(MediaView media){
        this.infoFragmentData = media;
        this.isDataLoaded = true;

        if(infoFragment!=null) {
            infoFragment.onDetailsRetrieved(media);
        }

        if(media instanceof SerieView && seasonsFragment!=null) {
            seasonsFragment.onDetailsRetrieved(media);
        }

        getComments();
        hideProgress();
        if(dialog!=null){
            showTrailer();
        }
    }

    @Override
    public void onCommentsListRetrieved(MediaCommentsView comments){
        this.commentsFragmentData = comments;
        if(commentsFragment != null) {
            commentsFragment.onCommentsListsRetrieved(comments);
        }
    }

    @Override
    public void goToLogin(){
        LoginActivityImpl.startThisActivity(this);
        finish();
    }

    private void showTrailer(){
        if(isDataLoaded) {
            if (infoFragmentData.getTrailerKey() != null) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v="+infoFragmentData.getTrailerKey())));
                hideProgress();
            } else {
                hideProgress();
                Toast.makeText(this, "No hay un trailer disponible", Toast.LENGTH_LONG).show();
            }
        }

        else{
            showProgress("Cargando video...");
        }
    }

    @Override
    public void markAsFavourite() {
        floatingActionButton.setImageDrawable(getResources().getDrawable(R.drawable.favourite_filled));
        floatingActionButton.setSelected(true);
    }

    @Override
    public void markAsNotFavourite() {
        floatingActionButton.setImageDrawable(getResources().getDrawable(R.drawable.favourite_not_filed));
        floatingActionButton.setSelected(false);
    }


    public Integer getVibrantColor() {
        return vibrantColor;
    }
}
