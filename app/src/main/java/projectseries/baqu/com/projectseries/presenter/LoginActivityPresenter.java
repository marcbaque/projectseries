package projectseries.baqu.com.projectseries.presenter;

import com.baqu.entity.UserParams;
import com.baqu.exception.ErrorBundle;
import com.baqu.usecase.NormalLoginUseCase;
import com.baqu.usecase.OnStartUseCase;
import com.baqu.usecase.OnStopUseCase;
import com.baqu.usecase.RegisterUseCase;
import com.google.firebase.auth.FirebaseAuthException;

import javax.inject.Inject;

import projectseries.baqu.com.projectseries.view.LoginActivity;

/**
 * Created by mbaque on 28/01/2017.
 */

public class LoginActivityPresenter {

    LoginActivity view;
    NormalLoginUseCase normalLoginUseCase;
    RegisterUseCase registerUseCase;
    OnStartUseCase onStartUseCase;
    OnStopUseCase onStopUseCase;

    @Inject
    public LoginActivityPresenter(LoginActivity view,
                                  NormalLoginUseCase normalLoginUseCase,
                                  RegisterUseCase registerUseCase,
                                  OnStartUseCase onStartUseCase,
                                  OnStopUseCase onStopUseCase) {
        this.view = view;
        this.normalLoginUseCase = normalLoginUseCase;
        this.registerUseCase = registerUseCase;
        this.onStartUseCase = onStartUseCase;
        this.onStopUseCase = onStopUseCase;
    }

    public void onActivityStart(){
        onStartUseCase.execute(null, new OnStartUseCase.UserListener() {
            @Override
            public void onSuccess(Void returnParam) {
                view.onLoginSuccess();
            }

            @Override
            public void onError(ErrorBundle errorBundle) {

            }
        });
        onStartUseCase.run();
    }

    public void onActivityStop(){
        onStopUseCase.execute(null, null);
        onStopUseCase.run();
    }

    public void login(String user, String pssw) {
        if(user == null || user.equals("")){
            view.onSoftError("Email no válido");
        }
        else if(pssw == null || pssw.equals("")){
            view.onSoftError("Contraseña no válida");
        }
        else {
            normalLoginUseCase.execute(new UserParams(user, pssw), new NormalLoginUseCase.LoginCallback() {
                @Override
                public void onSuccess(Void returnParam) {
                    view.hideLoading();
                    view.onLoginSuccess();
                }

                @Override
                public void onError(ErrorBundle errorBundle) {
                    if (errorBundle != null) {
                        if(errorBundle.getException() instanceof FirebaseAuthException){
                            view.onSoftError(errorBundle.getErrorMessage());
                        }
                        else {
                            view.onError(errorBundle.getErrorMessage());
                        }
                    }
                    view.hideLoading();
                }
            });
            normalLoginUseCase.run();
            view.showLoading("Comprobando credenciales...");
        }
    }

    public void register(String user, String pssw) {
        if(user == null || user.equals("")){
            view.onError("Email no válido");
        }
        else if(pssw == null || pssw.equals("")){
            view.onError("Contraseña no válida");
        }
        else {
            registerUseCase.execute(new UserParams(user, pssw), new RegisterUseCase.RegisterCallback() {
                @Override
                public void onSuccess(Void returnParam) {
                    view.hideLoading();
                    view.onLoginSuccess();
                }

                @Override
                public void onError(ErrorBundle errorBundle) {
                    if (errorBundle != null) {
                        view.onError(errorBundle.getErrorMessage());
                    }
                    view.hideLoading();
                }
            });
            registerUseCase.run();
            view.showLoading("Registrando...");
        }
    }
}
