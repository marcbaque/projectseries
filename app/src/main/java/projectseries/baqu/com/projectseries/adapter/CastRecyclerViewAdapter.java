package projectseries.baqu.com.projectseries.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import projectseries.baqu.com.projectseries.R;
import projectseries.baqu.com.projectseries.entity.ActorView;
import projectseries.baqu.com.projectseries.entity.CastView;

/**
 * Created by mbaque on 31/01/2017.
 */

public abstract class CastRecyclerViewAdapter extends RecyclerView.Adapter<CastRecyclerViewAdapter.ViewHolder> {

    private final Context context;
    private final CastView cast;


    public CastRecyclerViewAdapter(Context context, CastView cast) {
        this.context = context;
        this.cast = cast;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.movie_cast_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ActorView actor = cast.getCast().get(position);
        holder.actorName.setText(actor.getName());
        holder.actorCharacter.setText(actor.getCharacter());
        Picasso.with(context).load("http://image.tmdb.org/t/p/w154" + actor.getProfilePath()).placeholder(R.drawable.person_placeholder).into(holder.photo);

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onActorSelected(actor);
            }
        });
    }

    @Override
    public int getItemCount() {
        return cast.getCast().size();
    }

    public abstract void onActorSelected(ActorView actorView);

    public static class ViewHolder extends RecyclerView.ViewHolder{

        View view;

        @BindView(R.id.cast_actor_photo)
        ImageView photo;

        @BindView(R.id.cast_actor_name)
        TextView actorName;

        @BindView(R.id.cast_actor_character)
        TextView actorCharacter;

        public ViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;
            ButterKnife.bind(this, itemView);
        }
    }

}
