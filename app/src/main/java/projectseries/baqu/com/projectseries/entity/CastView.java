package projectseries.baqu.com.projectseries.entity;

import java.io.Serializable;
import java.util.List;

/**
 * Created by mbaque on 01/02/2017.
 */

public class CastView implements Serializable {

    List<ActorView> cast;

    public CastView(List<ActorView> cast) {
        this.cast = cast;
    }

    public List<ActorView> getCast() {
        return cast;
    }

    public void setCast(List<ActorView> cast) {
        this.cast = cast;
    }
}
