package projectseries.baqu.com.projectseries.presenter;

import com.baqu.entity.MediaList;
import com.baqu.exception.ErrorBundle;
import com.baqu.usecase.RecommendedListUseCase;

import javax.inject.Inject;

import projectseries.baqu.com.projectseries.mapper.MediaMapper;
import projectseries.baqu.com.projectseries.view.RecommendedActivity;

/**
 * Created by mbaque on 05/02/2017.
 */

public class RecommendedActivityPresenter {

    RecommendedListUseCase recommendedListUseCase;
    RecommendedActivity view;

    @Inject
    public RecommendedActivityPresenter(RecommendedActivity view, RecommendedListUseCase recommendedListUseCase) {
        this.view = view;
        this.recommendedListUseCase = recommendedListUseCase;
    }

    public void onCreateAdapter() {
        view.showLoading("Cargando...");
        recommendedListUseCase.execute(null, new RecommendedListUseCase.GetRecommendedListCallback() {
            @Override
            public void onSuccess(MediaList returnParam) {
                view.onListRetrieved(MediaMapper.toMediaListView(returnParam));
                view.hideLoading();
            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                if(errorBundle!=null) {
                    view.onError(errorBundle.getErrorMessage());
                }
                view.hideLoading();
            }
        });
        recommendedListUseCase.run();
    }

}
