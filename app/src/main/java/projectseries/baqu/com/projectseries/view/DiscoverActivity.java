package projectseries.baqu.com.projectseries.view;

import projectseries.baqu.com.projectseries.entity.MediaListView;
/**
 * Created by mbaque on 27/01/2017.
 */

public interface DiscoverActivity extends MenuActivity {

    void onListRetrieved(MediaListView list);

    void onError(String message);

    void onMoreDataLoaded(MediaListView mediaListView);
}
