package projectseries.baqu.com.projectseries.view.impl;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.lapism.searchview.SearchView;

import javax.inject.Inject;

import projectseries.baqu.com.projectseries.R;
import projectseries.baqu.com.projectseries.dependencyinjection.App;
import projectseries.baqu.com.projectseries.dependencyinjection.activity.ActivityModule;
import projectseries.baqu.com.projectseries.dependencyinjection.view.ViewModule;
import projectseries.baqu.com.projectseries.presenter.MenuActivityPresenter;
import projectseries.baqu.com.projectseries.view.MenuActivity;

/**
 * Created by mbaque on 28/01/2017.
 */

public class MenuActivityImpl extends AppCompatActivity implements MenuActivity {

    protected DrawerLayout mDrawerLayout;
    protected ActionBarDrawerToggle mDrawerToggle;
    protected NavigationView mNavigationView;
    protected ProgressDialog progressDialog;
    protected SearchView searchView;

    @Inject
    MenuActivityPresenter presenter;

    TextView usernameTextView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        ((App) getApplication())
                .getComponent()
                .plus(new ActivityModule(this),
                        new ViewModule(this))
                .inject(this);
    }

    protected void initView(){
        searchView = (SearchView) findViewById(R.id.searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                SearchActivityImpl.startThisActivity(MenuActivityImpl.this, query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        if (getToolbarLayout() != null) {
            FrameLayout frameToolbar = (FrameLayout) findViewById(R.id.toolbar_layout);
            LayoutInflater.from(this).inflate(getToolbarLayout(), frameToolbar);
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        generateMenu(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_search:
                searchView.open(true, item);
                return true;
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START); finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void generateMenu(Toolbar toolbar) {
        setSupportActionBar(toolbar);

        presenter.getUserName();

        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                   //host Activity
                mDrawerLayout,          //DrawerLayout object
                toolbar, R.string.app_name, R.string.app_name);
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        if (getNavigationViewLayout() != null) {
            FrameLayout navigationContent = (FrameLayout) findViewById(R.id.leftContent);
            LayoutInflater.from(this).inflate(getNavigationViewLayout(), navigationContent);
            mNavigationView = (NavigationView) findViewById(R.id.app_navigation_view);
            usernameTextView = (TextView) mNavigationView.getHeaderView(0).findViewById(R.id.navigation_drawer_username);
        }
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                return false;
            }
        });

        mDrawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                InputMethodManager inputMethodManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });

        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Intent intent;
                switch (item.getItemId()){
                    case R.id.menu_discover:
                        intent = new Intent(MenuActivityImpl.this, DiscoverActivityImpl.class);
                        startActivity(intent);
                        break;
                    case R.id.menu_populares:
                        intent = new Intent(MenuActivityImpl.this, MostPopularActivityImpl.class);
                        startActivity(intent);
                        break;
                    case R.id.menu_mejor_valoradas:
                        intent = new Intent(MenuActivityImpl.this, TopRatedActivityImpl.class);
                        startActivity(intent);
                        break;
                    case R.id.menu_recomendadas:
                        intent = new Intent(MenuActivityImpl.this, RecommendedActivityImpl.class);
                        startActivity(intent);
                        break;
                    case R.id.menu_favoritos:
                        intent = new Intent(MenuActivityImpl.this, FavouritesActivityImpl.class);
                        startActivity(intent);
                        break;
                    case R.id.menu_signout:
                        presenter.logoff();
                        intent = new Intent(MenuActivityImpl.this, LoginActivityImpl.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                        break;
                }
                return false;
            }
        });
    }

    @Override
    public void onUserNameRetrieved(String name) {
        usernameTextView.setText(name);
    }

    @Override
    public void onError(String message) {
        hideLoading();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Error");
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                System.exit(0);
            }
        });
        builder.create().show();
    }

    @Override
    public void goToLogin() {
        LoginActivityImpl.startThisActivity(this);
        finish();
    }

    @Override
    public void showLoading(String message) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(message);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
    }

    @Override
    public void hideLoading() {
        if(progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onActivityStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.onActivityStop();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    public Integer getToolbarLayout() {
        return R.layout.toolbar_core;
    }

    public Integer getToolbarMenu() {
        return R.menu.main_menu;
    }

    protected Integer getNavigationViewLayout() {
        return R.layout.app_navigation_view;
    }


}
