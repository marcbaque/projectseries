package projectseries.baqu.com.projectseries.entity;

import java.io.Serializable;

/**
 * Created by mbaque on 01/02/2017.
 */

public abstract class MediaView implements Serializable {

    public abstract String getPoster();
    public abstract String getRate();
    public abstract String getBackdrop();
    public abstract String getTitle();
    public abstract String getOverview();
    public abstract String getVoteCount();
    public abstract String getYear();
    public abstract MediaListView getSimilar();
    public abstract CastView getCredits();
    public abstract String getTrailerKey();

}
