package projectseries.baqu.com.projectseries.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mbaque on 01/02/2017.
 */

public class MediaListView  implements Serializable {

    private List<MediaView> mediaList;

    public MediaListView(List<MediaView> mediaList) {
        this.mediaList = mediaList;
    }

    public MediaListView() {
        this.mediaList = new ArrayList<>();
    }

    public List<MediaView> getMediaList() {
        return mediaList;
    }

    public void setMediaList(List<MediaView> mediaList) {
        this.mediaList = mediaList;
    }
}
