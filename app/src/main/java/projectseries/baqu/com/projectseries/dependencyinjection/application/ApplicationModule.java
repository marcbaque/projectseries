package projectseries.baqu.com.projectseries.dependencyinjection.application;

import android.content.Context;

import com.baqu.RepositoryInterface;
import com.baqu.executor.JobExecutor;
import com.baqu.executor.PostExecutionThread;
import com.baqu.executor.ThreadExecutor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import projectseries.baqu.com.data.AppRepository;
import projectseries.baqu.com.data.datasource.DBDataSource;
import projectseries.baqu.com.data.datasource.FilmDataSource;
import projectseries.baqu.com.data.datasource.FirebaseDataSource;
import projectseries.baqu.com.data.datasource.InternalStorageDataSource;
import projectseries.baqu.com.data.datasource.SharedPreferencesDataSource;
import projectseries.baqu.com.data.datasource.TMDBDataSource;
import projectseries.baqu.com.data.datasource.TvisoApi;
import projectseries.baqu.com.data.datasource.TvisoDataSource;
import projectseries.baqu.com.data.dependencyinjection.qualifier.ForApp;
import projectseries.baqu.com.projectseries.UIThread;
import projectseries.baqu.com.projectseries.dependencyinjection.App;

/**
 * Created by inlab on 26/01/2017.
 */
@Module
public class ApplicationModule {


    private final App app;

    public ApplicationModule(App app) {
        this.app = app;
    }

    @Provides
    @Singleton
    @ForApp
    public Context providesContext(){
        return app;
    }

    @Provides
    @Singleton
    public PostExecutionThread providePostExecutionThread() {
        return new UIThread();
    }

    @Provides
    @Singleton
    public ThreadExecutor provideThreadExecutor() {
        return new JobExecutor();
    }

    @Provides
    @Singleton
    public RepositoryInterface providesRepository(AppRepository repository){
        return repository;
    }


    @Provides
    @Singleton
    public DBDataSource providesDBDataSource(FirebaseDataSource apiDataSource){
        return apiDataSource;
    }

    @Provides
    @Singleton
    public FilmDataSource providesFilmDataSource(TMDBDataSource apiDataSource){
        return apiDataSource;
    }

    @Provides
    @Singleton
    public TvisoDataSource providesTvisoDataSource(TvisoApi apiDataSource){
        return apiDataSource;
    }

    @Provides
    @Singleton
    public InternalStorageDataSource providesInternalDataSource(SharedPreferencesDataSource dataSource){
        return dataSource;
    }
}
