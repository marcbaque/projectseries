package projectseries.baqu.com.projectseries.view.impl;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.Toast;

import javax.inject.Inject;

import projectseries.baqu.com.projectseries.R;
import projectseries.baqu.com.projectseries.dependencyinjection.App;
import projectseries.baqu.com.projectseries.dependencyinjection.activity.ActivityModule;
import projectseries.baqu.com.projectseries.dependencyinjection.view.ViewModule;
import projectseries.baqu.com.projectseries.presenter.LoginActivityPresenter;
import projectseries.baqu.com.projectseries.view.LoginActivity;
import projectseries.baqu.com.projectseries.view.fragment.DefaultLoginFragment;
import projectseries.baqu.com.projectseries.view.fragment.NormalLoginFragment;
import projectseries.baqu.com.projectseries.view.fragment.RegisterFragment;

/**
 * Created by mbaque on 28/01/2017.
 */

public class LoginActivityImpl extends AppCompatActivity implements LoginActivity{

    @Inject
    LoginActivityPresenter presenter;

    private ProgressDialog progressDialog;

    public static void startThisActivity(Context context){
        Intent intent = new Intent(context, LoginActivityImpl.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_base);

        ((App) getApplication())
                .getComponent()
                .plus(new ActivityModule(this),
                        new ViewModule(this))
                .inject(this);

        showDefaultFragment();
        setUpToolbar();

    }

    public void showNormalLoginFragment(){
        NormalLoginFragment normalLoginFragment = new NormalLoginFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.contentLayout, normalLoginFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void showRegisterFragment(){
        RegisterFragment registerFragment = new RegisterFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.contentLayout, registerFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void showDefaultFragment(){
        DefaultLoginFragment defaultLoginFragment = new DefaultLoginFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.contentLayout, defaultLoginFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void setUpToolbar(){
        FrameLayout frameToolbar = (FrameLayout) findViewById(R.id.toolbar_layout);
        LayoutInflater.from(this).inflate(getToolbarLayout(), frameToolbar);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

    }

    private Integer getToolbarLayout() {
        return R.layout.toolbar_core;
    }


    @Override
    protected void onStart() {
        super.onStart();
        presenter.onActivityStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.onActivityStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                showDefaultFragment();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLoginSuccess() {
        DiscoverActivityImpl.startThisActivity(this);
    }

    @Override
    public void onError(String message) {
        hideLoading();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Error");
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                System.exit(0);
            }
        });
        builder.create().show();
    }

    @Override
    public void showLoading(String message) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(message);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
    }

    @Override
    public void hideLoading() {
        if(progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onSoftError(String message) {
        hideLoading();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Error");
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    public void register(String email, String password){
        presenter.register(email, password);
    }

    public void normalLogin(String email, String password){
        presenter.login(email, password);
    }


}
