package projectseries.baqu.com.projectseries.view;

/**
 * Created by mbaque on 28/01/2017.
 */

public interface LoginActivity {
    void onLoginSuccess();

    void onError(String message);

    void showLoading(String message);

    void hideLoading();

    void onSoftError(String message);
}
