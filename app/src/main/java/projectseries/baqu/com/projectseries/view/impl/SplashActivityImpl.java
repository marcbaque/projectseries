package projectseries.baqu.com.projectseries.view.impl;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

import projectseries.baqu.com.projectseries.R;
import projectseries.baqu.com.projectseries.dependencyinjection.App;
import projectseries.baqu.com.projectseries.dependencyinjection.activity.ActivityModule;
import projectseries.baqu.com.projectseries.dependencyinjection.view.ViewModule;
import projectseries.baqu.com.projectseries.presenter.SplashActivityPresenter;
import projectseries.baqu.com.projectseries.view.SplashActivity;

/**
 * Created by mbaque on 04/02/2017.
 */

public class SplashActivityImpl extends AppCompatActivity implements SplashActivity {

    @Inject
    SplashActivityPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.splash_layout);

        ((App) getApplication())
                .getComponent()
                .plus(new ActivityModule(this),
                        new ViewModule(this))
                .inject(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onActivityStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.onActivityStop();
    }

    @Override
    public void userExists() {
        DiscoverActivityImpl.startThisActivity(this);
    }

    @Override
    public void goToLogin() {
        LoginActivityImpl.startThisActivity(this);
    }
}
