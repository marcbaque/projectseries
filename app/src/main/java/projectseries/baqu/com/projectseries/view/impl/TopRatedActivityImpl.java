package projectseries.baqu.com.projectseries.view.impl;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import projectseries.baqu.com.projectseries.R;
import projectseries.baqu.com.projectseries.adapter.MovieMainListRecyclerViewAdapter;
import projectseries.baqu.com.projectseries.dependencyinjection.App;
import projectseries.baqu.com.projectseries.dependencyinjection.activity.ActivityModule;
import projectseries.baqu.com.projectseries.dependencyinjection.view.ViewModule;
import projectseries.baqu.com.projectseries.entity.MediaListView;
import projectseries.baqu.com.projectseries.entity.MediaView;
import projectseries.baqu.com.projectseries.presenter.TopRatedActivityPresenter;
import projectseries.baqu.com.projectseries.utils.DataType;
import projectseries.baqu.com.projectseries.view.TopRatedActivity;

/**
 * Created by mbaque on 31/01/2017.
 */

public class TopRatedActivityImpl extends MenuActivityImpl implements TopRatedActivity {

    @Inject
    TopRatedActivityPresenter presenter;

    MovieMainListRecyclerViewAdapter adapter;

    @BindView(R.id.home_tablayout)
    TabLayout tabLayout;

    @BindView(R.id.home_movie_recyclerview)
    RecyclerView recyclerView;
    private boolean isListLoading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ((App) getApplication())
                .getComponent()
                .plus(new ActivityModule(this),
                        new ViewModule(this))
                .inject(this);

        ButterKnife.bind(this);

        initView();
    }

    protected void initView(){
        super.initView();
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch(tab.getPosition()){
                    case 0:
                        adapter.changeDataType(DataType.ALL);
                        break;
                    case 1:
                        adapter.changeDataType(DataType.MOVIE);
                        break;
                    case 2:
                        adapter.changeDataType(DataType.SERIES);
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        final GridLayoutManager manager = new GridLayoutManager(this, 2);

        recyclerView.setLayoutManager(manager);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if(dy > 0) //check for scroll down
                {
                    int visibleItemCount = manager.getChildCount();
                    int totalItemCount = manager.getItemCount();
                    int pastVisiblesItems = manager.findFirstVisibleItemPosition();

                    if (!isListLoading){
                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount){
                            isListLoading = true;
                            presenter.loadMoreData();
                        }
                    }
                }
            }
        });

        presenter.onCreateAdapter();

    }

    @Override
    public void onListRetrieved(MediaListView data) {
        adapter = new MovieMainListRecyclerViewAdapter(this, data) {
            @Override
            public void onMovieSelected(MediaView selectedItem) {
                Intent detailsIntent = new Intent(TopRatedActivityImpl.this, MovieDetailActivityImpl.class);
                detailsIntent.putExtra("selectedItem", selectedItem);
                startActivity(detailsIntent);
            }
        };

        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onMoreDataLoaded(MediaListView mediaListView) {
        adapter.loadMoreData(mediaListView);
        isListLoading = false;
    }

}
