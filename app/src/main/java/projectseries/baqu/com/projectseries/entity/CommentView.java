package projectseries.baqu.com.projectseries.entity;

import java.io.Serializable;
import java.util.List;

/**
 * Created by mbaque on 07/02/2017.
 */

public class CommentView implements Serializable {

    String id;

    TvisoUserView actor;

    String rating;

    String content;

    boolean isSpoiler;

    List<ReplyView> replies;

    String datePublished;

    public CommentView() {
    }

    public CommentView(String id, TvisoUserView actor, String rating, String content, boolean isSpoiler, List<ReplyView> replies, String datePublished) {
        this.id = id;
        this.actor = actor;
        this.rating = rating;
        this.content = content;
        this.isSpoiler = isSpoiler;
        this.replies = replies;
        this.datePublished = datePublished;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public TvisoUserView getActor() {
        return actor;
    }

    public void setActor(TvisoUserView actor) {
        this.actor = actor;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isSpoiler() {
        return isSpoiler;
    }

    public void setSpoiler(boolean spoiler) {
        isSpoiler = spoiler;
    }

    public List<ReplyView> getReplies() {
        return replies;
    }

    public void setReplies(List<ReplyView> replies) {
        this.replies = replies;
    }

    public String getDatePublished() {
        return datePublished;
    }

    public void setDatePublished(String datePublished) {
        this.datePublished = datePublished;
    }
}
