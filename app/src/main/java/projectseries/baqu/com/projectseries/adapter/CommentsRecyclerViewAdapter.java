package projectseries.baqu.com.projectseries.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import at.blogc.android.views.ExpandableTextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import projectseries.baqu.com.projectseries.R;
import projectseries.baqu.com.projectseries.entity.CommentView;
import projectseries.baqu.com.projectseries.entity.MediaCommentsView;

/**
 * Created by mbaque on 07/02/2017.
 */

public class CommentsRecyclerViewAdapter extends RecyclerView.Adapter<CommentsRecyclerViewAdapter.ViewHolder> {

    private Context context;
    private MediaCommentsView comments;

    public CommentsRecyclerViewAdapter(Context context, MediaCommentsView comments) {
        this.context = context;
        this.comments = comments;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.comments_list_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        CommentView comment = comments.getReviews().get(position);
        Picasso.with(context).load(comment.getActor().getImagePath()).noFade().placeholder(R.drawable.default_avatar).into(holder.autorImage);
        holder.autorName.setText(comment.getActor().getFullName());
        holder.content.setText(comment.getContent());


        try {
            @SuppressLint("SimpleDateFormat")
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'zZ'");
            Date date = df.parse(comment.getDatePublished());

            DateFormat finalDf = new SimpleDateFormat("dd-MM-yyyy");
            holder.date.setText(finalDf.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.content.isExpanded()){
                    holder.content.collapse();
                }
                else{
                    holder.content.expand();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return comments.getReviews().size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.comment_autor_image)
        CircleImageView autorImage;

        @BindView(R.id.comment_autor_name)
        TextView autorName;

        @BindView(R.id.comment_content)
        ExpandableTextView content;

        @BindView(R.id.comment_date)
        TextView date;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
