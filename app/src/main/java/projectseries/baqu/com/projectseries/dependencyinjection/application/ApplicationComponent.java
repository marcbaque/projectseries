package projectseries.baqu.com.projectseries.dependencyinjection.application;

import javax.inject.Singleton;

import dagger.Component;
import projectseries.baqu.com.projectseries.dependencyinjection.App;
import projectseries.baqu.com.projectseries.dependencyinjection.activity.ActivityComponent;
import projectseries.baqu.com.projectseries.dependencyinjection.activity.ActivityModule;
import projectseries.baqu.com.projectseries.dependencyinjection.view.ViewModule;

@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {

    void inject(App application);

    ActivityComponent plus(ActivityModule activityModule, ViewModule viewModule);

}
