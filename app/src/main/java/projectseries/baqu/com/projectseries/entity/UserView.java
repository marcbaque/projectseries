package projectseries.baqu.com.projectseries.entity;

/**
 * Created by mbaque on 01/02/2017.
 */
public class UserView {

    String name;

    String email;

    public UserView(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
