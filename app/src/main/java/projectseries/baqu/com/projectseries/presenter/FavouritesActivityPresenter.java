package projectseries.baqu.com.projectseries.presenter;

import com.baqu.entity.MediaList;
import com.baqu.exception.ErrorBundle;
import com.baqu.usecase.GetFavouritesUseCase;

import javax.inject.Inject;

import projectseries.baqu.com.projectseries.mapper.MediaMapper;
import projectseries.baqu.com.projectseries.view.FavouritesActivity;

/**
 * Created by mbaque on 04/02/2017.
 */

public class FavouritesActivityPresenter {

    FavouritesActivity view;
    GetFavouritesUseCase getFavouritesUseCase;

    @Inject
    public FavouritesActivityPresenter(FavouritesActivity view,
                                       GetFavouritesUseCase getFavouritesUseCase) {
        this.view = view;
        this.getFavouritesUseCase = getFavouritesUseCase;
    }

    public void onCreateAdapter() {
        view.showLoading("Cargando...");
        getFavouritesUseCase.execute(null, new GetFavouritesUseCase.GetFavouritesListCallback() {
            @Override
            public void onSuccess(MediaList returnParam) {
                view.onListRetrieved(MediaMapper.toMediaListView(returnParam));
                view.hideLoading();
            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                if(errorBundle!=null) {
                    view.onError(errorBundle.getErrorMessage());
                }
                view.hideLoading();
            }
        });
        getFavouritesUseCase.run();
    }
}
