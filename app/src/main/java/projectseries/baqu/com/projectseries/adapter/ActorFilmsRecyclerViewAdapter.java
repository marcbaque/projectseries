package projectseries.baqu.com.projectseries.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import projectseries.baqu.com.projectseries.R;
import projectseries.baqu.com.projectseries.entity.ActorFilmView;
import projectseries.baqu.com.projectseries.view.impl.ActorMovieListActivityImpl;

/**
 * Created by mbaque on 09/02/2017.
 */

public abstract class ActorFilmsRecyclerViewAdapter extends RecyclerView.Adapter<ActorFilmsRecyclerViewAdapter.ViewHolder> {


    private Context context;
    private List<ActorFilmView> cast;

    public ActorFilmsRecyclerViewAdapter(Context context, List<ActorFilmView> cast) {
        this.context = context;
        this.cast = cast;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.actor_film_list_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Picasso.with(context).load("http://image.tmdb.org/t/p/w154" + cast.get(position).getPosterPath()).placeholder(R.drawable.movie_placeholder).into(holder.moviePoster);
        holder.movieTitle.setText(cast.get(position).getTitle());
        holder.movieYear.setText(cast.get(position).getReleaseDate());
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onMovieSelected(cast.get(position));
            }
        });
    }

    public abstract void onMovieSelected(ActorFilmView actorFilmView);

    @Override
    public int getItemCount() {
        return cast.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{

        View view;

        @BindView(R.id.actor_movie_poster)
        ImageView moviePoster;

        @BindView(R.id.actor_movie_title)
        TextView movieTitle;

        @BindView(R.id.actor_movie_year)
        TextView movieYear;

        public ViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;
            ButterKnife.bind(this, itemView);
        }
    }
}
