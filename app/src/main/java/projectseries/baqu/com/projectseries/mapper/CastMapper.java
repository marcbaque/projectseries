package projectseries.baqu.com.projectseries.mapper;

import com.baqu.entity.Actor;
import com.baqu.entity.ActorFilm;
import com.baqu.entity.Cast;
import com.baqu.entity.FullActorDetails;

import java.util.ArrayList;
import java.util.List;

import projectseries.baqu.com.data.entity.tmdb.ActorFilmDTO;
import projectseries.baqu.com.projectseries.entity.ActorFilmView;
import projectseries.baqu.com.projectseries.entity.ActorView;
import projectseries.baqu.com.projectseries.entity.CastView;
import projectseries.baqu.com.projectseries.entity.FullActorDetailsView;

/**
 * Created by mbaque on 02/02/2017.
 */

public class CastMapper {

    public static ActorView toActorView(Actor actor){
        ActorView actorView = new ActorView(actor.getId(), actor.getName(), actor.getProfilePath(), actor.getCharacter());
        return actorView;
    }

    public static Actor toActor(ActorView actorView){
        Actor actor = new Actor(actorView.getId(), actorView.getName(), actorView.getProfilePath(), actorView.getCharacter());
        return actor;
    }

    public static CastView toCastView(Cast cast){
        List<ActorView> actors = new ArrayList<>();
        for(Actor actor : cast.getCast()){
            actors.add(toActorView(actor));
        }
        return new CastView(actors);
    }

    public static Cast toCast(CastView castView){
        List<Actor> actors = new ArrayList<>();
        if(castView != null && castView.getCast()!=null) {
            for (ActorView actor : castView.getCast()) {
                actors.add(toActor(actor));
            }
        }
        return new Cast(actors);
    }

    public static FullActorDetails toFullActorDetails(FullActorDetailsView fullActorDetailsView){
        return null;
    }

    public static FullActorDetailsView toFullActorDetailsView(FullActorDetails fullActorDetails){
        List<ActorFilmView> actorFilms = new ArrayList<>();

        for(ActorFilm actorFilmDTO : fullActorDetails.getCombinedCredits().getCast()){
            ActorFilmView actorFilm = new ActorFilmView(actorFilmDTO.getId(), actorFilmDTO.getTitle(), actorFilmDTO.getCharacter(), actorFilmDTO.getReleaseDate(), actorFilmDTO.getPosterPath(), actorFilmDTO.getMediaType());
            actorFilms.add(actorFilm);
        }

        FullActorDetailsView fullActorDetailsView = new FullActorDetailsView(fullActorDetails.getId(), fullActorDetails.getName(), fullActorDetails.getPlaceOfBirth(), fullActorDetails.getProfilePath(), fullActorDetails.getBiography(), actorFilms);

        return fullActorDetailsView;
    }

}
