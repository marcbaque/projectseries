package projectseries.baqu.com.projectseries.presenter;

import com.baqu.entity.Actor;
import com.baqu.entity.FullActorDetails;
import com.baqu.exception.ErrorBundle;
import com.baqu.usecase.GetActorDetailsUseCase;
import com.baqu.usecase.OnStartUseCase;
import com.baqu.usecase.OnStopUseCase;

import javax.inject.Inject;

import projectseries.baqu.com.projectseries.entity.ActorView;
import projectseries.baqu.com.projectseries.mapper.CastMapper;
import projectseries.baqu.com.projectseries.view.ActorMovieListActivity;

/**
 * Created by mbaque on 09/02/2017.
 */

public class ActorMovieListActivityPresenter {

    ActorMovieListActivity view;
    private OnStartUseCase onStartUseCase;
    private OnStopUseCase onStopUseCase;
    private GetActorDetailsUseCase actorDetailsUseCase;

    @Inject
    public ActorMovieListActivityPresenter(ActorMovieListActivity view,
                                           OnStartUseCase onStartUseCase,
                                           GetActorDetailsUseCase actorDetailsUseCase,
                                           OnStopUseCase onStopUseCase) {

        this.view = view;
        this.onStartUseCase = onStartUseCase;
        this.onStopUseCase = onStopUseCase;
        this.actorDetailsUseCase = actorDetailsUseCase;

    }

    public void getActorDetails(ActorView actor){
        view.showLoading("Cargando detalles...");
        actorDetailsUseCase.execute(CastMapper.toActor(actor), new GetActorDetailsUseCase.GetActorDetailsCallback(){
            @Override
            public void onSuccess(FullActorDetails returnParam) {
                view.onActorDetailsRetrieved(CastMapper.toFullActorDetailsView(returnParam));
                view.hideLoading();
            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                if(errorBundle!=null) {
                    view.onError(errorBundle.getErrorMessage());
                }
                view.hideLoading();
            }
        });
    }

    public void onActivityStart(){
        onStartUseCase.execute(null, new OnStartUseCase.UserListener() {
            @Override
            public void onSuccess(Void returnParam) {

            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                view.goToLogin();
            }
        });
        onStartUseCase.run();
    }

    public void onActivityStop(){
        onStopUseCase.execute(null, null);
        onStopUseCase.run();
    }
}
