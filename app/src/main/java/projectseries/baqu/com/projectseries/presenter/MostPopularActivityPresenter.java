package projectseries.baqu.com.projectseries.presenter;

import com.baqu.entity.MediaList;
import com.baqu.exception.ErrorBundle;
import com.baqu.usecase.MostPopularUseCase;

import javax.inject.Inject;

import projectseries.baqu.com.projectseries.mapper.MediaMapper;
import projectseries.baqu.com.projectseries.view.MostPopularActivity;

/**
 * Created by mbaque on 30/01/2017.
 */

public class MostPopularActivityPresenter {

    MostPopularUseCase mostPopularUseCase;
    MostPopularActivity view;
    int page = 1;

    @Inject
    public MostPopularActivityPresenter(MostPopularActivity view, MostPopularUseCase mostPopularUseCaseInterface) {
        this.view = view;
        this.mostPopularUseCase = mostPopularUseCaseInterface;
    }

    public void onCreateAdapter() {
        view.showLoading("Cargando...");
        mostPopularUseCase.execute(page, new MostPopularUseCase.GetMostPopularListCallback() {
            @Override
            public void onSuccess(MediaList returnParam) {
                view.onListRetrieved(MediaMapper.toMediaListView(returnParam));
                view.hideLoading();
            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                if(errorBundle!=null) {
                    view.onError(errorBundle.getErrorMessage());
                }
                view.hideLoading();
            }
        });
        mostPopularUseCase.run();
    }

    public void loadMoreData() {
        ++page;
        mostPopularUseCase.execute(page, new MostPopularUseCase.GetMostPopularListCallback() {
            @Override
            public void onSuccess(MediaList returnParam) {
                view.onMoreDataLoaded(MediaMapper.toMediaListView(returnParam));
            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                if(errorBundle!=null) {
                    view.onError(errorBundle.getErrorMessage());
                }
            }
        });
        mostPopularUseCase.run();
    }
}
