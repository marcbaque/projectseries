package projectseries.baqu.com.projectseries.presenter;

import com.baqu.entity.MediaList;
import com.baqu.exception.ErrorBundle;
import com.baqu.usecase.TopRatedListUseCase;

import javax.inject.Inject;

import projectseries.baqu.com.projectseries.mapper.MediaMapper;
import projectseries.baqu.com.projectseries.view.TopRatedActivity;

/**
 * Created by mbaque on 31/01/2017.
 */

public class TopRatedActivityPresenter {

    TopRatedActivity view;
    TopRatedListUseCase topRatedListUseCase;
    int page = 1;

    @Inject
    public TopRatedActivityPresenter(TopRatedActivity view,
                                     TopRatedListUseCase topRatedListUseCaseInterface) {
        this.view = view;
        this.topRatedListUseCase = topRatedListUseCaseInterface;
    }

    public void onCreateAdapter() {
        view.showLoading("Cargando...");
        topRatedListUseCase.execute(page, new TopRatedListUseCase.GetTopRatedListCallback() {
            @Override
            public void onSuccess(MediaList returnParam) {
                view.onListRetrieved(MediaMapper.toMediaListView(returnParam));
                view.hideLoading();
            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                if(errorBundle!=null) {
                    view.onError(errorBundle.getErrorMessage());
                }
                view.hideLoading();
            }
        });
        topRatedListUseCase.run();
    }

    public void loadMoreData() {
        ++page;
        topRatedListUseCase.execute(page, new TopRatedListUseCase.GetTopRatedListCallback() {
            @Override
            public void onSuccess(MediaList returnParam) {
                view.onMoreDataLoaded(MediaMapper.toMediaListView(returnParam));
            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                if(errorBundle!=null) {
                    view.onError(errorBundle.getErrorMessage());
                }
            }
        });
        topRatedListUseCase.run();
    }
}
