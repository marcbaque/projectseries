package projectseries.baqu.com.projectseries.view;

import projectseries.baqu.com.projectseries.entity.MediaListView;

/**
 * Created by mbaque on 05/02/2017.
 */

public interface RecommendedActivity extends MenuActivity {
    void onListRetrieved(MediaListView data);
}
