package projectseries.baqu.com.projectseries.view;

/**
 * Created by mbaque on 04/02/2017.
 */

public interface SplashActivity {
    void userExists();

    void goToLogin();
}
