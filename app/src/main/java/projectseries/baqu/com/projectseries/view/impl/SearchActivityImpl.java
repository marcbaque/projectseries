package projectseries.baqu.com.projectseries.view.impl;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import projectseries.baqu.com.projectseries.R;
import projectseries.baqu.com.projectseries.adapter.MovieMainListRecyclerViewAdapter;
import projectseries.baqu.com.projectseries.dependencyinjection.App;
import projectseries.baqu.com.projectseries.dependencyinjection.activity.ActivityModule;
import projectseries.baqu.com.projectseries.dependencyinjection.view.ViewModule;
import projectseries.baqu.com.projectseries.entity.MediaListView;
import projectseries.baqu.com.projectseries.entity.MediaView;
import projectseries.baqu.com.projectseries.presenter.SearchActivityPresenter;
import projectseries.baqu.com.projectseries.view.SearchActivity;

/**
 * Created by mbaque on 06/02/2017.
 */

public class SearchActivityImpl extends AppCompatActivity implements SearchActivity {

    @Inject
    SearchActivityPresenter presenter;

    @BindView(R.id.search_recyclerview)
    RecyclerView recyclerView;

    private MovieMainListRecyclerViewAdapter adapter;
    private ProgressDialog progressDialog;

    public static void startThisActivity(MenuActivityImpl context, String query){
        Intent searchIntent = new Intent(context, SearchActivityImpl.class);
        searchIntent.putExtra("query", query);
        context.startActivity(searchIntent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_search);

        ((App) getApplication())
                .getComponent()
                .plus(new ActivityModule(this),
                        new ViewModule(this))
                .inject(this);

        ButterKnife.bind(this);
        setTitle(getIntent().getStringExtra("query"));
        presenter.onCreate(getIntent().getStringExtra("query"));

        initView();

    }

    private void initView() {
        FrameLayout frameToolbar = (FrameLayout) findViewById(R.id.toolbar_layout);
        LayoutInflater.from(this).inflate(R.layout.toolbar_core, frameToolbar);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        GridLayoutManager manager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(manager);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onActivityStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.onActivityStop();
    }

    @Override
    public void onListRetrieved(MediaListView data) {
        adapter = new MovieMainListRecyclerViewAdapter(this, data) {
            @Override
            public void onMovieSelected(MediaView selectedItem) {
                MovieDetailActivityImpl.startThisActivity(SearchActivityImpl.this, selectedItem);
            }
        };

        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onError(String error) {
        hideLoading();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Error");
        builder.setMessage(error);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                System.exit(0);
            }
        });
        builder.create().show();
    }


    @Override
    public void goToLogin(){
        LoginActivityImpl.startThisActivity(this);
        finish();
    }

    @Override
    public void showLoading(String message) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(message);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();

    }

    @Override
    public void hideLoading() {
        if(progressDialog != null) {
            progressDialog.dismiss();
        }
    }
}
