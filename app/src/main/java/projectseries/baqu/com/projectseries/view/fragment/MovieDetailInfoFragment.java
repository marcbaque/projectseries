package projectseries.baqu.com.projectseries.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import projectseries.baqu.com.projectseries.R;
import projectseries.baqu.com.projectseries.adapter.CastRecyclerViewAdapter;
import projectseries.baqu.com.projectseries.adapter.SimilarRecyclerViewAdapter;
import projectseries.baqu.com.projectseries.entity.ActorView;
import projectseries.baqu.com.projectseries.entity.MediaView;
import projectseries.baqu.com.projectseries.view.impl.ActorMovieListActivityImpl;
import projectseries.baqu.com.projectseries.view.impl.MovieDetailActivityImpl;

/**
 * Created by mbaque on 07/02/2017.
 */

public class MovieDetailInfoFragment extends Fragment {

    @BindView(R.id.detail_title)
    TextView titleTextView;

    @BindView(R.id.detail_year)
    TextView yearTextView;

    @BindView(R.id.detail_overview)
    TextView overviewTextView;

    @BindView(R.id.detail_votecount)
    TextView votesTextView;

    @BindView(R.id.details_similar_recyclerview)
    RecyclerView similarRecyclerView;

    @BindView(R.id.details_similar_cast_recyclerview)
    RecyclerView castRecyclerView;

    SimilarRecyclerViewAdapter similarRecyclerViewAdapter;
    CastRecyclerViewAdapter castRecyclerViewAdapter;

    MovieDetailActivityImpl activity;

    private MediaView media;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (MovieDetailActivityImpl) context;

        this.media = activity.getSelectedItem();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.content_movie_info, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        MediaView selectedItem = activity.getSelectedItem();

        overviewTextView.setText(selectedItem.getOverview());
        titleTextView.setText(selectedItem.getTitle());
        yearTextView.setText(selectedItem.getYear());
        votesTextView.setText(selectedItem.getRate() + " (Votos : " + selectedItem.getVoteCount() + ")");

        if(media != null) {
            showSimilar();
            showCast();
        }
    }

    public void onDetailsRetrieved(MediaView media){
        this.media = media;
        showCast();
        showSimilar();
    }

    public void showSimilar() {
        if(media.getSimilar()!=null) {
            LinearLayoutManager manager = new LinearLayoutManager(activity);
            similarRecyclerView.setLayoutManager(manager);
            similarRecyclerViewAdapter = new SimilarRecyclerViewAdapter(activity, media.getSimilar()) {
                @Override
                public void onItemSelected(MediaView data) {
                    Intent intent = new Intent(activity, MovieDetailActivityImpl.class);
                    intent.putExtra("selectedItem", data);
                    startActivity(intent);
                }
            };
            similarRecyclerView.setAdapter(similarRecyclerViewAdapter);
        }
    }

    public void showCast() {
        if(media.getCredits()!=null) {
            LinearLayoutManager manager = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
            castRecyclerView.setLayoutManager(manager);
            castRecyclerViewAdapter = new CastRecyclerViewAdapter(activity, media.getCredits()) {
                @Override
                public void onActorSelected(ActorView actorView) {
                    ActorMovieListActivityImpl.startThisActivity(activity, actorView, activity.getVibrantColor());
                }
            };
            castRecyclerView.setAdapter(castRecyclerViewAdapter);
        }

    }
}
