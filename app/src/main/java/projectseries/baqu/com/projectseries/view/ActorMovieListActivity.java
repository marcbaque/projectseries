package projectseries.baqu.com.projectseries.view;

import projectseries.baqu.com.projectseries.entity.ActorView;
import projectseries.baqu.com.projectseries.entity.FullActorDetailsView;

/**
 * Created by mbaque on 09/02/2017.
 */

public interface ActorMovieListActivity {
    void onActorDetailsRetrieved(FullActorDetailsView actor);

    void goToLogin();

    void onError(String message);

    void showLoading(String message);

    void hideLoading();
}
