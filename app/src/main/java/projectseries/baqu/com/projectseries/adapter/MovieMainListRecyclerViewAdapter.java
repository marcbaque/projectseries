package projectseries.baqu.com.projectseries.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import projectseries.baqu.com.projectseries.R;
import projectseries.baqu.com.projectseries.entity.MediaListView;
import projectseries.baqu.com.projectseries.entity.MediaView;
import projectseries.baqu.com.projectseries.utils.DataType;
import projectseries.baqu.com.projectseries.utils.MediaListFilter;

/**
 * Created by mbaque on 27/01/2017.
 */

public abstract class MovieMainListRecyclerViewAdapter extends RecyclerView.Adapter<MovieMainListRecyclerViewAdapter.ViewHolder> {

    private final Context context;
    private MediaListView dataList;
    private MediaListView displayList;

    public MovieMainListRecyclerViewAdapter(Context context, MediaListView data) {
        this.context = context;
        this.dataList = data;
        this.displayList = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.home_movie_list_item,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.item.setVisibility(View.INVISIBLE);

        final MediaView data = displayList.getMediaList().get(position);

        Picasso.with(context).load("http://image.tmdb.org/t/p/w154" + data.getPoster()).placeholder(R.drawable.movie_placeholder).into(holder.posterImageView);
        holder.rateTextView.setText(data.getRate());
        holder.v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onMovieSelected(data);
            }
        });
        holder.progressBar.setVisibility(View.GONE);
        holder.item.setVisibility(View.VISIBLE);
    }

    public abstract void onMovieSelected(MediaView selectedItem);

    @Override
    public int getItemCount() {
        return displayList.getMediaList().size();
    }

    public void changeDataType(DataType type){
        displayList = MediaListFilter.filter(dataList, type);
        notifyDataSetChanged();
    }


    public void loadMoreData(MediaListView data){
        displayList.getMediaList().addAll(data.getMediaList());
        dataList.getMediaList().addAll(data.getMediaList());
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.home_movie_list_imageview)
        ImageView posterImageView;

        @BindView(R.id.home_movie_list_rate)
        TextView rateTextView;

        @BindView(R.id.home_movie_list_progressbar)
        ProgressBar progressBar;

        @BindView(R.id.home_movie_list_view)
        View item;

        View v;

        public ViewHolder(View itemView) {
            super(itemView);
            v = itemView;
            ButterKnife.bind(this,itemView);
        }
    }
}
