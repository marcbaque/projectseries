package projectseries.baqu.com.projectseries.presenter;

import com.baqu.entity.User;
import com.baqu.exception.ErrorBundle;
import com.baqu.usecase.LogOffUseCase;
import com.baqu.usecase.OnStartUseCase;
import com.baqu.usecase.OnStopUseCase;
import com.baqu.usecase.UserDetailsUseCase;

import javax.inject.Inject;

import projectseries.baqu.com.projectseries.view.MenuActivity;

/**
 * Created by mbaque on 01/02/2017.
 */

public class MenuActivityPresenter {

    LogOffUseCase logOffUseCase;
    MenuActivity view;
    UserDetailsUseCase userControlUseCase;
    OnStartUseCase onStartUseCase;
    OnStopUseCase onStopUseCase;

    @Inject
    public MenuActivityPresenter(MenuActivity view,
                                 UserDetailsUseCase userControlUseCase,
                                 LogOffUseCase logOffUseCase,
                                 OnStartUseCase onStartUseCase,
                                 OnStopUseCase onStopUseCase) {
        this.view = view;
        this.userControlUseCase = userControlUseCase;
        this.logOffUseCase = logOffUseCase;
        this.onStartUseCase = onStartUseCase;
        this.onStopUseCase = onStopUseCase;
    }

    public void onActivityStart(){
        onStartUseCase.execute(null, new OnStartUseCase.UserListener() {
            @Override
            public void onSuccess(Void returnParam) {

            }

            @Override
            public void onError(ErrorBundle errorBundle) {

            }
        });
        onStartUseCase.run();
    }

    public void onActivityStop(){
        onStopUseCase.execute(null, null);
        onStopUseCase.run();
    }

    public void getUserName(){
        userControlUseCase.execute(null, new UserDetailsUseCase.UserDetailsCallback() {
            @Override
            public void onSuccess(User returnParam) {
                view.onUserNameRetrieved(returnParam.getEmail());
            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                if(errorBundle!=null) {
                    view.onError(errorBundle.getErrorMessage());
                }
            }
        });
        userControlUseCase.run();
    }

    public void logoff() {
        view.showLoading("Saliendo...");
        logOffUseCase.execute(null, new LogOffUseCase.LogOffCallback() {
            @Override
            public void onSuccess(Void returnParam) {
                view.goToLogin();
            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                if(errorBundle!=null) {
                    view.onError(errorBundle.getErrorMessage());
                }
                view.hideLoading();
            }
        });
        logOffUseCase.run();
    }
}
