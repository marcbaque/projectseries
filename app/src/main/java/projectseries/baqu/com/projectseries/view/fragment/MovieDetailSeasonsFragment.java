package projectseries.baqu.com.projectseries.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import projectseries.baqu.com.projectseries.R;
import projectseries.baqu.com.projectseries.adapter.SeasonsRecyclerViewAdapter;
import projectseries.baqu.com.projectseries.entity.MediaView;
import projectseries.baqu.com.projectseries.entity.SerieView;
import projectseries.baqu.com.projectseries.view.impl.MovieDetailActivityImpl;

/**
 * Created by mbaque on 09/02/2017.
 */

public class MovieDetailSeasonsFragment extends Fragment {

    @BindView(R.id.content_movie_seasons_recyclerview)
    RecyclerView seasonsRecyclerView;

    MovieDetailActivityImpl activity;

    SerieView serieView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (MovieDetailActivityImpl) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.content_movie_seasons, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(serieView!=null) {
            setUpRecyclerView();
        }
    }

    private void setUpRecyclerView(){
        if(seasonsRecyclerView != null) {
            LinearLayoutManager manager = new LinearLayoutManager(activity);
            seasonsRecyclerView.setLayoutManager(manager);
            seasonsRecyclerView.setAdapter(new SeasonsRecyclerViewAdapter(activity, serieView.getSeasons()));
        }
    }

    public void onDetailsRetrieved(MediaView media) {
        this.serieView = (SerieView) media;
        setUpRecyclerView();
    }
}
