package projectseries.baqu.com.projectseries.entity;

/**
 * Created by mbaque on 09/02/2017.
 */
public class SeasonView {

    String episodes;

    String seasonNumber;

    String posterPath;

    public SeasonView() {
    }

    public SeasonView(String episodes, String seasonNumber, String posterPath) {
        this.episodes = episodes;
        this.seasonNumber = seasonNumber;
        this.posterPath = posterPath;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public String getEpisodes() {
        return episodes;
    }

    public void setEpisodes(String episodes) {
        this.episodes = episodes;
    }

    public String getSeasonNumber() {
        return seasonNumber;
    }

    public void setSeasonNumber(String seasonNumber) {
        this.seasonNumber = seasonNumber;
    }

}
