package projectseries.baqu.com.projectseries.view;

import projectseries.baqu.com.projectseries.entity.MediaListView;

/**
 * Created by mbaque on 04/02/2017.
 */

public interface FavouritesActivity extends MenuActivity {

    void onListRetrieved(MediaListView data);

}
