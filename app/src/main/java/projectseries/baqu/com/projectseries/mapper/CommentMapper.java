package projectseries.baqu.com.projectseries.mapper;

import com.baqu.entity.Comment;
import com.baqu.entity.MediaComments;
import com.baqu.entity.Reply;
import com.baqu.entity.TvisoUser;

import java.util.ArrayList;
import java.util.List;

import projectseries.baqu.com.projectseries.entity.CommentView;
import projectseries.baqu.com.projectseries.entity.MediaCommentsView;
import projectseries.baqu.com.projectseries.entity.ReplyView;
import projectseries.baqu.com.projectseries.entity.TvisoUserView;

/**
 * Created by mbaque on 07/02/2017.
 */

public class CommentMapper {

    public static MediaCommentsView toMediaCommentsView(MediaComments reviews){

        List<CommentView> comments = new ArrayList<CommentView>();

        for(Comment comment : reviews.getReviews()){
            comments.add(toCommentView(comment));
        }

        MediaCommentsView mediaComments = new MediaCommentsView(comments);
        return mediaComments;
    }

    private static CommentView toCommentView(Comment review){
        List<ReplyView> replies = new ArrayList<>();

        for(Reply reply : review.getReplies()){
            replies.add(toReplyView(reply));
        }

        CommentView comment = new CommentView(review.getId(), toTvisoUserView(review.getActor()), review.getRating(), review.getContent(), review.isSpoiler(), replies, review.getDatePublished());
        return comment;
    }

    private static ReplyView toReplyView(Reply replyDTO){
        ReplyView reply = new ReplyView(replyDTO.getId(), toTvisoUserView(replyDTO.getActor()), replyDTO.getContent(), replyDTO.getDatePublished());
        return reply;
    }

    private static TvisoUserView toTvisoUserView(TvisoUser actor){
        TvisoUserView user = new TvisoUserView(actor.getUid(), actor.getFullName(), actor.getImagePath());
        return user;
    }

}
