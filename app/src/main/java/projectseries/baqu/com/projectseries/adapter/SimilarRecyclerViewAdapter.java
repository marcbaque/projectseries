package projectseries.baqu.com.projectseries.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import projectseries.baqu.com.projectseries.R;
import projectseries.baqu.com.projectseries.entity.MediaListView;
import projectseries.baqu.com.projectseries.entity.MediaView;

/**
 * Created by mbaque on 29/01/2017.
 */

public abstract class SimilarRecyclerViewAdapter extends RecyclerView.Adapter<SimilarRecyclerViewAdapter.ViewHolder> {

    private final Context context;
    private final MediaListView data;

    public SimilarRecyclerViewAdapter(Context context, MediaListView data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.similar_recyclerview_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final MediaView itemData = data.getMediaList().get(position);

        String imagePath = itemData.getPoster();
        String title = itemData.getTitle();
        String year = itemData.getYear();

        Picasso.with(context).load("http://image.tmdb.org/t/p/w154" + imagePath).placeholder(R.drawable.movie_placeholder).into(holder.poster);
        if(title!=null) {
            holder.title.setText(title);
        }
        if(year!=null) {
            holder.year.setText(year.substring(0, 4));
        }
        holder.v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemSelected(itemData);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.getMediaList().size();
    }

    public abstract void onItemSelected(MediaView data);

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.detail_similar_poster)
        ImageView poster;

        @BindView(R.id.detail_similar_title)
        TextView title;

        @BindView(R.id.detail_similar_year)
        TextView year;

        View v;

        public ViewHolder(View itemView) {
            super(itemView);
            this.v = itemView;
            ButterKnife.bind(this, itemView);
        }
    }
}
