package projectseries.baqu.com.projectseries.presenter;

import com.baqu.exception.ErrorBundle;
import com.baqu.usecase.OnStartUseCase;
import com.baqu.usecase.OnStopUseCase;

import javax.inject.Inject;

import projectseries.baqu.com.projectseries.view.SplashActivity;

/**
 * Created by mbaque on 04/02/2017.
 */

public class SplashActivityPresenter {

    SplashActivity view;
    OnStartUseCase onStartUseCase;
    OnStopUseCase onStopUseCase;

    @Inject
    public SplashActivityPresenter(SplashActivity view,
                                   OnStartUseCase onStartUseCase,
                                   OnStopUseCase onStopUseCase) {
        this.view = view;
        this.onStartUseCase = onStartUseCase;
        this.onStopUseCase = onStopUseCase;
    }

    public void onActivityStart(){
        onStartUseCase.execute(null, new OnStartUseCase.UserListener() {
            @Override
            public void onSuccess(Void returnParam) {
                view.userExists();
            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                view.goToLogin();
            }
        });
        onStartUseCase.run();
    }

    public void onActivityStop(){
        onStopUseCase.execute(null, null);
        onStopUseCase.run();
    }

}
