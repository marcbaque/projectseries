package projectseries.baqu.com.projectseries.view;

import projectseries.baqu.com.projectseries.entity.MediaCommentsView;
import projectseries.baqu.com.projectseries.entity.MediaView;

/**
 * Created by mbaque on 29/01/2017.
 */

public interface MovieDetailActivity {

    void showProgress(String message);

    void hideProgress();

    void onError(String error);

    void onMediaDetailsRetrieved(MediaView media);

    void onCommentsListRetrieved(MediaCommentsView comments);

    void goToLogin();

    void markAsFavourite();

    void markAsNotFavourite();

}
