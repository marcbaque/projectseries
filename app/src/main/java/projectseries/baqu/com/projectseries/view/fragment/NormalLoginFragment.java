package projectseries.baqu.com.projectseries.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import projectseries.baqu.com.projectseries.R;
import projectseries.baqu.com.projectseries.view.impl.LoginActivityImpl;

/**
 * Created by mbaque on 04/02/2017.
 */

public class NormalLoginFragment extends Fragment {

    @BindView(R.id.normal_login_confirm_button)
    Button loginButton;

    @BindView(R.id.input_normal_login_email)
    TextView emailTextView;

    @BindView(R.id.input_normal_login_password)
    TextView passwordTextView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.normal_login_layout, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        ButterKnife.bind(this, view);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((LoginActivityImpl) getActivity()).normalLogin(emailTextView.getText().toString(), passwordTextView.getText().toString());
            }
        });
    }
}
