package projectseries.baqu.com.projectseries.entity;

/**
 * Created by mbaque on 09/02/2017.
 */
public class ActorFilmView {

    String id;

    String title;

    String character;

    String releaseDate;

    String posterPath;

    String mediaType;

    public ActorFilmView(String id, String title, String character, String releaseDate, String posterPath, String mediaType) {
        this.id = id;
        this.title = title;
        this.character = character;
        this.releaseDate = releaseDate;
        this.posterPath = posterPath;
        this.mediaType = mediaType;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }
}
