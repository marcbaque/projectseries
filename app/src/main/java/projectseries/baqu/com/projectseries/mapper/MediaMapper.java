package projectseries.baqu.com.projectseries.mapper;

import com.baqu.entity.Media;
import com.baqu.entity.MediaList;
import com.baqu.entity.Movie;
import com.baqu.entity.Season;
import com.baqu.entity.Serie;

import java.util.ArrayList;
import java.util.List;

import projectseries.baqu.com.data.entity.tmdb.SeasonDTO;
import projectseries.baqu.com.projectseries.entity.MediaListView;
import projectseries.baqu.com.projectseries.entity.MediaView;
import projectseries.baqu.com.projectseries.entity.MovieView;
import projectseries.baqu.com.projectseries.entity.SeasonView;
import projectseries.baqu.com.projectseries.entity.SerieView;

/**
 * Created by mbaque on 02/02/2017.
 */

public class MediaMapper {

    public static MediaList toMediaList(MediaListView mediaListView){
        List<Media> medias = new ArrayList<>();
        if(mediaListView!=null && mediaListView.getMediaList()!=null) {
            for (MediaView mediaView : mediaListView.getMediaList()) {
                medias.add(toMedia(mediaView));
            }
        }
        return new MediaList(medias);
    }

    public static MediaListView toMediaListView(MediaList mediaList){
        List<MediaView> medias = new ArrayList<>();
        if(mediaList != null && mediaList.getMediaList()!=null) {
            for (Media mediaView : mediaList.getMediaList()) {
                medias.add(toMediaView(mediaView));
            }
        }
        return new MediaListView(medias);
    }

    public static MediaView toMediaView(Media media){
        if(media instanceof Movie){
            Movie movie = (Movie) media;
            MovieView movieView = new MovieView(movie.getYear(), movie.isAdult(), movie.getTitle(), movie.getPoster(), movie.getRate(), movie.getPopularity(), movie.getOverview(), movie.getGenres(), movie.getId(), movie.getOriginalTitle(), movie.getOriginalLanguage(), movie.getBackdrop(), movie.getVoteCount(), movie.getImdbId(), CastMapper.toCastView(movie.getCredits()), toMediaListView(((Movie) media).getSimilar()), movie.getTrailerKey());
            return movieView;
        }
        else {
            Serie serie = (Serie) media;

            List<SeasonView> seasonViews = new ArrayList<>();
            for(Season season : serie.getSeasons()){
                seasonViews.add(toSeasonView(season));
            }

            SerieView serieView = new SerieView(serie.getYear(),serie.getTitle(), serie.getPoster(),serie.getRate(), serie.getPopularity(), serie.isAdult(), serie.getOverview(), serie.getGenres(), serie.getId(), serie.getOriginalTitle(), serie.getOriginalLanguage(), serie.getBackdrop(), serie.getVoteCount(), serie.getImdbId(), CastMapper.toCastView(serie.getCast()), toMediaListView(serie.getSimilar()), serie.getTrailerKey(), seasonViews);
            return serieView;
        }
    }

    public static Media toMedia(MediaView mediaView){
        if(mediaView instanceof MovieView){
            MovieView movie = (MovieView) mediaView;
            Movie movieView = new Movie(movie.getYear(), movie.isAdult(), movie.getTitle(), movie.getPoster(), movie.getRate(), movie.getPopularity(), movie.getOverview(), movie.getGenres(), movie.getId(), movie.getOriginalTitle(), movie.getOriginalLanguage(), movie.getBackdrop(), movie.getVoteCount(), movie.getImdbId(), CastMapper.toCast(movie.getCredits()), toMediaList(movie.getSimilar()), movie.getTrailerKey());
            return movieView;
        }
        else {
            SerieView serie = (SerieView) mediaView;

            List<Season> seasonViews = new ArrayList<>();
            for(SeasonView season : serie.getSeasons()){
                seasonViews.add(toSeason(season));
            }

            Serie serieView = new Serie(serie.getYear(),serie.getTitle(), serie.getPoster(),serie.getRate(), serie.getPopularity(), serie.isAdult(), serie.getOverview(), serie.getGenres(), serie.getId(), serie.getOriginalTitle(), serie.getOriginalLanguage(), serie.getBackdrop(), serie.getVoteCount(), serie.getImdbId(), CastMapper.toCast(serie.getCredits()), toMediaList(serie.getSimilar()), serie.getTrailerKey(), seasonViews);
            return serieView;
        }
    }

    public static SeasonView toSeasonView(Season seasonDTO){
        SeasonView season = new SeasonView(seasonDTO.getEpisodes(),seasonDTO.getSeasonNumber(), seasonDTO.getPosterPath());
        return season;
    }

    public static Season toSeason(SeasonView season){
        Season seasonDTO = new Season(season.getEpisodes(),season.getSeasonNumber(), season.getPosterPath());
        return seasonDTO;
    }

}
