package projectseries.baqu.com.projectseries.dependencyinjection.activity;

import dagger.Subcomponent;
import projectseries.baqu.com.data.dependencyinjection.scope.PerActivity;
import projectseries.baqu.com.projectseries.dependencyinjection.view.ViewModule;
import projectseries.baqu.com.projectseries.view.impl.ActorMovieListActivityImpl;
import projectseries.baqu.com.projectseries.view.impl.DiscoverActivityImpl;
import projectseries.baqu.com.projectseries.view.impl.FavouritesActivityImpl;
import projectseries.baqu.com.projectseries.view.impl.LoginActivityImpl;
import projectseries.baqu.com.projectseries.view.impl.MenuActivityImpl;
import projectseries.baqu.com.projectseries.view.impl.MostPopularActivityImpl;
import projectseries.baqu.com.projectseries.view.impl.MovieDetailActivityImpl;
import projectseries.baqu.com.projectseries.view.impl.RecommendedActivityImpl;
import projectseries.baqu.com.projectseries.view.impl.SearchActivityImpl;
import projectseries.baqu.com.projectseries.view.impl.SplashActivityImpl;
import projectseries.baqu.com.projectseries.view.impl.TopRatedActivityImpl;

/**
 * Created by inlab on 26/01/2017.
 */

@PerActivity
@Subcomponent(modules = {ActivityModule.class, ViewModule.class})
public interface ActivityComponent {

    void inject(MenuActivityImpl activity);

    void inject(LoginActivityImpl activity);

    void inject(MovieDetailActivityImpl activity);

    void inject(MostPopularActivityImpl activity);

    void inject(TopRatedActivityImpl activity);

    void inject(DiscoverActivityImpl activity);

    void inject(SplashActivityImpl activity);

    void inject(FavouritesActivityImpl activity);

    void inject(RecommendedActivityImpl activity);

    void inject(SearchActivityImpl activity);

    void inject(ActorMovieListActivityImpl activity);
}