package projectseries.baqu.com.projectseries.utils;

/**
 * Created by mbaque on 28/01/2017.
 */

public enum DataType {
    MOVIE, SERIES, ALL
}
