package projectseries.baqu.com.projectseries.utils;

import projectseries.baqu.com.projectseries.entity.MediaListView;
import projectseries.baqu.com.projectseries.entity.MediaView;
import projectseries.baqu.com.projectseries.entity.MovieView;
import projectseries.baqu.com.projectseries.entity.SerieView;

/**
 * Created by mbaque on 01/02/2017.
 */

public class MediaListFilter {

    public static MediaListView filter(MediaListView list, DataType type){
        MediaListView filteredList = new MediaListView();

        for(MediaView media:list.getMediaList()){
            switch (type){
                case ALL:
                    filteredList = list;
                    break;
                case MOVIE:
                    if(media instanceof MovieView) filteredList.getMediaList().add(media);
                    break;
                case SERIES:
                    if(media instanceof SerieView) filteredList.getMediaList().add(media);
                    break;
            }
        }

        return filteredList;

    }

}
