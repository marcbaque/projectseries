package projectseries.baqu.com.projectseries.presenter;

import com.baqu.entity.MediaList;
import com.baqu.exception.ErrorBundle;
import com.baqu.usecase.DiscoverUseCase;

import javax.inject.Inject;

import projectseries.baqu.com.projectseries.mapper.MediaMapper;
import projectseries.baqu.com.projectseries.view.DiscoverActivity;

/**
 * Created by mbaque on 27/01/2017.
 */

public class DiscoverActivityPresenter {

    DiscoverActivity view;
    DiscoverUseCase discoverUseCase;
    int page = 1;

    @Inject
    public DiscoverActivityPresenter(DiscoverActivity view, DiscoverUseCase discoverUseCase) {
        this.view = view;
        this.discoverUseCase = discoverUseCase;
    }

    public void onCreateAdapter() {
        view.showLoading("Cargando...");
        discoverUseCase.execute(page, new DiscoverUseCase.GetDiscoverListCallback() {
            @Override
            public void onSuccess(MediaList returnParam) {
                view.onListRetrieved(MediaMapper.toMediaListView(returnParam));
                view.hideLoading();
            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                if(errorBundle!=null) {
                    view.onError(errorBundle.getErrorMessage());
                }
                view.hideLoading();
            }
        });
        discoverUseCase.run();
    }

    public void loadMoreData() {
        ++page;
        discoverUseCase.execute(page, new DiscoverUseCase.GetDiscoverListCallback() {
            @Override
            public void onSuccess(MediaList returnParam) {
                view.onMoreDataLoaded(MediaMapper.toMediaListView(returnParam));
            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                if(errorBundle!=null) {
                    view.onError(errorBundle.getErrorMessage());
                }
            }
        });
        discoverUseCase.run();
    }
}
