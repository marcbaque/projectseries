package projectseries.baqu.com.projectseries.view;

import projectseries.baqu.com.projectseries.entity.MediaListView;

/**
 * Created by mbaque on 06/02/2017.
 */

public interface SearchActivity {
    void onListRetrieved(MediaListView data);

    void onError(String error);

    void goToLogin();

    void showLoading(String message);

    void hideLoading();
}
