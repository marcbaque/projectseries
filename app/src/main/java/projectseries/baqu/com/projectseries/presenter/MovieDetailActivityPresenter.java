package projectseries.baqu.com.projectseries.presenter;

import com.baqu.entity.Media;
import com.baqu.entity.MediaComments;
import com.baqu.exception.ErrorBundle;
import com.baqu.usecase.AddMediaToFavouritesUseCase;
import com.baqu.usecase.GetMediaCommentsUseCase;
import com.baqu.usecase.GetMovieDetailsUseCase;
import com.baqu.usecase.OnMediaVisitedUseCase;
import com.baqu.usecase.OnStartUseCase;
import com.baqu.usecase.OnStopUseCase;
import com.baqu.usecase.RemoveMediaFromFavouritesUseCase;

import javax.inject.Inject;

import projectseries.baqu.com.projectseries.entity.MediaView;
import projectseries.baqu.com.projectseries.mapper.CommentMapper;
import projectseries.baqu.com.projectseries.mapper.MediaMapper;
import projectseries.baqu.com.projectseries.view.MovieDetailActivity;

/**
 * Created by mbaque on 29/01/2017.
 */

public class MovieDetailActivityPresenter {

    RemoveMediaFromFavouritesUseCase removeMediaFromFavouritesUseCase;
    AddMediaToFavouritesUseCase addMediaToFavouritesUseCase;
    MovieDetailActivity view;
    OnStartUseCase onStartUseCase;
    OnStopUseCase onStopUseCase;
    OnMediaVisitedUseCase onMediaVisitedUseCase;
    GetMediaCommentsUseCase commentsUseCase;
    GetMovieDetailsUseCase getDetailsUseCase;

    @Inject
    public MovieDetailActivityPresenter(MovieDetailActivity view,
                                        OnMediaVisitedUseCase onMediaVisitedUseCase,
                                        GetMediaCommentsUseCase commentsUseCase,
                                        GetMovieDetailsUseCase getDetailsUseCase,
                                        AddMediaToFavouritesUseCase addMediaToFavouritesUseCase,
                                        RemoveMediaFromFavouritesUseCase removeMediaFromFavouritesUseCase,
                                        OnStartUseCase onStartUseCase,
                                        OnStopUseCase onStopUseCase) {
        this.view = view;
        this.onMediaVisitedUseCase = onMediaVisitedUseCase;
        this.getDetailsUseCase = getDetailsUseCase;
        this.commentsUseCase = commentsUseCase;
        this.onStartUseCase = onStartUseCase;
        this.onStopUseCase = onStopUseCase;
        this.addMediaToFavouritesUseCase = addMediaToFavouritesUseCase;
        this.removeMediaFromFavouritesUseCase = removeMediaFromFavouritesUseCase;
    }

    public void onActivityStart(){
        onStartUseCase.execute(null, new OnStartUseCase.UserListener() {
            @Override
            public void onSuccess(Void returnParam) {

            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                view.goToLogin();
            }
        });
        onStartUseCase.run();
    }

    public void onActivityStop(){
        onStopUseCase.execute(null, null);
        onStopUseCase.run();
    }

    public void getComments(MediaView data){
        commentsUseCase.execute(MediaMapper.toMedia(data), new GetMediaCommentsUseCase.GetCommentsCallback() {
            @Override
            public void onSuccess(MediaComments returnParam) {
                view.onCommentsListRetrieved(CommentMapper.toMediaCommentsView(returnParam));
            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                if(errorBundle!=null) {
                    view.onError(errorBundle.getErrorMessage());
                }
            }
        });
    }

    public void getMediaDetails(MediaView media){
        view.showProgress("Cargando detalles...");
        getDetailsUseCase.execute(MediaMapper.toMedia(media), new GetMovieDetailsUseCase.GetMovieDetailsCallback() {
            @Override
            public void onSuccess(Media returnParam) {
                view.onMediaDetailsRetrieved(MediaMapper.toMediaView(returnParam));
            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                if(errorBundle!=null) {
                    view.onError(errorBundle.getErrorMessage());
                }
                view.hideProgress();
            }
        });
    }

    public void onFabClick(boolean favourite, MediaView media) {
        if(favourite){
            view.showProgress("Desmarcando...");
            removeMediaFromFavouritesUseCase.execute(MediaMapper.toMedia(media), new RemoveMediaFromFavouritesUseCase.RemoveMediaFromFavourites(){

                @Override
                public void onSuccess(Void returnParam) {
                    view.markAsNotFavourite();
                    view.hideProgress();
                }

                @Override
                public void onError(ErrorBundle errorBundle) {
                    if(errorBundle!=null) {
                        view.onError(errorBundle.getErrorMessage());
                    }
                    view.hideProgress();
                }
            });
            removeMediaFromFavouritesUseCase.run();
        }
        else {
            view.showProgress("Marcando...");
            addMediaToFavouritesUseCase.execute(MediaMapper.toMedia(media), new AddMediaToFavouritesUseCase.AddMediaToFavourites() {

                @Override
                public void onSuccess(Void returnParam) {
                    view.markAsFavourite();
                    view.hideProgress();
                }

                @Override
                public void onError(ErrorBundle errorBundle) {
                    if(errorBundle!=null) {
                        view.onError(errorBundle.getErrorMessage());
                    }
                    view.hideProgress();
                }
            });
            addMediaToFavouritesUseCase.run();
        }
    }

    public void onCreate(MediaView media) {
        onMediaVisitedUseCase.execute(MediaMapper.toMedia(media), new OnMediaVisitedUseCase.OnMediaVisitedCallback() {
            @Override
            public void onSuccess(Boolean returnParam) {
                if(returnParam){
                    view.markAsFavourite();
                }
                else{
                    view.markAsNotFavourite();
                }
            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                if(errorBundle!=null) {
                    view.onError(errorBundle.getErrorMessage());
                }
                view.hideProgress();
            }
        });
        onMediaVisitedUseCase.run();
    }
}
