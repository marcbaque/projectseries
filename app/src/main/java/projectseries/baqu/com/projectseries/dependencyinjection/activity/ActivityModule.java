package projectseries.baqu.com.projectseries.dependencyinjection.activity;

import android.app.Activity;
import android.content.Context;

import dagger.Module;
import dagger.Provides;
import projectseries.baqu.com.data.dependencyinjection.qualifier.ForActivity;
import projectseries.baqu.com.data.dependencyinjection.scope.PerActivity;
import projectseries.baqu.com.projectseries.view.impl.ActorMovieListActivityImpl;
import projectseries.baqu.com.projectseries.view.impl.LoginActivityImpl;
import projectseries.baqu.com.projectseries.view.impl.MenuActivityImpl;
import projectseries.baqu.com.projectseries.view.impl.MovieDetailActivityImpl;
import projectseries.baqu.com.projectseries.view.impl.SearchActivityImpl;
import projectseries.baqu.com.projectseries.view.impl.SplashActivityImpl;

/**
 * Created by inlab on 26/01/2017.
 */

@Module
public class ActivityModule {

    private Activity activity;

    public ActivityModule(MenuActivityImpl activity){
        this.activity = activity;
    }

    public ActivityModule(LoginActivityImpl activity){
        this.activity = activity;
    }

    public ActivityModule(MovieDetailActivityImpl activity){
        this.activity = activity;
    }

    public ActivityModule(SplashActivityImpl activity){
        this.activity = activity;
    }

    public ActivityModule(SearchActivityImpl activity){
        this.activity = activity;
    }

    public ActivityModule(ActorMovieListActivityImpl activity){
        this.activity = activity;
    }



    @Provides
    @PerActivity
    @ForActivity
    Context providesContext(){
        return activity;
    }

}