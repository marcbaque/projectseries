package projectseries.baqu.com.projectseries.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import projectseries.baqu.com.projectseries.R;
import projectseries.baqu.com.projectseries.entity.SeasonView;

/**
 * Created by mbaque on 09/02/2017.
 */

public class SeasonsRecyclerViewAdapter extends RecyclerView.Adapter<SeasonsRecyclerViewAdapter.ViewHolder> {

    private Context context;
    private List<SeasonView> seasons;

    public SeasonsRecyclerViewAdapter(Context context, List<SeasonView> seasons) {
        this.context = context;
        this.seasons = seasons;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.seasons_list_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Picasso.with(context).load("http://image.tmdb.org/t/p/w154" + seasons.get(position).getPosterPath()).placeholder(R.drawable.movie_placeholder).into(holder.seasonPoster);
        holder.seasonNumber.setText("Temporada " + seasons.get(position).getSeasonNumber());
        holder.seasonEpisodesCount.setText(seasons.get(position).getEpisodes() + " episodios");
    }

    @Override
    public int getItemCount() {
        return seasons.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.season_poster)
        ImageView seasonPoster;

        @BindView(R.id.season_number)
        TextView seasonNumber;

        @BindView(R.id.season_episodes_count)
        TextView seasonEpisodesCount;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
