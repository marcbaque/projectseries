package projectseries.baqu.com.projectseries.entity;

import java.util.List;

/**
 * Created by mbaque on 09/02/2017.
 */

public class FullActorDetailsView {

    String id;

    String name;

    String placeOfBirth;

    String profilePath;

    String biography;

    List<ActorFilmView> cast;

    public FullActorDetailsView(String id, String name, String placeOfBirth, String profilePath, String biography, List<ActorFilmView> cast) {
        this.id = id;
        this.name = name;
        this.placeOfBirth = placeOfBirth;
        this.profilePath = profilePath;
        this.biography = biography;
        this.cast = cast;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public String getProfilePath() {
        return profilePath;
    }

    public void setProfilePath(String profilePath) {
        this.profilePath = profilePath;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public List<ActorFilmView> getCast() {
        return cast;
    }

    public void setCast(List<ActorFilmView> cast) {
        this.cast = cast;
    }
}
