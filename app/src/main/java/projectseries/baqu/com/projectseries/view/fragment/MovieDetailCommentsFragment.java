package projectseries.baqu.com.projectseries.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import projectseries.baqu.com.projectseries.R;
import projectseries.baqu.com.projectseries.adapter.CommentsRecyclerViewAdapter;
import projectseries.baqu.com.projectseries.entity.MediaCommentsView;
import projectseries.baqu.com.projectseries.entity.MediaView;
import projectseries.baqu.com.projectseries.view.impl.MovieDetailActivityImpl;

/**
 * Created by mbaque on 07/02/2017.
 */

public class MovieDetailCommentsFragment extends Fragment {

    MovieDetailActivityImpl activity;

    @BindView(R.id.comments_recyclerview)
    RecyclerView commentsList;

    private MediaCommentsView comments;
    private CommentsRecyclerViewAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (MovieDetailActivityImpl) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.comments_list, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(comments != null) {
            showComments();
        }
    }

    public void onCommentsListsRetrieved(MediaCommentsView comments){
        this.comments = comments;
        showComments();
    }

    private void showComments(){
        if(comments != null && commentsList!=null) {
            LinearLayoutManager manager = new LinearLayoutManager(activity);
            commentsList.setLayoutManager(manager);
            adapter = new CommentsRecyclerViewAdapter(activity, comments);
            commentsList.setAdapter(adapter);
        }
    }

}
