package projectseries.baqu.com.projectseries.entity;

import java.io.Serializable;

/**
 * Created by mbaque on 01/02/2017.
 */

public class ActorView implements Serializable {

    String id;
    String name;
    String profilePath;
    String character;

    public ActorView(String id, String name, String profilePath, String character) {
        this.id = id;
        this.name = name;
        this.profilePath = profilePath;
        this.character = character;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfilePath() {
        return profilePath;
    }

    public void setProfilePath(String profilePath) {
        this.profilePath = profilePath;
    }

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

}
