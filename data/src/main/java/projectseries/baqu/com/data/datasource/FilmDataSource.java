package projectseries.baqu.com.data.datasource;

import com.baqu.callback.DefaultCallback;

import java.util.List;

import projectseries.baqu.com.data.entity.UserDTO;
import projectseries.baqu.com.data.entity.tmdb.ActorDTO;
import projectseries.baqu.com.data.entity.tmdb.FullActorDetailsDTO;
import projectseries.baqu.com.data.entity.tmdb.MediaDTO;
import projectseries.baqu.com.data.entity.tmdb.MediaListDTO;

/**
 * Created by mbaque on 02/02/2017.
 */

public interface FilmDataSource {

    void searchMedia(String page, String query, String language, MediaListCallback listener);

    void getMovieDetails(String movieId, String language, String attach, MediaCallback listener);

    void getSerieDetails(String movieId, String language, String attach, MediaCallback listener);

    void getDiscover(String page, String language, MediaListCallback listener);

    void getMostPopular(String page, String language, String country, MediaListCallback listener);

    void getTopRated(String page, String language, String country, MediaListCallback listener);

    void getRecommended(List<Integer> genres, String minDate, String maxDate, UserDTO user, MediaListCallback listener);

    void getActorDetails(String actorId, String language, String attach, ActorCallback callback);

    interface MediaListCallback extends DefaultCallback<MediaListDTO> {}

    interface MediaCallback extends DefaultCallback<MediaDTO> {}

    interface ActorCallback extends DefaultCallback<FullActorDetailsDTO> {}
}
