package projectseries.baqu.com.data.entity.tmdb;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by mbaque on 28/01/2017.
 */

public class MediaListDTO {

    private MoviesDTO movies;
    private SeriesDTO series;

    public MediaListDTO() {
    }

    public MediaListDTO(MoviesDTO movies, SeriesDTO series) {
        this.movies = movies;
        this.series = series;
    }

    public MoviesDTO getMovies() {
        return movies;
    }

    public void setMovies(MoviesDTO movies) {
        this.movies = movies;
    }

    public void addData(MediaListDTO data){
        movies.addAll(data.getMovies());
        series.addAll(data.getSeries());
    }

    public void addMovies(MoviesDTO movies){
        movies.addAll(movies);
    }

    public void addSeries(SeriesDTO series){
        series.addAll(series);
    }

    public SeriesDTO getSeries() {
        return series;
    }

    public void setSeries(SeriesDTO series) {
        this.series = series;
    }

    public List<MediaDTO> shuffle(){
        List<MediaDTO> superList = new ArrayList<>();
        superList.addAll(movies.getMovies());
        superList.addAll(series.getSeries());
        Collections.shuffle(superList);
        return superList;
    }

    public List<MediaDTO> joinByRate(){
        List<MediaDTO> superList = new ArrayList<>();
        superList.addAll(movies.getMovies());
        superList.addAll(series.getSeries());
        Collections.sort(superList, new Comparator<MediaDTO>() {
            @Override
            public int compare(MediaDTO o1, MediaDTO o2) {
                Double rate1 = Double.valueOf(o1.getRate());
                Double rate2 = Double.valueOf(o2.getRate());
                return rate2.compareTo(rate1);
            }
        });
        return superList;
    }

    public int size(){
        return movies.getMovies().size() + series.getSeries().size();
    }
}
