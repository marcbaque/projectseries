package projectseries.baqu.com.data.entity.tmdb;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mbaque on 28/01/2017.
 */

public class TMDBUserDTO {

    int id;

    @SerializedName("iso_639_1")
    String language;

    @SerializedName("iso_3166_1")
    String country;

    String name;

    @SerializedName("include_adult")
    boolean adult;

    String username;

    public TMDBUserDTO(int id, String language, String country, String name, boolean adult, String username) {
        this.id = id;
        this.language = language;
        this.country = country;
        this.name = name;
        this.adult = adult;
        this.username = username;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isAdult() {
        return adult;
    }

    public void setAdult(boolean adult) {
        this.adult = adult;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
