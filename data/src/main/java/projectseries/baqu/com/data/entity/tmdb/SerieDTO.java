package projectseries.baqu.com.data.entity.tmdb;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mbaque on 28/01/2017.
 */

public class SerieDTO extends MediaDTO {

    @SerializedName("first_air_date")
    String year;

    @SerializedName("name")
    String title;

    @SerializedName("poster_path")
    String poster;

    @SerializedName("vote_average")
    String rate;

    @SerializedName("popularity")
    String popularity;

    @SerializedName("external_ids")
    ExternalIds externalIds;

    boolean adult;

    String overview;

    @SerializedName("genre_ids")
    List<Integer> genres;

    String id;

    @SerializedName("original_title")
    String originalTitle;

    @SerializedName("original_language")
    String originalLanguage;

    @SerializedName("backdrop_path")
    String backdrop;

    @SerializedName("vote_count")
    String voteCount;

    CastDTO credits;

    SeriesDTO similar;

    VideosDTO videos;

    List<SeasonDTO> seasons;

    public SerieDTO() {
    }

    public SerieDTO(String year, String title, String poster, String rate, String popularity, String imdbId, boolean adult, String overview, List<Integer> genres, String id, String originalTitle, String originalLanguage, String backdrop, String voteCount, CastDTO credits, SeriesDTO similar, VideosDTO videos, List<SeasonDTO> seasons) {
        this.year = year;
        this.title = title;
        this.poster = poster;
        this.rate = rate;
        this.popularity = popularity;
        this.externalIds = new ExternalIds(imdbId);
        this.adult = adult;
        this.overview = overview;
        this.genres = genres;
        this.id = id;
        this.originalTitle = originalTitle;
        this.originalLanguage = originalLanguage;
        this.backdrop = backdrop;
        this.voteCount = voteCount;
        this.credits = credits;
        this.similar = similar;
        this.videos = videos;
        this.seasons = seasons;
    }

    public List<SeasonDTO> getSeasons() {
        return seasons;
    }

    public void setSeasons(List<SeasonDTO> seasons) {
        this.seasons = seasons;
    }

    public VideosDTO getVideos() {
        return videos;
    }

    public void setVideos(VideosDTO videos) {
        this.videos = videos;
    }

    public CastDTO getCredits() {
        return credits;
    }

    public void setCredits(CastDTO credits) {
        this.credits = credits;
    }

    public MediaListDTO getSimilar() {
        return new MediaListDTO(null, similar);
    }

    public void setSimilar(SeriesDTO similar) {
        this.similar = similar;
    }

    public String getImdbId() {
        if(externalIds==null) return null;
        return externalIds.getImdbId();
    }

    public void setImdbId(String imdbId) {
        this.externalIds.setImdbId(imdbId);
    }

    public ExternalIds getExternalIds() {
        return externalIds;
    }

    public void setExternalIds(ExternalIds externalIds) {
        this.externalIds = externalIds;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getPopularity() {
        return popularity;
    }

    public void setPopularity(String popularity) {
        this.popularity = popularity;
    }

    public boolean isAdult() {
        return adult;
    }

    public void setAdult(boolean adult) {
        this.adult = adult;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public List<Integer> getGenres() {
        return genres;
    }

    public void setGenres(List<Integer> genres) {
        this.genres = genres;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public String getOriginalLanguage() {
        return originalLanguage;
    }

    public void setOriginalLanguage(String originalLanguage) {
        this.originalLanguage = originalLanguage;
    }

    public String getBackdrop() {
        return backdrop;
    }

    public void setBackdrop(String backdrop) {
        this.backdrop = backdrop;
    }

    public String getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(String voteCount) {
        this.voteCount = voteCount;
    }

    private class ExternalIds{
        @SerializedName("imdb_id")
        String imdbId;

        public ExternalIds() {
        }

        public ExternalIds(String imdbId) {
            this.imdbId = imdbId;
        }

        public String getImdbId() {
            return imdbId;
        }

        public void setImdbId(String imdbId) {
            this.imdbId = imdbId;
        }
    }
}
