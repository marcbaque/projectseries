package projectseries.baqu.com.data.entity.tmdb;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mbaque on 27/01/2017.
 */

public class MovieDTO extends MediaDTO {

    @SerializedName("release_date")
    String year;

    boolean adult;

    @SerializedName("title")
    String title;

    @SerializedName("poster_path")
    String poster;

    @SerializedName("vote_average")
    String rate;

    @SerializedName("imdb_id")
    String imdbId;

    @SerializedName("popularity")
    String popularity;

    String overview;

    @SerializedName("genre_ids")
    List<Integer> genres;

    String id;

    @SerializedName("original_title")
    String originalTitle;

    @SerializedName("original_language")
    String originalLanguage;

    @SerializedName("backdrop_path")
    String backdrop;

    @SerializedName("vote_count")
    String voteCount;

    CastDTO credits;

    MoviesDTO similar;

    VideosDTO videos;

    public MovieDTO() {
    }


    public MovieDTO(String year, boolean adult, String title, String poster, String rate, String imdbId, String popularity, String overview, List<Integer> genres, String id, String originalTitle, String originalLanguage, String backdrop, String voteCount, CastDTO credits, MoviesDTO similar, VideosDTO videos) {
        this.year = year;
        this.adult = adult;
        this.title = title;
        this.poster = poster;
        this.rate = rate;
        this.imdbId = imdbId;
        this.popularity = popularity;
        this.overview = overview;
        this.genres = genres;
        this.id = id;
        this.originalTitle = originalTitle;
        this.originalLanguage = originalLanguage;
        this.backdrop = backdrop;
        this.voteCount = voteCount;
        this.credits = credits;
        this.similar = similar;
        this.videos = videos;
    }

    public VideosDTO getVideos() {
        return videos;
    }

    public void setVideos(VideosDTO videos) {
        this.videos = videos;
    }

    public CastDTO getCredits() {
        return credits;
    }

    public void setCredits(CastDTO credits) {
        this.credits = credits;
    }

    public MediaListDTO getSimilar() {
        return new MediaListDTO(similar, null);
    }

    public void setSimilar(MoviesDTO similar) {
        this.similar = similar;
    }

    public String getImdbId() {
        return imdbId;
    }

    public void setImdbId(String imdbId) {
        this.imdbId = imdbId;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public boolean isAdult() {
        return adult;
    }

    public void setAdult(boolean adult) {
        this.adult = adult;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getPopularity() {
        return popularity;
    }

    public void setPopularity(String popularity) {
        this.popularity = popularity;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public List<Integer> getGenres() {
        return genres;
    }

    public void setGenres(List<Integer> genres) {
        this.genres = genres;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public String getOriginalLanguage() {
        return originalLanguage;
    }

    public void setOriginalLanguage(String originalLanguage) {
        this.originalLanguage = originalLanguage;
    }

    public String getBackdrop() {
        return backdrop;
    }

    public void setBackdrop(String backdrop) {
        this.backdrop = backdrop;
    }

    public String getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(String voteCount) {
        this.voteCount = voteCount;
    }
}
