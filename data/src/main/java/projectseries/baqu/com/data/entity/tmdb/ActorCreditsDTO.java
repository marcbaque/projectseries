package projectseries.baqu.com.data.entity.tmdb;

import java.util.List;

/**
 * Created by mbaque on 09/02/2017.
 */
public class ActorCreditsDTO {

    List<ActorFilmDTO> cast;

    public ActorCreditsDTO(List<ActorFilmDTO> cast) {
        this.cast = cast;
    }

    public List<ActorFilmDTO> getCast() {
        return cast;
    }

    public void setCast(List<ActorFilmDTO> cast) {
        this.cast = cast;
    }
}
