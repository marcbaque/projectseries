package projectseries.baqu.com.data.entity.tmdb;

/**
 * Created by mbaque on 28/01/2017.
 */

public class SessionDTO {

    boolean success;

    String sessionId;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}
