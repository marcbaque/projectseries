package projectseries.baqu.com.data.datasource;

import com.baqu.callback.DefaultCallback;

import projectseries.baqu.com.data.entity.tviso.MediaReviewsDTO;

/**
 * Created by mbaque on 07/02/2017.
 */

public interface TvisoDataSource {

    interface MediaReviewsCallback extends DefaultCallback<MediaReviewsDTO> {}

    void getMediaComments(String imdbId, MediaReviewsCallback callback);

}
