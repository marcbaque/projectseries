package projectseries.baqu.com.data.entity.tmdb;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mbaque on 31/01/2017.
 */

public class ActorDTO {

    String id;

    String name;

    @SerializedName("profile_path")
    String profilePath;

    String character;

    public ActorDTO(String id, String name, String profilePath, String character) {
        this.id = id;
        this.name = name;
        this.profilePath = profilePath;
        this.character = character;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfilePath() {
        return profilePath;
    }

    public void setProfilePath(String profilePath) {
        this.profilePath = profilePath;
    }

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }
}
