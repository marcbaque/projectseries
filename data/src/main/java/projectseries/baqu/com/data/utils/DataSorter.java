package projectseries.baqu.com.data.utils;

import com.baqu.entity.Media;
import com.baqu.entity.MediaList;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by mbaque on 04/02/2017.
 */

public class DataSorter {

    public static MediaList sortMediaList(MediaList mediaList, SortPolicy policy){
        MediaList result = null;
        switch (policy){
            case RANDOM:
                result = sortRandom(mediaList);
                break;
            case TOP_RATED:
                result = sortTopRated(mediaList);
                break;
            case MOST_POPULAR:
                result = sortMostPopular(mediaList);
                break;
        }
        return result;
    }

    private static MediaList sortRandom(MediaList mediaList){
        List<Media> list = mediaList.getMediaList();
        Collections.shuffle(list);
        return new MediaList(list);
    }

    private static MediaList sortTopRated(MediaList mediaList){
        List<Media> list = mediaList.getMediaList();
        Collections.sort(list, new Comparator<Media>() {
            @Override
            public int compare(Media o1, Media o2) {
                return o2.getRate().compareTo(o1.getRate());
            }
        });
        return new MediaList(list);
    }

    private static MediaList sortMostPopular(MediaList mediaList){
        List<Media> list = mediaList.getMediaList();
        Collections.sort(list, new Comparator<Media>() {
            @Override
            public int compare(Media o1, Media o2) {
                return o2.getPopularity().compareTo(o1.getPopularity());
            }
        });
        return new MediaList(list);
    }

    public enum SortPolicy { RANDOM, TOP_RATED, MOST_POPULAR }

}
