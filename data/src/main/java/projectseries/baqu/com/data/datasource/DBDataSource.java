package projectseries.baqu.com.data.datasource;

import com.baqu.callback.DefaultCallback;
import com.google.firebase.auth.FirebaseUser;

import projectseries.baqu.com.data.entity.UserDTO;
import projectseries.baqu.com.data.entity.tmdb.MediaDTO;

/**
 * Created by mbaque on 02/02/2017.
 */

public interface DBDataSource {

    void login(String user, String password, UserCallback callback);

    FirebaseUser getAppUser();

    void register(String user, String password, FirebaseUserCallback callback);

    void logoff();

    void onActivityStart(FirebaseUserCallback listener);

    void onActivityStop();

    UserDTO addMediaToFavourites(UserDTO user, MediaDTO mediaDTO);

    UserDTO removeMediaFromFavourites(UserDTO user, MediaDTO mediaDTO);

    void getUserDetails(UserDTO user, UserCallback callback);

    void getUserDetails(FirebaseUser user, UserCallback callback);

    void initializeUser(UserDTO user);

    void updateUser(UserDTO user);

    interface FirebaseUserCallback extends DefaultCallback<FirebaseUser> {}

    interface UserCallback extends DefaultCallback<UserDTO> {}


}
