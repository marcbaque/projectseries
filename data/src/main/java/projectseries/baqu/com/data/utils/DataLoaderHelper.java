package projectseries.baqu.com.data.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mbaque on 04/02/2017.
 */

public class DataLoaderHelper<TYPE1, TYPE2> {

    private List<TYPE1> type1List;
    private List<TYPE2> type2List;
    private int callNumber;

    private boolean onError;


    public DataLoaderHelper(int totalCallNumber) {
        this.type1List = new ArrayList<>();
        this.type2List = new ArrayList<>();
        this.callNumber = totalCallNumber;

        this.onError = false;
    }

    public void onFirstTypeDataLoaded(TYPE1 data){
        type1List.add(data);
    }

    public void onSecondTypeDataLoaded(TYPE2 data){
        type2List.add(data);
    }

    public boolean isDataLoaded(){
        return type1List.size()==callNumber && type2List.size()==callNumber;
    }

    public TYPE1 getFirstTypeData(int call){
        return this.type1List.get(call);
    }

    public TYPE2 getSecondTypeData(int call){
        return this.type2List.get(call);
    }

    public int getCallNumber(){
        return callNumber;
    }

    public void onError(){
        this.onError = true;
    }

    public boolean errorHappened(){
        return this.onError;
    }
}
