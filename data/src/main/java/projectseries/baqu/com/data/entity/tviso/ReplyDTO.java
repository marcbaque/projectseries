package projectseries.baqu.com.data.entity.tviso;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mbaque on 07/02/2017.
 */
public class ReplyDTO {

    String id;

    TvisoActor actor;

    String content;

    @SerializedName("published")
    String datePublished;

    public ReplyDTO() {
    }

    public ReplyDTO(String id, TvisoActor actor, String content, String datePublished) {
        this.id = id;
        this.actor = actor;
        this.content = content;
        this.datePublished = datePublished;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public TvisoActor getActor() {
        return actor;
    }

    public void setActor(TvisoActor actor) {
        this.actor = actor;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDatePublished() {
        return datePublished;
    }

    public void setDatePublished(String datePublished) {
        this.datePublished = datePublished;
    }
}
