package projectseries.baqu.com.data.mapper;

import com.baqu.entity.Media;
import com.baqu.entity.MediaList;
import com.baqu.entity.Movie;
import com.baqu.entity.Season;
import com.baqu.entity.Serie;

import java.util.ArrayList;
import java.util.List;

import projectseries.baqu.com.data.entity.tmdb.MediaDTO;
import projectseries.baqu.com.data.entity.tmdb.MediaListDTO;
import projectseries.baqu.com.data.entity.tmdb.MovieDTO;
import projectseries.baqu.com.data.entity.tmdb.MoviesDTO;
import projectseries.baqu.com.data.entity.tmdb.SeasonDTO;
import projectseries.baqu.com.data.entity.tmdb.SerieDTO;
import projectseries.baqu.com.data.entity.tmdb.SeriesDTO;
import projectseries.baqu.com.data.entity.tmdb.VideoDTO;
import projectseries.baqu.com.data.entity.tmdb.VideosDTO;

/**
 * Created by mbaque on 02/02/2017.
 */

public class DataMediaMapper {

    public static MediaList toMediaList(MediaListDTO mediaListDTO){
        List<Movie> movies = new ArrayList<>();
        if(mediaListDTO !=null && mediaListDTO.getMovies() != null) {
            for (MovieDTO movieDTO : mediaListDTO.getMovies().getMovies()) {
                movies.add(toMovie(movieDTO));
            }
        }

        List<Serie> series = new ArrayList<>();
        if(mediaListDTO != null && mediaListDTO.getSeries() != null) {
            for (SerieDTO serieDTO : mediaListDTO.getSeries().getSeries()) {
                series.add(toSerie(serieDTO));
            }
        }
        List<Media> media = new ArrayList<>();
        media.addAll(movies);
        media.addAll(series);
        return new MediaList(media);
    }

    public static Movie toMovie(MovieDTO movieDTO){
        String trailerKey = null;
        if(movieDTO.getVideos()!= null && movieDTO.getVideos().getResults() !=null) {
            for (VideoDTO video : movieDTO.getVideos().getResults()) {
                if(video.getSite().equals("YouTube") && video.getType().equals("Trailer")){
                    trailerKey = video.getKey();
                }
            }
        }

        Movie movie = new Movie(movieDTO.getYear(),movieDTO.isAdult(),movieDTO.getTitle(),movieDTO.getPoster(),movieDTO.getRate(), movieDTO.getPopularity(),movieDTO.getOverview(),movieDTO.getGenres(), movieDTO.getId(), movieDTO.getOriginalTitle(), movieDTO.getOriginalLanguage(), movieDTO.getBackdrop(), movieDTO.getVoteCount(), movieDTO.getImdbId(), DataCastMapper.toCast(movieDTO.getCredits()), toMediaList(movieDTO.getSimilar()), trailerKey);
        return movie;
    }

    public static Serie toSerie(SerieDTO serieDTO){
        String trailerKey = null;
        if(serieDTO.getVideos()!= null && serieDTO.getVideos().getResults() !=null) {
            for (VideoDTO video : serieDTO.getVideos().getResults()) {
                if(video.getSite().equals("YouTube") && video.getType().equals("Trailer")){
                    trailerKey = video.getKey();
                }
            }
        }

        List<Season> seasonDTOs = new ArrayList<>();
        if(serieDTO.getSeasons()!=null) {
            for (SeasonDTO season : serieDTO.getSeasons()) {
                seasonDTOs.add(toSeason(season));
            }
        }

        Serie serie = new Serie(serieDTO.getYear(),serieDTO.getTitle(),serieDTO.getPoster(), serieDTO.getRate(),serieDTO.getPopularity(), serieDTO.isAdult(),serieDTO.getOverview(),serieDTO.getGenres(), serieDTO.getId(), serieDTO.getOriginalTitle(), serieDTO.getOriginalLanguage(), serieDTO.getBackdrop(), serieDTO.getVoteCount(), serieDTO.getImdbId(), DataCastMapper.toCast(serieDTO.getCredits()), toMediaList(serieDTO.getSimilar()), trailerKey, seasonDTOs);
        return serie;
    }

    public static MediaListDTO toMediaListDTO(MediaList mediaList){
        MoviesDTO movies = new MoviesDTO(new ArrayList<MovieDTO>());
        SeriesDTO series = new SeriesDTO(new ArrayList<SerieDTO>());
        for(Media media : mediaList.getMediaList()){
            if(media instanceof Movie){
                movies.addMovie(toMovieDTO((Movie) media));
            }
            else{
                series.getSeries().add(toSerieDTO((Serie) media));
            }
        }

        MediaListDTO mediaListDTO = new MediaListDTO(movies, series);
        return mediaListDTO;
    }

    public static Media toMedia(MediaDTO mediaDTO){
        if(mediaDTO instanceof MovieDTO){
            return toMovie((MovieDTO) mediaDTO);
        }
        else if(mediaDTO instanceof SerieDTO){
            return toSerie((SerieDTO) mediaDTO);
        }
        return null;
    }

    public static MediaDTO toMediaDTO(Media media){
        if(media instanceof Movie){
            return toMovieDTO((Movie) media);
        }
        else if(media instanceof Serie){
            return toSerieDTO((Serie) media);
        }
        return null;
    }

    public static MovieDTO toMovieDTO(Movie movie){
        List<VideoDTO> list = new ArrayList<>();
        VideoDTO video = new VideoDTO("YouTube", movie.getTrailerKey(), "Trailer");
        list.add(video);
        VideosDTO videosDTO = new VideosDTO(list);


        MovieDTO movieDTO = new MovieDTO(movie.getYear(),movie.isAdult(),movie.getTitle(),movie.getPoster(),movie.getRate(), movie.getImdbId(), movie.getPopularity(),movie.getOverview(),movie.getGenres(), movie.getId(), movie.getOriginalTitle(), movie.getOriginalLanguage(), movie.getBackdrop(), movie.getVoteCount(), DataCastMapper.toCastDTO(movie.getCredits()), toMediaListDTO(movie.getSimilar()).getMovies(), videosDTO);
        return movieDTO;
    }

    public static Season toSeason(SeasonDTO seasonDTO){
        Season season = new Season(seasonDTO.getEpisodes(),seasonDTO.getSeasonNumber(), seasonDTO.getPosterPath());
        return season;
    }

    public static SeasonDTO toSeasonDTO(Season season){
        SeasonDTO seasonDTO = new SeasonDTO(season.getEpisodes(),season.getSeasonNumber(), season.getPosterPath());
        return seasonDTO;
    }

    public static SerieDTO toSerieDTO(Serie serie){
        List<VideoDTO> list = new ArrayList<>();
        VideoDTO video = new VideoDTO("YouTube", serie.getTrailerKey(), "Trailer");
        list.add(video);
        VideosDTO videosDTO = new VideosDTO(list);

        List<SeasonDTO> seasonDTOs = new ArrayList<>();
        for(Season season : serie.getSeasons()){
            seasonDTOs.add(toSeasonDTO(season));
        }

        SerieDTO serieDTO = new SerieDTO(serie.getYear(),serie.getTitle(),serie.getPoster(), serie.getRate(),serie.getPopularity(), serie.getImdbId(), serie.isAdult(),serie.getOverview(),serie.getGenres(), serie.getId(), serie.getOriginalTitle(), serie.getOriginalLanguage(), serie.getBackdrop(), serie.getVoteCount(), DataCastMapper.toCastDTO(serie.getCast()), toMediaListDTO(serie.getSimilar()).getSeries(), videosDTO, seasonDTOs);
        return serieDTO;
    }

}
