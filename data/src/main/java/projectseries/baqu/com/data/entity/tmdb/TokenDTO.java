package projectseries.baqu.com.data.entity.tmdb;


import com.google.gson.annotations.SerializedName;

/**
 * Created by mbaque on 28/01/2017.
 */

public class TokenDTO {

    boolean success;

    @SerializedName("request_token")
    String requestToken;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getRequestToken() {
        return requestToken;
    }

    public void setRequestToken(String requestToken) {
        this.requestToken = requestToken;
    }
}
