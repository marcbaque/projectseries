package projectseries.baqu.com.data.entity.tmdb;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mbaque on 27/01/2017.
 */

public class MoviesDTO{

    @SerializedName("results")
    List<MovieDTO> movies;

    public MoviesDTO() {
    }

    public MoviesDTO(List<MovieDTO> movies) {
        this.movies = movies;
    }

    public List<MovieDTO> getMovies() {
        return movies;
    }

    public void setMovies(List<MovieDTO> movies) {
        this.movies = movies;
    }

    public void addMovie(MovieDTO movie){
        movies.add(movie);
    }

    public int size(){
        return movies.size();
    }

    public void addAll(MoviesDTO list){
        movies.addAll(list.getMovies());
    }

    public MovieDTO getMovie(int position){
        return movies.get(position);
    }

    public void setMovie(int position, MovieDTO movie){
        movies.set(position, movie);
    }
}
