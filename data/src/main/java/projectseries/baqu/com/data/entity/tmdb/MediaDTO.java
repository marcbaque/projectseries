package projectseries.baqu.com.data.entity.tmdb;

import java.io.Serializable;
import java.util.List;

/**
 * Created by mbaque on 28/01/2017.
 */

public abstract class MediaDTO implements Serializable {
    public abstract String getPoster();
    public abstract String getRate();
    public abstract String getBackdrop();
    public abstract String getTitle();
    public abstract String getOverview();
    public abstract String getVoteCount();
    public abstract String getYear();
    public abstract String getPopularity();
    public abstract String getImdbId();
    public abstract List<Integer> getGenres();
    public abstract MediaListDTO getSimilar();
    public abstract CastDTO getCredits();
    public abstract VideosDTO getVideos();
}
