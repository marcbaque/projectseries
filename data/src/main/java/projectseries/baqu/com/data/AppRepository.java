package projectseries.baqu.com.data;

import com.baqu.RepositoryInterface;
import com.baqu.entity.Actor;
import com.baqu.entity.Media;
import com.baqu.entity.MediaList;
import com.baqu.entity.Movie;
import com.baqu.entity.Serie;
import com.baqu.exception.ErrorBundle;
import com.google.firebase.auth.FirebaseUser;

import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import projectseries.baqu.com.data.datasource.DBDataSource;
import projectseries.baqu.com.data.datasource.FilmDataSource;
import projectseries.baqu.com.data.datasource.InternalStorageDataSource;
import projectseries.baqu.com.data.datasource.TvisoDataSource;
import projectseries.baqu.com.data.entity.UserDTO;
import projectseries.baqu.com.data.entity.tmdb.ActorDTO;
import projectseries.baqu.com.data.entity.tmdb.FullActorDetailsDTO;
import projectseries.baqu.com.data.entity.tmdb.MediaDTO;
import projectseries.baqu.com.data.entity.tmdb.MediaListDTO;
import projectseries.baqu.com.data.entity.tmdb.MovieDTO;
import projectseries.baqu.com.data.entity.tmdb.SerieDTO;
import projectseries.baqu.com.data.entity.tviso.MediaReviewsDTO;
import projectseries.baqu.com.data.mapper.DataCastMapper;
import projectseries.baqu.com.data.mapper.DataCommentsMapper;
import projectseries.baqu.com.data.mapper.DataMediaMapper;
import projectseries.baqu.com.data.mapper.DataUserMapper;
import projectseries.baqu.com.data.utils.DataSorter;

/**
 * Created by mbaque on 02/02/2017.
 */

public class AppRepository implements RepositoryInterface {

    InternalStorageDataSource internalDataStore;

    DBDataSource dbDataSource;

    FilmDataSource filmDataSource;

    TvisoDataSource tvisoDataSource;

    @Inject
    public AppRepository(DBDataSource dbDataSource, FilmDataSource filmDataSource, TvisoDataSource tvisoDataSource, InternalStorageDataSource internalDataStore) {
        this.dbDataSource = dbDataSource;
        this.filmDataSource = filmDataSource;
        this.tvisoDataSource = tvisoDataSource;
        this.internalDataStore = internalDataStore;
    }

    @Override
    public void onActivityStart(final VoidCallback userListener) {
        DBDataSource.FirebaseUserCallback listener = new DBDataSource.FirebaseUserCallback() {
            @Override
            public void onSuccess(FirebaseUser user) {
                userListener.onSuccess(null);
            }

            @Override
            public void onError(ErrorBundle bundle) {
                userListener.onError(bundle);
            }
        };
        dbDataSource.onActivityStart(listener);
        if(dbDataSource.getAppUser() == null){
            listener.onError(null);
            return;
        }
    }

    @Override
    public void onActivityStop() {
        dbDataSource.onActivityStop();
    }

    @Override
    public void getRecommendedList(final MediaListCallback callback) {
        UserDTO user = internalDataStore.getUser();

        String maxDate, minDate;

        List<Integer> yearList = user.getRecommendationData().recommendedYears();
        if(yearList.size()<2){
            if(yearList.size()==1){
                maxDate = ((Integer) (yearList.get(0) + 9)).toString() + "-12-31";
                minDate = ((Integer) (yearList.get(0))).toString() + "-01-01";
            }
            else{
                maxDate = ((Integer) Calendar.getInstance().get(Calendar.YEAR)).toString() + "-12-31";
                minDate = ((Integer) 1900).toString() + "-01-01";
            }
        }
        else {
            if (yearList.get(0) > yearList.get(1)) {
                maxDate = ((Integer) (yearList.get(0) + 9)).toString() + "-12-31";
                minDate = ((Integer) (yearList.get(1))).toString() + "-01-01";
            } else {
                maxDate = ((Integer) (yearList.get(1) + 9)).toString() + "-12-31";
                minDate = ((yearList.get(0))).toString() + "-01-01";
            }
        }

        FilmDataSource.MediaListCallback listener = new FilmDataSource.MediaListCallback() {
            @Override
            public void onSuccess(MediaListDTO returnParam) {
                callback.onSuccess(DataMediaMapper.toMediaList(returnParam));
            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                callback.onError(errorBundle);
            }
        };

        filmDataSource.getRecommended(user.getRecommendationData().recommendedGenres(), minDate, maxDate, user, listener);
    }

    @Override
    public void searchMedia(String query, final MediaListCallback dataCallback) {
        UserDTO user = internalDataStore.getUser();

        FilmDataSource.MediaListCallback listener = new FilmDataSource.MediaListCallback() {
            @Override
            public void onSuccess(MediaListDTO returnParam) {
                dataCallback.onSuccess(DataMediaMapper.toMediaList(returnParam));
            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                dataCallback.onError(errorBundle);
            }
        };
        filmDataSource.searchMedia("1", query, user.getLanguage(), listener);
    }

    @Override
    public void onMediaVisited(Media media, BooleanCallback dataCallback) {
        UserDTO user = internalDataStore.getUser();
        user.getRecommendationData().newMediaViewed(DataMediaMapper.toMediaDTO(media));
        internalDataStore.storeUser(user);
        dbDataSource.updateUser(user);



        if(media instanceof Movie){
            if(user.getFavourites() != null && user.getFavourites().getMovies()!=null) {
                for (MovieDTO movie : user.getFavourites().getMovies().getMovies()) {
                    if (movie.getId().equals(((Movie) media).getId())) {
                        dataCallback.onSuccess(true);
                        return;
                    }
                }
            }
        }
        else if(media instanceof Serie){
            if(user.getFavourites() != null && user.getFavourites().getSeries()!=null) {
                for (SerieDTO serie : user.getFavourites().getSeries().getSeries()) {
                    if (serie.getId().equals(((Serie) media).getId())) {
                        dataCallback.onSuccess(true);
                        return;
                    }
                }
            }
        }
        dataCallback.onSuccess(false);
    }

    @Override
    public void getDiscover(int page, final MediaListCallback callback) {
            FilmDataSource.MediaListCallback listener = new FilmDataSource.MediaListCallback() {
                @Override
                public void onSuccess(MediaListDTO returnParam) {
                    MediaList mediaList = DataMediaMapper.toMediaList(returnParam);
                    callback.onSuccess(DataSorter.sortMediaList(mediaList, DataSorter.SortPolicy.RANDOM));
                }

                @Override
                public void onError(ErrorBundle errorBundle) {
                    callback.onError(errorBundle);
                }
            };
            filmDataSource.getDiscover(Integer.toString(page), internalDataStore.getUser().getLanguage(), listener);
    }

    @Override
    public void normalLogin(String user, String password, final VoidCallback callback) {
        DBDataSource.UserCallback userCallback = new DBDataSource.UserCallback() {
            @Override
            public void onSuccess(UserDTO returnParam) {
                internalDataStore.storeUser(returnParam);
                callback.onSuccess(null);
            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                callback.onError(errorBundle);
            }
        };
        dbDataSource.login(user, password, userCallback);
    }

    @Override
    public void register(String email, String password, final VoidCallback callback) {
        DBDataSource.FirebaseUserCallback userCallback = new DBDataSource.FirebaseUserCallback() {
            @Override
            public void onSuccess(FirebaseUser returnParam) {
                UserDTO user = new UserDTO(returnParam, internalDataStore.getGenres());
                dbDataSource.initializeUser(user);
                internalDataStore.storeUser(user);
                callback.onSuccess(null);
            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                callback.onError(errorBundle);
            }
        };
        dbDataSource.register(email, password, userCallback);
    }

    @Override
    public void getMediaDetails(Media media, final MediaCallback callback) {
        UserDTO user = internalDataStore.getUser();
        if(media instanceof Movie){
            filmDataSource.getMovieDetails(((Movie) media).getId(), user.getLanguage(), "credits,similar,videos", new FilmDataSource.MediaCallback() {
                @Override
                public void onSuccess(MediaDTO returnParam) {
                    callback.onSuccess(DataMediaMapper.toMedia(returnParam));
                }

                @Override
                public void onError(ErrorBundle errorBundle) {
                    callback.onError(errorBundle);
                }
            });
        }
        else if(media instanceof Serie){
            filmDataSource.getSerieDetails(((Serie) media).getId(), user.getLanguage(), "videos,credits,similar,external_ids", new FilmDataSource.MediaCallback() {
                @Override
                public void onSuccess(MediaDTO returnParam) {
                    callback.onSuccess(DataMediaMapper.toMedia(returnParam));
                }

                @Override
                public void onError(ErrorBundle errorBundle) {
                    callback.onError(errorBundle);
                }
            });
        }
    }

    @Override
    public void getMostPopular(int page, final MediaListCallback callback) {
        UserDTO user = internalDataStore.getUser();

        FilmDataSource.MediaListCallback listener = new FilmDataSource.MediaListCallback() {
            @Override
            public void onSuccess(MediaListDTO returnParam) {
                MediaList mediaList = DataMediaMapper.toMediaList(returnParam);
                callback.onSuccess(DataSorter.sortMediaList(mediaList, DataSorter.SortPolicy.MOST_POPULAR));
            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                callback.onError(errorBundle);
            }
        };

        filmDataSource.getMostPopular(Integer.toString(page), user.getLanguage(), user.getCountry(), listener);

    }

    @Override
    public void getUserDetails(final UserCallback callback) {
        UserDTO user = internalDataStore.getUser();
        callback.onSuccess(DataUserMapper.toUser(user));
    }

    @Override
    public void getTopRated(int page, final MediaListCallback callback) {
        UserDTO user = internalDataStore.getUser();

        FilmDataSource.MediaListCallback listener = new FilmDataSource.MediaListCallback() {
            @Override
            public void onSuccess(MediaListDTO returnParam) {
                MediaList mediaList = DataMediaMapper.toMediaList(returnParam);
                callback.onSuccess(DataSorter.sortMediaList(mediaList, DataSorter.SortPolicy.TOP_RATED));
            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                callback.onError(errorBundle);
            }
        };

        filmDataSource.getTopRated(Integer.toString(page), user.getLanguage(), user.getCountry(), listener);

    }

    @Override
    public void getFavourites(final MediaListCallback dataCallback) {
        DBDataSource.UserCallback userCallback = new DBDataSource.UserCallback() {
            @Override
            public void onSuccess(UserDTO returnParam) {
                internalDataStore.storeUser(returnParam);
                dataCallback.onSuccess(DataMediaMapper.toMediaList(returnParam.getFavourites()));
            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                dataCallback.onError(errorBundle);
            }
        };
        UserDTO user = internalDataStore.getUser();
        dbDataSource.getUserDetails(user, userCallback);
    }

    @Override
    public void addMediaToFavourites(Media media, VoidCallback dataCallback) {
        UserDTO user = dbDataSource.addMediaToFavourites(internalDataStore.getUser(), DataMediaMapper.toMediaDTO(media));
        internalDataStore.storeUser(user);
        dataCallback.onSuccess(null);
    }

    @Override
    public void deleteMediaFromFavourites(Media media, VoidCallback dataCallback) {
        UserDTO user = dbDataSource.removeMediaFromFavourites(internalDataStore.getUser(), DataMediaMapper.toMediaDTO(media));
        internalDataStore.storeUser(user);
        dataCallback.onSuccess(null);
    }

    @Override
    public void getMediaComments(Media media, final CommentsCallback dataCallback) {
        TvisoDataSource.MediaReviewsCallback callback = new TvisoDataSource.MediaReviewsCallback() {
            @Override
            public void onSuccess(MediaReviewsDTO returnParam) {
                dataCallback.onSuccess(DataCommentsMapper.toMediaComments(returnParam));
            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                dataCallback.onError(errorBundle);
            }
        };
        tvisoDataSource.getMediaComments(media.getImdbId(), callback);
    }

    @Override
    public void getActorDetails(Actor actor, final ActorCallback dataCallback) {
        UserDTO user = internalDataStore.getUser();
        FilmDataSource.ActorCallback callback = new FilmDataSource.ActorCallback() {
            @Override
            public void onSuccess(FullActorDetailsDTO returnParam) {
                dataCallback.onSuccess(DataCastMapper.toFullActorDetails(returnParam));
            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                dataCallback.onError(errorBundle);
            }
        };

        filmDataSource.getActorDetails(actor.getId(), user.getLanguage(), "combined_credits", callback);
    }

    @Override
    public void logoff() {
        dbDataSource.logoff();
        internalDataStore.removeUser();
    }
}
