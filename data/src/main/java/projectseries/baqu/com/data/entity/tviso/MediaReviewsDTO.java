package projectseries.baqu.com.data.entity.tviso;

import java.util.List;

/**
 * Created by mbaque on 07/02/2017.
 */

public class MediaReviewsDTO {

    List<ReviewDTO> reviews;

    public MediaReviewsDTO() {
    }

    public MediaReviewsDTO(List<ReviewDTO> reviews) {
        this.reviews = reviews;
    }

    public List<ReviewDTO> getReviews() {
        return reviews;
    }

    public void setReviews(List<ReviewDTO> reviews) {
        this.reviews = reviews;
    }
}
