package projectseries.baqu.com.data.entity.tmdb;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mbaque on 09/02/2017.
 */
public class SeasonDTO {

    @SerializedName("episode_count")
    String episodes;

    @SerializedName("season_number")
    String seasonNumber;

    @SerializedName("poster_path")
    String posterPath;

    public SeasonDTO() {
    }

    public SeasonDTO(String episodes, String seasonNumber, String posterPath) {
        this.episodes = episodes;
        this.seasonNumber = seasonNumber;
        this.posterPath = posterPath;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public String getEpisodes() {
        return episodes;
    }

    public void setEpisodes(String episodes) {
        this.episodes = episodes;
    }

    public String getSeasonNumber() {
        return seasonNumber;
    }

    public void setSeasonNumber(String seasonNumber) {
        this.seasonNumber = seasonNumber;
    }
}
