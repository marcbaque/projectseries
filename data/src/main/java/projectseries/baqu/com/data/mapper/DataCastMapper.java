package projectseries.baqu.com.data.mapper;

import com.baqu.entity.Actor;
import com.baqu.entity.ActorCredits;
import com.baqu.entity.ActorFilm;
import com.baqu.entity.Cast;

import java.util.ArrayList;
import java.util.List;

import projectseries.baqu.com.data.entity.tmdb.ActorDTO;
import projectseries.baqu.com.data.entity.tmdb.ActorFilmDTO;
import projectseries.baqu.com.data.entity.tmdb.CastDTO;
import com.baqu.entity.FullActorDetails;
import projectseries.baqu.com.data.entity.tmdb.FullActorDetailsDTO;

/**
 * Created by mbaque on 03/02/2017.
 */

public class DataCastMapper {

    public static Cast toCast(CastDTO castDTO){
        List<Actor> actors = new ArrayList<>();
        if(castDTO!=null) {
            for (ActorDTO actorDTO : castDTO.getCast()) {
                actors.add(toActor(actorDTO));
            }
        }
        return new Cast(actors);
    }

    public static CastDTO toCastDTO(Cast cast){
        List<ActorDTO> actors = new ArrayList<>();
        for(Actor actor : cast.getCast()){
            actors.add(toActorDTO(actor));
        }
        return new CastDTO(actors);
    }

    public static Actor toActor(ActorDTO actorDTO){
        Actor actor = new Actor(actorDTO.getId(), actorDTO.getName(), actorDTO.getProfilePath(), actorDTO.getCharacter());
        return actor;
    }

    public static ActorDTO toActorDTO(Actor actor){
        ActorDTO actorDTO = new ActorDTO(actor.getId(), actor.getName(), actor.getProfilePath(), actor.getCharacter());
        return actorDTO;
    }

    public static FullActorDetails toFullActorDetails(FullActorDetailsDTO actorDetailsDTO){
        List<ActorFilm> actorFilms = new ArrayList<>();

        for(ActorFilmDTO actorFilmDTO : actorDetailsDTO.getCombinedCredits().getCast()){
            if(actorFilmDTO.getName()==null){
                //Es una pelicula
                ActorFilm actorFilm = new ActorFilm(actorFilmDTO.getId(), actorFilmDTO.getTitle(), actorFilmDTO.getCharacter(), actorFilmDTO.getReleaseDate(), actorFilmDTO.getPosterPath(), "Movie");
                actorFilms.add(actorFilm);
            }
            else{
                ActorFilm actorFilm = new ActorFilm(actorFilmDTO.getId(), actorFilmDTO.getName(), actorFilmDTO.getCharacter(), actorFilmDTO.getFirstAirDate(), actorFilmDTO.getPosterPath(), "Tv");
                actorFilms.add(actorFilm);
            }

        }

        FullActorDetails fullActorDetails = new FullActorDetails(actorDetailsDTO.getId(), actorDetailsDTO.getName(), actorDetailsDTO.getPlaceOfBirth(), actorDetailsDTO.getProfilePath(), actorDetailsDTO.getBiography(), new ActorCredits(actorFilms));

        return fullActorDetails;
    }

    public static FullActorDetailsDTO toFullActorDetailsDTO(FullActorDetails actorDetails){
        return null;
    }

}
