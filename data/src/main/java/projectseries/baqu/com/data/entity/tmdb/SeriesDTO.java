package projectseries.baqu.com.data.entity.tmdb;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mbaque on 28/01/2017.
 */

public class SeriesDTO {

    @SerializedName("results")
    private List<SerieDTO> series;

    public SeriesDTO() {
    }

    public SeriesDTO(List<SerieDTO> series) {
        this.series = series;
    }

    public List<SerieDTO> getSeries() {
        return series;
    }

    public void setSeries(List<SerieDTO> series) {
        this.series = series;
    }

    public SerieDTO getSerie(int position){
        return series.get(position);
    }

    public int size(){
        return series.size();
    }

    public void addAll(SeriesDTO series) {
        this.series.addAll(series.getSeries());
    }
}
