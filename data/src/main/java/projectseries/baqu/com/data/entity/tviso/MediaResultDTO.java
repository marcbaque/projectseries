package projectseries.baqu.com.data.entity.tviso;

import java.util.List;

/**
 * Created by mbaque on 07/02/2017.
 */

public class MediaResultDTO{

    List<MediaInfoDTO> results;

    public MediaResultDTO() {
    }

    public MediaResultDTO(List<MediaInfoDTO> results) {
        this.results = results;
    }

    public List<MediaInfoDTO> getResults() {
        return results;
    }

    public void setResults(List<MediaInfoDTO> results) {
        this.results = results;
    }
}
