package projectseries.baqu.com.data.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import projectseries.baqu.com.data.entity.tmdb.MediaDTO;

/**
 * Created by mbaque on 05/02/2017.
 */

public class RecommendationData {

    Map<String, Double> weightedList;

    List<List<Integer>> lruList;

    List<Integer> lruYears;

    Map<String, Long> yearsList;

    public RecommendationData() {

    }

    public RecommendationData(List<Genre> genreList) {
        this.weightedList = new HashMap<>();
        for(Genre g : genreList){
            weightedList.put(g.getId().toString(), 0.0);
        }

        this.lruList = new ArrayList<>(2);
        this.yearsList = new HashMap<>();
        this.lruYears = new ArrayList<>(2);
    }

    public Map<String, Double> getWeightedList() {
        return weightedList;
    }

    public void setWeightedList(Map<String, Double> weightedList) {
        this.weightedList = weightedList;
    }

    public List<Integer> getLruYears() {
        return lruYears;
    }

    public void setLruYears(List<Integer> lruYears) {
        this.lruYears = lruYears;
    }

    public Map<String, Long> getYearsList() {
        return yearsList;
    }

    public void setYearsList(Map<String, Long> yearsList) {
        this.yearsList = yearsList;
    }

    public List<List<Integer>> getLruList() {
        return lruList;
    }

    public void setLruList(List<List<Integer>> lruList) {
        this.lruList = lruList;
    }

    public void newMediaViewed(MediaDTO media){
        List<Integer> genres = new ArrayList<>();
        if(media.getGenres()!=null) {
            for (Integer genreId : media.getGenres()) {
                genres.add(genreId);
            }
        }

        for(String genre : weightedList.keySet()){
            if(genres.contains(genre)){
                Double newWeight = weightedList.get(genre) + ((double) 1/weightedList.size());
                weightedList.put(genre, newWeight);
            }
            else{
                Double newWeight = weightedList.get(genre) - ((double) 1/(weightedList.size()*weightedList.size()));
                weightedList.put(genre, newWeight);
            }
        }

        if(lruList != null && !lruList.contains(genres)) {
            if (lruList.size() == 0) {
                lruList.add(0, genres);
            } else if (lruList.size() == 1) {
                lruList.add(0, genres);
            } else {
                lruList.set(1, lruList.get(0));
                lruList.set(0, genres);
            }
        }
        else if(lruList == null){
            lruList = new ArrayList<>();
            lruList.add(0, genres);
        }

        boolean found = false;
        if(media.getYear()!=null) {
            Integer mediaYear = Integer.parseInt(media.getYear().substring(0, 4));
            if(yearsList != null) {
                for (String year : yearsList.keySet()) {
                    if (Integer.parseInt(year) == mediaYear / 10) {
                        Long updatedCounterValue = yearsList.get(year) + 1;
                        yearsList.put(year, updatedCounterValue);
                        found = true;
                        break;
                    }
                }
            }
            else{
                yearsList = new HashMap<>();
            }

            if(!found){
                yearsList.put(String.valueOf(mediaYear/10), 1L);
            }
        }
    }

    public List<Integer> recommendedGenres(){
        List<Integer> recommended = new ArrayList<>();

        List<Double> weights = new ArrayList<>(weightedList.values());
        Collections.sort(weights);

        int num = 0;
        for(String genre : weightedList.keySet()){
            if(weightedList.get(genre).equals(weights.get(0)) || weightedList.get(genre).equals(weights.get(0))){
                ++num;
                recommended.add(Integer.parseInt(genre));
                if(num == 2){
                    break;
                }
            }
        }

        if(lruList.size()>0) {
            recommended.addAll(lruList.get(0));
        }
        if(lruList.size()>1) {
            recommended.addAll(lruList.get(1));
        }

        return recommended;
    }

    public List<Integer> recommendedYears(){
        List<Integer> recommended = new ArrayList<>();

        List<Long> years = new ArrayList<>(yearsList.values());
        Collections.sort(years);

        int num = 0;
        for(String year : yearsList.keySet()){

            if(yearsList.get(year).equals(years.get(0)) || yearsList.get(year).equals(years.get(1))){
                ++num;
                recommended.add(Integer.parseInt(year) * 10);
                if(num == 2){
                    break;
                }
            }
        }
        return recommended;
    }
}
