package projectseries.baqu.com.data.datasource;

import android.support.annotation.NonNull;

import com.baqu.callback.DefaultCallback;
import com.baqu.exception.ErrorBundle;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import javax.inject.Inject;

import projectseries.baqu.com.data.entity.UserDTO;
import projectseries.baqu.com.data.entity.tmdb.MediaDTO;
import projectseries.baqu.com.data.entity.tmdb.MediaListDTO;
import projectseries.baqu.com.data.entity.tmdb.MovieDTO;
import projectseries.baqu.com.data.entity.tmdb.MoviesDTO;
import projectseries.baqu.com.data.entity.tmdb.SerieDTO;
import projectseries.baqu.com.data.entity.tmdb.SeriesDTO;

/**
 * Created by mbaque on 28/01/2017.
 */

public class FirebaseDataSource implements DBDataSource {

    private DatabaseReference database;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseUserCallback userListener;

    @Inject
    public FirebaseDataSource() {
        database = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    if(userListener!=null){
                        userListener.onSuccess(user);
                    }
                } else {
                    if(userListener!=null){
                        userListener.onError(null);
                    }
                }
            }
        };
    }


    @Override
    public void login(String user, String password, final UserCallback callback) {
        mAuth.signInWithEmailAndPassword(user, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull final Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    getUserDetails(task.getResult().getUser(), callback);
                } else {
                    handleAuthError(task.getException(), callback);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                handleAuthError(e, callback);
            }
        });

    }



    @Override
    public FirebaseUser getAppUser() {
        return mAuth.getCurrentUser();
    }

    @Override
    public void register(String user, String password, final FirebaseUserCallback callback) {
        mAuth.createUserWithEmailAndPassword(user, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    callback.onSuccess(task.getResult().getUser());
                }
                else{
                    handleAuthError((FirebaseAuthException) task.getException(), callback);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                handleAuthError(e, callback);
            }
        });

    }

    @Override
    public void onActivityStart(FirebaseUserCallback listener) {
        this.userListener = listener;
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onActivityStop() {
        this.userListener = null;
        mAuth.removeAuthStateListener(mAuthListener);
    }

    @Override
    public UserDTO addMediaToFavourites(UserDTO user, MediaDTO mediaDTO) {
        if(user.getFavourites()==null){
            user.setFavourites(new MediaListDTO(new MoviesDTO(new ArrayList<MovieDTO>()), new SeriesDTO(new ArrayList<SerieDTO>())));
        }
        if(user.getFavourites().getMovies()==null){
            user.getFavourites().setMovies(new MoviesDTO(new ArrayList<MovieDTO>()));
        }
        if(user.getFavourites().getMovies()==null){
            user.getFavourites().setSeries(new SeriesDTO(new ArrayList<SerieDTO>()));
        }
        if(mediaDTO instanceof MovieDTO){
            user.getFavourites().getMovies().addMovie((MovieDTO) mediaDTO);
        }
        else{
            user.getFavourites().getSeries().getSeries().add((SerieDTO) mediaDTO);
        }
        database.child("users").child(user.parsedEmail()).child("favourites").setValue(user.getFavourites());
        return user;
    }

    @Override
    public UserDTO removeMediaFromFavourites(UserDTO user, MediaDTO mediaDTO) {
        if(mediaDTO instanceof MovieDTO){
            for(int i = 0; i < user.getFavourites().getMovies().getMovies().size(); ++i){
                MovieDTO movie = user.getFavourites().getMovies().getMovie(i);
                if(movie.getId() == ((MovieDTO) mediaDTO).getId()){
                    user.getFavourites().getMovies().getMovies().remove(i);
                }
            }
        }
        else{
            for(int i = 0; i < user.getFavourites().getSeries().getSeries().size(); ++i){
                SerieDTO serie = user.getFavourites().getSeries().getSerie(i);
                if(serie.getId() == ((SerieDTO) mediaDTO).getId()){
                    user.getFavourites().getSeries().getSeries().remove(i);
                }
            }
        }
        database.child("users").child(user.parsedEmail()).child("favourites").setValue(user.getFavourites());
        return user;
    }

    @Override
    public void getUserDetails(final FirebaseUser user, final UserCallback callback) {
        database.child("users").child(user.getEmail().replace(".", " ")).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                callback.onSuccess(dataSnapshot.getValue(UserDTO.class));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                handleError(callback);
            }
        });
    }

    @Override
    public void getUserDetails(final UserDTO user, final UserCallback callback) {
        database.child("users").child(user.parsedEmail()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                callback.onSuccess(dataSnapshot.getValue(UserDTO.class));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                handleError(callback);
            }
        });
    }

    @Override
    public void initializeUser(UserDTO user) {
        database.child("users").child(user.parsedEmail()).setValue(user);
    }

    @Override
    public void updateUser(UserDTO user) {
        database.child("users").child(user.parsedEmail()).setValue(user);
    }

    @Override
    public void logoff() {
        mAuth.signOut();
    }

    private void handleError(DefaultCallback callback){
        callback.onError(new ErrorBundle() {
            @Override
            public Exception getException() {
                return new Exception();
            }

            @Override
            public String getErrorMessage() {
                return "Se ha producido un error con el servidor";
            }
        });
    }

    private void handleAuthError(final Exception exception, DefaultCallback callback){
        String message;
        if(exception instanceof FirebaseAuthException) {
            String error = ((FirebaseAuthException) exception).getErrorCode();
            switch (error) {
                case "ERROR_INVALID_EMAIL":
                    message = "Este email no es válido";
                    break;

                case "ERROR_USER_NOT_FOUND":
                    message = "Este usuario no existe";
                    break;

                case "ERROR_WRONG_PASSWORD":
                    message = "Contraseña incorrecta";
                    break;

                case "ERROR_INVALID_CREDENTIAL":
                    message = "Estas credenciales no son válidas";
                    break;

                case "ERROR_EMAIL_ALREADY_IN_USE":
                    message = "Este email ya está en uso";
                    break;

                case "ERROR_CREDENTIAL_ALREADY_IN_USE":
                    message = "Este email ya está en uso";
                    break;

                case "ERROR_WEAK_PASSWORD":
                    message = "La contraseña debe tener más de 6 caracteres";
                    break;

                default:
                    message = "Se ha producido un error desconocido";
            }
        }
        else if(exception instanceof FirebaseNetworkException){
            message = "No ha conexión a internet";
        }
        else{
            message = "Se ha producido un error desconocido";
        }

        final String finalMessage = message;
        callback.onError(new ErrorBundle() {
            @Override
            public Exception getException() {
                return exception;
            }

            @Override
            public String getErrorMessage() {
                return finalMessage;
            }
        });

    }
}
