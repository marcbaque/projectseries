package projectseries.baqu.com.data.entity.tmdb;

import java.util.List;

/**
 * Created by mbaque on 08/02/2017.
 */

public class VideosDTO {

    List<VideoDTO> results;

    public VideosDTO() {
    }

    public VideosDTO(List<VideoDTO> results) {
        this.results = results;
    }

    public List<VideoDTO> getResults() {
        return results;
    }

    public void setResults(List<VideoDTO> results) {
        this.results = results;
    }
}
