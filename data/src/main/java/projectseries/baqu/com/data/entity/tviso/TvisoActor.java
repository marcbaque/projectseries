package projectseries.baqu.com.data.entity.tviso;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mbaque on 07/02/2017.
 */

public class TvisoActor {

    String uid;

    String fullName;

    @SerializedName("image")
    String imagePath;

    public TvisoActor() {
    }

    public TvisoActor(String uid, String fullName, String imagePath) {
        this.uid = uid;
        this.fullName = fullName;
        this.imagePath = imagePath;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
}
