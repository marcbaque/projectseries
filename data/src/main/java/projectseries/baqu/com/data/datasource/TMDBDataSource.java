package projectseries.baqu.com.data.datasource;

import com.baqu.entity.MediaList;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import projectseries.baqu.com.data.entity.UserDTO;
import projectseries.baqu.com.data.entity.tmdb.ActorDTO;
import projectseries.baqu.com.data.entity.tmdb.FullActorDetailsDTO;
import projectseries.baqu.com.data.entity.tmdb.MediaDTO;
import projectseries.baqu.com.data.entity.tmdb.MediaListDTO;
import projectseries.baqu.com.data.entity.tmdb.MovieDTO;
import projectseries.baqu.com.data.entity.tmdb.MoviesDTO;
import projectseries.baqu.com.data.entity.tmdb.SerieDTO;
import projectseries.baqu.com.data.entity.tmdb.SeriesDTO;
import projectseries.baqu.com.data.service.TMDBService;
import projectseries.baqu.com.data.utils.DataLoaderHelper;
import projectseries.baqu.com.data.utils.RetrofitErrorHandler;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by mbaque on 27/01/2017.
 */

public class TMDBDataSource implements FilmDataSource{

    private static final String API_KEY = "f8ad200e565c8443bc8558f7f36515d2";
    private TMDBService retrofitService;

    @Inject
    public TMDBDataSource() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.addInterceptor(interceptor)
                .connectTimeout(5, TimeUnit.MINUTES)
                .writeTimeout(5, TimeUnit.MINUTES)
                .readTimeout(5, TimeUnit.MINUTES);
        OkHttpClient client = builder.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.themoviedb.org/3/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        retrofitService = retrofit.create(TMDBService.class);
    }


    @Override
    public void searchMedia(String page, String query, String language, final MediaListCallback listener) {
        final DataLoaderHelper<MoviesDTO, SeriesDTO> dataLoaderHelper = new DataLoaderHelper<>(1);

        retrofitService.searchMovie(API_KEY, page, "es", query).enqueue(getMoviesCallback(0, dataLoaderHelper, listener));

        retrofitService.searchSerie(API_KEY, page, "es", query).enqueue(getSeriesCallback(0, dataLoaderHelper, listener));
    }

    @Override
    public void getMovieDetails(String movieId, String language, String attach, final MediaCallback listener) {
        retrofitService.getMovieDetails(movieId, API_KEY, language, attach).enqueue(new RetrofitErrorHandler<MediaDTO, MovieDTO>(listener) {
            @Override
            public void onResponse(Call<MovieDTO> call, Response<MovieDTO> response) {
                if(response.isSuccessful()) {
                    listener.onSuccess(response.body());
                }
                else{
                    super.onResponse(call, response);
                }
            }
        });
    }

    @Override
    public void getSerieDetails(String serieId, String language, String attach, final MediaCallback listener) {
        retrofitService.getSerieDetails(serieId, API_KEY, "es", attach).enqueue(new RetrofitErrorHandler<MediaDTO, SerieDTO>(listener) {
            @Override
            public void onResponse(Call<SerieDTO> call, Response<SerieDTO> response) {
                if(response.isSuccessful()) {
                    listener.onSuccess(response.body());
                }
                else{
                    super.onResponse(call, response);
                }
            }
        });
    }

    @Override
    public void getDiscover(String page, String language, final MediaListCallback listener){
        final DataLoaderHelper<MoviesDTO, SeriesDTO> dataLoaderHelper = new DataLoaderHelper<>(1);

        retrofitService.getDiscoverMovies(API_KEY, page, "es").enqueue(getMoviesCallback(0, dataLoaderHelper, listener));

        retrofitService.getDiscoverSeries(API_KEY, page, "es").enqueue(getSeriesCallback(0, dataLoaderHelper, listener));

    }

    @Override
    public void getMostPopular(String page, String language, String country, final MediaListCallback listener) {
        final DataLoaderHelper<MoviesDTO, SeriesDTO> dataLoaderHelper = new DataLoaderHelper<>(1);
        retrofitService.getMostPopularMovies(API_KEY, page, "es", country).enqueue(getMoviesCallback(0, dataLoaderHelper, listener));

        retrofitService.getMostPopularSeries(API_KEY, page, "es", country).enqueue(getSeriesCallback(0, dataLoaderHelper, listener));
    }

    @Override
    public void getTopRated(String page, String language, String country, final MediaListCallback listener) {
        final DataLoaderHelper<MoviesDTO, SeriesDTO> dataLoaderHelper = new DataLoaderHelper<>(1);

        retrofitService.getTopRatedMovies(API_KEY, page, "es", country).enqueue(getMoviesCallback(0, dataLoaderHelper, listener));
        retrofitService.getTopRatedSeries(API_KEY, page, "es", country).enqueue(getSeriesCallback(0, dataLoaderHelper, listener));
    }

    @Override
    public void getRecommended(List<Integer> genres, String minDate, String maxDate, UserDTO user, final MediaListCallback listener) {

        final DataLoaderHelper<MoviesDTO, SeriesDTO> dataLoaderHelper = new DataLoaderHelper<>(genres.size());

        for(int i = 0; i < genres.size(); ++i){

            retrofitService.getRecommendedMovies(API_KEY, "es", minDate, maxDate, Integer.toString(genres.get(i))).enqueue(getMoviesCallback(i, dataLoaderHelper, listener));

            retrofitService.getRecommendedSeries(API_KEY, "es", minDate, maxDate, Integer.toString(genres.get(i))).enqueue(getSeriesCallback(i, dataLoaderHelper, listener));

        }

    }

    @Override
    public void getActorDetails(String actorId, String language, String attach, final ActorCallback callback) {
        retrofitService.getActorDetails(actorId, API_KEY, "es", attach).enqueue(new RetrofitErrorHandler<FullActorDetailsDTO, FullActorDetailsDTO>(callback) {
            @Override
            public void onResponse(Call<FullActorDetailsDTO> call, Response<FullActorDetailsDTO> response) {
                if(response.isSuccessful()){
                    callback.onSuccess(response.body());
                }
                else{
                    super.onResponse(call, response);
                }
            }
        });
    }

    private Callback<MoviesDTO> getMoviesCallback(final int callNumber, final DataLoaderHelper<MoviesDTO, SeriesDTO> dataLoaderHelper, final MediaListCallback listener){
        return new RetrofitErrorHandler<MediaListDTO, MoviesDTO>(listener) {

            @Override
            public void onResponse(Call<MoviesDTO> call, Response<MoviesDTO> response) {
                if(dataLoaderHelper.errorHappened()){
                    listener.onError(null);
                    return;
                }

                if(response.isSuccessful()) {
                    dataLoaderHelper.onFirstTypeDataLoaded(response.body());
                    if(dataLoaderHelper.isDataLoaded()){
                        onMediaListLoded(dataLoaderHelper, listener);
                    }
                }
                else{
                    dataLoaderHelper.onError();
                    super.onResponse(call, response);

                }
            }

            @Override
            public void onFailure(Call<MoviesDTO> call, Throwable t) {
                super.onFailure(call, t);
                dataLoaderHelper.onError();
            }
        };
    }

    private Callback<SeriesDTO> getSeriesCallback(final int callNumber, final DataLoaderHelper<MoviesDTO, SeriesDTO> dataLoaderHelper, final MediaListCallback listener){

        return new RetrofitErrorHandler<MediaListDTO, SeriesDTO>(listener) {
            @Override
            public void onResponse(Call<SeriesDTO> call, Response<SeriesDTO> response) {
                if(dataLoaderHelper.errorHappened()){
                    listener.onError(null);
                    return;
                }

                if(response.isSuccessful()) {
                    dataLoaderHelper.onSecondTypeDataLoaded(response.body());
                    if(dataLoaderHelper.isDataLoaded()){
                        onMediaListLoded(dataLoaderHelper, listener);
                    }
                }
                else{
                    dataLoaderHelper.onError();
                    super.onResponse(call, response);

                }
            }

            @Override
            public void onFailure(Call<SeriesDTO> call, Throwable t) {
                super.onFailure(call, t);
                dataLoaderHelper.onError();
            }
        };
    }

    private void onMediaListLoded(DataLoaderHelper<MoviesDTO, SeriesDTO> dataLoaderHelper, MediaListCallback listener){
        MoviesDTO movies = new MoviesDTO(new ArrayList<MovieDTO>());
        SeriesDTO series = new SeriesDTO(new ArrayList<SerieDTO>());
        for(int i = 0; i < dataLoaderHelper.getCallNumber(); ++i){
            movies.getMovies().addAll(dataLoaderHelper.getFirstTypeData(i).getMovies());
            series.getSeries().addAll(dataLoaderHelper.getSecondTypeData(i).getSeries());
        }

        listener.onSuccess(new MediaListDTO(movies, series));
    }
}
