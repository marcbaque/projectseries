package projectseries.baqu.com.data.mapper;

import com.baqu.entity.User;

import projectseries.baqu.com.data.entity.UserDTO;

/**
 * Created by mbaque on 05/02/2017.
 */

public class DataUserMapper {

    public static User toUser(UserDTO userDTO){
        User user = new User(userDTO.getId(), userDTO.getLanguage(), userDTO.getCountry(), userDTO.getEmail());
        return user;
    }

}
