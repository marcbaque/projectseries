package projectseries.baqu.com.data.entity;

import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import projectseries.baqu.com.data.entity.tmdb.MediaListDTO;
import projectseries.baqu.com.data.entity.tmdb.MovieDTO;
import projectseries.baqu.com.data.entity.tmdb.MoviesDTO;
import projectseries.baqu.com.data.entity.tmdb.SerieDTO;
import projectseries.baqu.com.data.entity.tmdb.SeriesDTO;

/**
 * Created by mbaque on 02/02/2017.
 */

public class UserDTO {

    String id;
    String language;
    String country;
    String email;

    MediaListDTO favourites;

    RecommendationData recommendationData;

    public UserDTO() {
    }

    public UserDTO(String id, String language, String country, String email, List<Genre> genreList) {
        this.id = id;
        this.language = language;
        this.country = country;
        this.email = email;
        this.favourites = new MediaListDTO(new MoviesDTO(new ArrayList<MovieDTO>()), new SeriesDTO(new ArrayList<SerieDTO>()));
        this.recommendationData = new RecommendationData(genreList);
    }

    public UserDTO(FirebaseUser user, List<Genre> genreList){
        this.id = user.getUid();
        this.language = Locale.getDefault().getLanguage();
        this.country = Locale.getDefault().getCountry();
        this.email = user.getEmail();
        this.favourites = new MediaListDTO(new MoviesDTO(new ArrayList<MovieDTO>()), new SeriesDTO(new ArrayList<SerieDTO>()));
        this.recommendationData = new RecommendationData(genreList);
    }

    public MediaListDTO getFavourites() {
        return favourites;
    }

    public void setFavourites(MediaListDTO favourites) {
        this.favourites = favourites;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String parsedEmail(){
        return email.replace(".", " ");
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public RecommendationData getRecommendationData() {
        return recommendationData;
    }

    public void setRecommendationData(RecommendationData recommendationData) {
        this.recommendationData = recommendationData;
    }
}
