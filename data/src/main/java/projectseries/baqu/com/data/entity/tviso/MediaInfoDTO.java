package projectseries.baqu.com.data.entity.tviso;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mbaque on 07/02/2017.
 */

public class MediaInfoDTO {

    @SerializedName("imdbid")
    String imdbId;

    MediaFullDTO mediaInfo;

    public MediaInfoDTO() {
    }

    public MediaInfoDTO(String imdbId, MediaFullDTO mediaInfo) {
        this.imdbId = imdbId;
        this.mediaInfo = mediaInfo;
    }

    public String getImdbId() {
        return imdbId;
    }

    public void setImdbId(String imdbId) {
        this.imdbId = imdbId;
    }

    public MediaFullDTO getMediaInfo() {
        return mediaInfo;
    }

    public void setMediaInfo(MediaFullDTO mediaInfo) {
        this.mediaInfo = mediaInfo;
    }
}
