package projectseries.baqu.com.data.service;


import projectseries.baqu.com.data.entity.tmdb.ActorDTO;
import projectseries.baqu.com.data.entity.tmdb.FullActorDetailsDTO;
import projectseries.baqu.com.data.entity.tmdb.MovieDTO;
import projectseries.baqu.com.data.entity.tmdb.MoviesDTO;
import projectseries.baqu.com.data.entity.tmdb.SerieDTO;
import projectseries.baqu.com.data.entity.tmdb.SeriesDTO;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by mbaque on 27/01/2017.
 */

public interface TMDBService {

    @GET("discover/movie")
    Call<MoviesDTO> getDiscoverMovies(@Query("api_key") String apiKey, @Query("page") String page, @Query("language") String language);

    @GET("discover/movie")
    Call<MoviesDTO> getRecommendedMovies(@Query("api_key") String apiKey, @Query("language") String language, @Query("primary_release_date.gte") String minDate, @Query("primary_release_date.lte") String maxDate, @Query("with_genres") String genre);

    @GET("discover/tv")
    Call<SeriesDTO> getDiscoverSeries(@Query("api_key") String apiKey, @Query("page") String page, @Query("language") String language);

    @GET("search/movie")
    Call<MoviesDTO> searchMovie(@Query("api_key") String apiKey, @Query("page") String page, @Query("language") String language, @Query("query") String query);

    @GET("search/tv")
    Call<SeriesDTO> searchSerie(@Query("api_key") String apiKey, @Query("page") String page, @Query("language") String language, @Query("query") String query);

    @GET("discover/tv")
    Call<SeriesDTO> getRecommendedSeries(@Query("api_key") String apiKey, @Query("language") String language, @Query("air_date.gte") String minDate, @Query("air_date.lte") String maxDate, @Query("with_genres") String genres);

    @GET("movie/popular")
    Call<MoviesDTO> getMostPopularMovies(@Query("api_key") String apiKey, @Query("page") String page, @Query("language") String language, @Query("region") String country);

    @GET("tv/popular")
    Call<SeriesDTO> getMostPopularSeries(@Query("api_key") String apiKey, @Query("page") String page, @Query("language") String language, @Query("region") String country);

    @GET("movie/top_rated")
    Call<MoviesDTO> getTopRatedMovies(@Query("api_key") String apiKey, @Query("page") String page, @Query("language") String language, @Query("region") String country);

    @GET("tv/top_rated")
    Call<SeriesDTO> getTopRatedSeries(@Query("api_key") String apiKey, @Query("page") String page, @Query("language") String language, @Query("region") String country);

    @GET("movie/{movie_id}")
    Call<MovieDTO> getMovieDetails(@Path("movie_id") String movieId, @Query("api_key") String apiKey, @Query("language") String language, @Query("append_to_response") String attach);

    @GET("tv/{tv_id}")
    Call<SerieDTO> getSerieDetails(@Path("tv_id") String movieId, @Query("api_key") String apiKey, @Query("language") String language, @Query("append_to_response") String attach);

    @GET("person/{person_id}")
    Call<FullActorDetailsDTO> getActorDetails(@Path("person_id") String actorId, @Query("api_key") String apiKey, @Query("language") String language, @Query("append_to_response") String attach);


}
