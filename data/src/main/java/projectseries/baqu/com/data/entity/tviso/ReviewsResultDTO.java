package projectseries.baqu.com.data.entity.tviso;

/**
 * Created by mbaque on 07/02/2017.
 */

public class ReviewsResultDTO {

    MediaReviewsDTO results;

    public ReviewsResultDTO() {
    }

    public ReviewsResultDTO(MediaReviewsDTO results) {
        this.results = results;
    }

    public MediaReviewsDTO getResults() {
        return results;
    }

    public void setResults(MediaReviewsDTO results) {
        this.results = results;
    }
}
