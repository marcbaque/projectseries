package projectseries.baqu.com.data.entity.tmdb;

import java.util.List;

/**
 * Created by mbaque on 31/01/2017.
 */

public class CastDTO {

    List<ActorDTO> cast;

    public CastDTO(List<ActorDTO> cast) {
        this.cast = cast;
    }

    public List<ActorDTO> getCast() {
        return cast;
    }

    public void setCast(List<ActorDTO> cast) {
        this.cast = cast;
    }
}
