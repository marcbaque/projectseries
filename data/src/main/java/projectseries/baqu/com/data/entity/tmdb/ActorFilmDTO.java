package projectseries.baqu.com.data.entity.tmdb;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mbaque on 09/02/2017.
 */

public class ActorFilmDTO {

    String id;

    String title;

    String name;

    String character;

    @SerializedName("release_date")
    String releaseDate;

    @SerializedName("poster_path")
    String posterPath;

    @SerializedName("first_air_date")
    String firstAirDate;

    public ActorFilmDTO(String id, String title, String character, String releaseDate, String posterPath) {
        this.id = id;
        this.title = title;
        this.character = character;
        this.releaseDate = releaseDate;
        this.posterPath = posterPath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstAirDate() {
        return firstAirDate;
    }

    public void setFirstAirDate(String firstAirDate) {
        this.firstAirDate = firstAirDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }
}
