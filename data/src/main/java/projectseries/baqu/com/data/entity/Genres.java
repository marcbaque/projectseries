package projectseries.baqu.com.data.entity;

import java.util.List;

/**
 * Created by mbaque on 05/02/2017.
 */

public class Genres {

    List<Genre> genres;

    public Genres() {
    }

    public Genres(List<Genre> genres) {
        this.genres = genres;
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }
}
