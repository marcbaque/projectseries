package projectseries.baqu.com.data.datasource;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.List;

import javax.inject.Inject;

import projectseries.baqu.com.data.R;
import projectseries.baqu.com.data.dependencyinjection.qualifier.ForApp;
import projectseries.baqu.com.data.entity.Genre;
import projectseries.baqu.com.data.entity.Genres;
import projectseries.baqu.com.data.entity.UserDTO;

/**
 * Created by mbaque on 04/02/2017.
 */

public class SharedPreferencesDataSource implements InternalStorageDataSource {

    private static final Type REVIEW_TYPE = new TypeToken<List<Genre>>(){}.getType();
    SharedPreferences sharedPreferences;
    Context context;


    @Inject
    public SharedPreferencesDataSource(@ForApp Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences("project_series", Context.MODE_PRIVATE);

    }

    @Override
    public void storeUser(UserDTO user) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(user);
        editor.putString("user", json);
        editor.apply();
    }

    @Override
    public UserDTO getUser() {
        String json = sharedPreferences.getString("user", "");
        Gson gson = new Gson();
        return gson.fromJson(json, UserDTO.class);
    }

    @Override
    public void removeUser() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove("user");
        editor.apply();
    }

    public List<Genre> getGenres(){
        Gson gson = new Gson();
        Genres genres = gson.fromJson(readFile(), Genres.class);
        return genres.getGenres();
    }

    private String readFile(){
        InputStream raw = context.getResources().openRawResource(R.raw.genres);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        int i;
        try
        {
            i = raw.read();
            while (i != -1)
            {
                byteArrayOutputStream.write(i);
                i = raw.read();
            }
            raw.close();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block

            e.printStackTrace();
        }

        return byteArrayOutputStream.toString();
    }
}
