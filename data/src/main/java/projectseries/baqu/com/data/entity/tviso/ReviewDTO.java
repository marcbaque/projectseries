package projectseries.baqu.com.data.entity.tviso;


import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mbaque on 07/02/2017.
 */

public class ReviewDTO {

    String id;

    TvisoActor actor;

    String rating;

    String content;

    boolean isSpoiler;

    List<ReplyDTO> replies;

    @SerializedName("published")
    String datePublished;

    public ReviewDTO() {
    }

    public ReviewDTO(String id, TvisoActor actor, String rating, String content, boolean isSpoiler, List<ReplyDTO> replies, String datePublished) {
        this.id = id;
        this.actor = actor;
        this.rating = rating;
        this.content = content;
        this.isSpoiler = isSpoiler;
        this.replies = replies;
        this.datePublished = datePublished;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public TvisoActor getActor() {
        return actor;
    }

    public void setActor(TvisoActor actor) {
        this.actor = actor;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isSpoiler() {
        return isSpoiler;
    }

    public void setSpoiler(boolean spoiler) {
        isSpoiler = spoiler;
    }

    public List<ReplyDTO> getReplies() {
        return replies;
    }

    public void setReplies(List<ReplyDTO> replies) {
        this.replies = replies;
    }

    public String getDatePublished() {
        return datePublished;
    }

    public void setDatePublished(String datePublished) {
        this.datePublished = datePublished;
    }
}
