package projectseries.baqu.com.data.datasource;

import com.baqu.callback.DefaultCallback;
import com.baqu.exception.ErrorBundle;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import projectseries.baqu.com.data.entity.tviso.AuthTokenDTO;
import projectseries.baqu.com.data.entity.tviso.MediaInfoDTO;
import projectseries.baqu.com.data.entity.tviso.MediaResultDTO;
import projectseries.baqu.com.data.entity.tviso.MediaReviewsDTO;
import projectseries.baqu.com.data.entity.tviso.ReviewsResultDTO;
import projectseries.baqu.com.data.service.TvisoService;
import projectseries.baqu.com.data.utils.RetrofitErrorHandler;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by mbaque on 07/02/2017.
 */

public class TvisoApi implements TvisoDataSource {

    private TvisoService retrofitService;
    private AuthTokenDTO apiToken;
    private static final String APP_SECRET = "5bFXXfD3y5eXNNCwn3ZK";
    private static final String API_ID = "3530";

    @Inject
    public TvisoApi() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.addInterceptor(interceptor)
                .connectTimeout(5, TimeUnit.MINUTES)
                .writeTimeout(5, TimeUnit.MINUTES)
                .readTimeout(5, TimeUnit.MINUTES);
        OkHttpClient client = builder.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.tviso.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        retrofitService = retrofit.create(TvisoService.class);
    }

    private void getAuthToken(final AuthTokenCallback callback){
        retrofitService.getAuthToken(API_ID, APP_SECRET).enqueue(new RetrofitErrorHandler<Void, AuthTokenDTO>(callback) {
            @Override
            public void onResponse(Call<AuthTokenDTO> call, Response<AuthTokenDTO> response) {
                if(response.isSuccessful()) {
                    apiToken = response.body();
                    callback.onSuccess(null);
                }
                else {
                    super.onResponse(call, response);
                }
            }
        });
    }

    private void getMediaByImdb(String imdbId, final MediaInfoCallback callback){
        retrofitService.getMediaByImdb(apiToken.getAuthToken(), imdbId).enqueue(new RetrofitErrorHandler<MediaInfoDTO, MediaResultDTO>(callback) {
            @Override
            public void onResponse(Call<MediaResultDTO> call, Response<MediaResultDTO> response) {
                if(response.isSuccessful()) {
                    callback.onSuccess(response.body().getResults().get(0));
                }
                else {
                    super.onResponse(call, response);
                }
            }
        });
    }

    @Override
    public void getMediaComments(final String imdbId, final MediaReviewsCallback callback) {
        if(apiToken == null){
            getAuthToken(new AuthTokenCallback() {
                @Override
                public void onSuccess(Void returnParam) {
                    getMediaComments(imdbId, callback);
                }

                @Override
                public void onError(ErrorBundle errorBundle) {
                    callback.onError(errorBundle);
                }
            });
        }
        else{
            getMediaByImdb(imdbId, new MediaInfoCallback(){
                @Override
                public void onSuccess(final MediaInfoDTO returnParam) {
                    String mediaId = returnParam.getMediaInfo().getMediaType() + "-" + returnParam.getMediaInfo().getIdm();
                    retrofitService.getMediaReviews(mediaId, apiToken.getAuthToken(), "most_voted").enqueue(new RetrofitErrorHandler<MediaReviewsDTO, ReviewsResultDTO>(callback) {
                        @Override
                        public void onResponse(Call<ReviewsResultDTO> call, Response<ReviewsResultDTO> response) {
                            if(response.isSuccessful()){
                                callback.onSuccess(response.body().getResults());
                            }
                            else{
                                super.onResponse(call, response);
                            }
                        }
                    });
                }

                @Override
                public void onError(ErrorBundle errorBundle) {
                    callback.onError(errorBundle);
                }
            });
        }
    }

    private interface AuthTokenCallback extends DefaultCallback<Void> {}

    private interface MediaInfoCallback extends DefaultCallback<MediaInfoDTO> {}
}
