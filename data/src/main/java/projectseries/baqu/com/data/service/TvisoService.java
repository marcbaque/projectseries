package projectseries.baqu.com.data.service;

import projectseries.baqu.com.data.entity.tviso.AuthTokenDTO;
import projectseries.baqu.com.data.entity.tviso.MediaResultDTO;
import projectseries.baqu.com.data.entity.tviso.ReviewsResultDTO;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by mbaque on 07/02/2017.
 */

public interface TvisoService {

    @GET("auth_token")
    Call<AuthTokenDTO> getAuthToken(@Query("id_api") String idApi, @Query("secret") String appSecret);

    @GET("v2/media/find/external")
    Call<MediaResultDTO> getMediaByImdb(@Query("auth_token") String authToken, @Query("imdb") String imdbId);

    @GET("v2/media/{mediaIndex}/reviews")
    Call<ReviewsResultDTO> getMediaReviews(@Path("mediaIndex") String mediaId, @Query("auth_token") String authToken, @Query("order") String order);

}
