package projectseries.baqu.com.data.mapper;

import com.baqu.entity.Comment;
import com.baqu.entity.MediaComments;
import com.baqu.entity.Reply;
import com.baqu.entity.TvisoUser;

import java.util.ArrayList;
import java.util.List;

import projectseries.baqu.com.data.entity.tviso.MediaReviewsDTO;
import projectseries.baqu.com.data.entity.tviso.ReplyDTO;
import projectseries.baqu.com.data.entity.tviso.ReviewDTO;
import projectseries.baqu.com.data.entity.tviso.TvisoActor;

/**
 * Created by mbaque on 07/02/2017.
 */

public class DataCommentsMapper {

    public static MediaComments toMediaComments(MediaReviewsDTO reviewsDTO){

        List<Comment> comments = new ArrayList<Comment>();

        for(ReviewDTO reviewDTO : reviewsDTO.getReviews()){
            comments.add(toComment(reviewDTO));
        }

        MediaComments mediaComments = new MediaComments(comments);
        return mediaComments;
    }

    private static Comment toComment(ReviewDTO reviewDTO){
        List<Reply> replies = new ArrayList<>();
        if(reviewDTO.getReplies()!=null) {
            for (ReplyDTO replyDTO : reviewDTO.getReplies()) {
                replies.add(toReply(replyDTO));
            }
        }

        Comment comment = new Comment(reviewDTO.getId(), toTvisoUser(reviewDTO.getActor()), reviewDTO.getRating(), reviewDTO.getContent(), reviewDTO.isSpoiler(), replies, reviewDTO.getDatePublished());
        return comment;
    }

    private static Reply toReply(ReplyDTO replyDTO){
        Reply reply = new Reply(replyDTO.getId(), toTvisoUser(replyDTO.getActor()), replyDTO.getContent(), replyDTO.getDatePublished());
        return reply;
    }

    private static TvisoUser toTvisoUser(TvisoActor actor){
        TvisoUser user = new TvisoUser(actor.getUid(), actor.getFullName(), actor.getImagePath());
        return user;
    }

}
