package projectseries.baqu.com.data.entity.tviso;

/**
 * Created by mbaque on 07/02/2017.
 */
public class MediaFullDTO {

    String idm;

    String mediaType;

    public MediaFullDTO() {
    }

    public MediaFullDTO(String idm, String mediaType) {
        this.idm = idm;
        this.mediaType = mediaType;
    }

    public String getIdm() {
        return idm;
    }

    public void setIdm(String idm) {
        this.idm = idm;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }
}
