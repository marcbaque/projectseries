package projectseries.baqu.com.data.entity.tviso;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mbaque on 07/02/2017.
 */

public class AuthTokenDTO {
    @SerializedName("auth_token")
    String authToken;

    @SerializedName("auth_expires_date")
    Integer expiringDate;

    String error;

    public AuthTokenDTO() {
    }

    public AuthTokenDTO(String authToken, Integer expiringDate, String error) {
        this.authToken = authToken;
        this.expiringDate = expiringDate;
        this.error = error;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public Integer getExpiringDate() {
        return expiringDate;
    }

    public void setExpiringDate(Integer expiringDate) {
        this.expiringDate = expiringDate;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
