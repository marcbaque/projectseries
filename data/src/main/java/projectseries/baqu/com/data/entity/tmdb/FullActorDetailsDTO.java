package projectseries.baqu.com.data.entity.tmdb;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mbaque on 09/02/2017.
 */

public class FullActorDetailsDTO {

    String id;

    String name;

    @SerializedName("place_of_birth")
    String placeOfBirth;

    @SerializedName("profile_path")
    String profilePath;

    String biography;

    @SerializedName("combined_credits")
    ActorCreditsDTO combinedCredits;

    public FullActorDetailsDTO(String id, String name, String placeOfBirth, String profilePath, String biography, ActorCreditsDTO combinedCredits) {
        this.id = id;
        this.name = name;
        this.placeOfBirth = placeOfBirth;
        this.profilePath = profilePath;
        this.biography = biography;
        this.combinedCredits = combinedCredits;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public String getProfilePath() {
        return profilePath;
    }

    public void setProfilePath(String profilePath) {
        this.profilePath = profilePath;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public ActorCreditsDTO getCombinedCredits() {
        return combinedCredits;
    }

    public void setCombinedCredits(ActorCreditsDTO combinedCredits) {
        this.combinedCredits = combinedCredits;
    }
}
