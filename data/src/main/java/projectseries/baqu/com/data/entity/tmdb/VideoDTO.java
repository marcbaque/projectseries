package projectseries.baqu.com.data.entity.tmdb;

/**
 * Created by mbaque on 08/02/2017.
 */
public class VideoDTO {

    String site;

    String key;

    String type;

    public VideoDTO() {
    }

    public VideoDTO(String site, String key, String type) {
        this.site = site;
        this.key = key;
        this.type = type;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
