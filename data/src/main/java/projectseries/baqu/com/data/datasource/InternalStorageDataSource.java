package projectseries.baqu.com.data.datasource;

import java.util.List;

import projectseries.baqu.com.data.entity.Genre;
import projectseries.baqu.com.data.entity.UserDTO;

/**
 * Created by mbaque on 04/02/2017.
 */

public interface InternalStorageDataSource {

    void storeUser(UserDTO user);

    UserDTO getUser();

    void removeUser();

    List<Genre> getGenres();
}
